angular.module('bovinoApp').config([
  '$stateProvider',
  '$urlRouterProvider',

  function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('home', {
      url: "/home",
      templateUrl: "template/partial-home.html"
    })
 	.state('bovino', {
		url : '/bovino',
		templateUrl : 'template/partial-bovino.html',
		controller : "BovinoController"
    })
   $urlRouterProvider.otherwise('/home');
   
}])
