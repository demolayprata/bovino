package com.brasilmilk.milkbrasil.Enum;

/**
 * Created by ricar on 24/11/2017.
 */

public enum Mask {
        CPF("[00]{.}[000]{.}[000]{-}[00]"),
        NIRF("[0]{.}[000]{.}[000]{-}[0]"),
        LATITUDE("[00]{º}[00]{'}[00]{''}[A]"),
        LONGITUDE("[00]{º}[00]{'}[00]{''}[A]"),
        INSC_ESTADUAL("[00]{.}[000]{.}[0000]{-}[0]"),
        CEP("[00]{.}[000]{-}[000]"),
        DATA("[00]{/}[00]{/}[0000]");
        private String mascara;

        Mask(String s) {
                this.mascara = s;
        }
        public String getMascara(){
                return  this.mascara;
        }
}
