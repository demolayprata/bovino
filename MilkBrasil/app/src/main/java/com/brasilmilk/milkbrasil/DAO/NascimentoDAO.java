package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class NascimentoDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Nascimento> listaNascimento;
    public NascimentoDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Nascimento.class);
            scriptSQLDelete = Util.getDROPSQLite(Nascimento.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaNascimento = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objBovino) {
        return super.insert(objBovino, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_NASCIMENTO");
        return super.delete(this.db, Nascimento.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_NASCIMENTO");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdNascimento"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaNascimento.clear();
        listaNascimento.addAll((Collection<? extends Nascimento>) super.getAll(db, Nascimento.class));
        return listaNascimento;
    }

    public List<?> getBovinosByNascimento(String idNascimento) {
        List<Bovino> bovinoList = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("ID_NASCIMENTO");
            whereBuilder.append(" = ?");

            String where = whereBuilder.toString();
            String[]args = new String[]{idNascimento};

            cursor = db.query(Util.getNomeTabela(Bovino.class), Util.getColumView(Bovino.class), where, args, null, null, null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Bovino.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, objLinha );
                    }
                    bovinoList.add((Bovino) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return bovinoList;
    }



    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        try {
            Integer paramCod = Integer.valueOf((String) param);
            compoList.add("ID_NASCIMENTO");
        }catch (Exception e){
            compoList.add("APELIDO");
        }
        String[] parametros = new String[]{(String) param};
        return super.getByParam(db, Nascimento.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Nascimento.class);
    }

}
