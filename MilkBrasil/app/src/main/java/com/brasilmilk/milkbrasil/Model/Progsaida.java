package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;



/**
 * The persistent class for the progsaida database table.
 * 
 */
@ClassAnotation(nomeTabela = "PROGSAIDA")
public class ProgSaida {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_PROGSAIDA", tipoAtributo = "INTEGER", nomeSet = "setIdProgsaida", nomeGet = "getIdProgsaida", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idProgsaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTSAIDA", nomeGet = "getDtSaida", nomeSet = "setDtSaida")
	private Date dtSaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "PESOSAIDA", tipoAtributo = "DOUBLE", nomeGet = "getPesoSaida", nomeSet = "setPesoSaida")
	private BigDecimal pesoSaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDPREPARO", tipoAtributo = "DOUBLE", nomeGet = "getQtdPreparo", nomeSet = "setQtdPreparo")
	private BigDecimal qtdPreparo;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_Bovino",tipoAtributo = "INTEGER", nomeGet = "getIdBovino", nomeSet = "setIdBovino",  forengKey = true, referencesTable = "BOVINO", fieldReferences = "ID_BOVINO", notNull = true)
	private Integer idBovino;

	public ProgSaida(Integer idProgsaida, Date dtSaida, BigDecimal pesoSaida, BigDecimal qtdPreparo, Integer idLote) {
		this.idProgsaida = idProgsaida;
		this.dtSaida = dtSaida;
		this.pesoSaida = pesoSaida;
		this.qtdPreparo = qtdPreparo;
		this.idBovino = idLote;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdProgsaida() {
		return idProgsaida;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdProgsaida(Integer idProgsaida) {
		this.idProgsaida = idProgsaida;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtSaida() {
		return dtSaida;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtSaida(Date dtSaida) {
		this.dtSaida = dtSaida;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getPesoSaida() {
		return pesoSaida;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setPesoSaida(BigDecimal pesoSaida) {
		this.pesoSaida = pesoSaida;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdPreparo() {
		return qtdPreparo;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdPreparo(BigDecimal qtdPreparo) {
		this.qtdPreparo = qtdPreparo;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdBovino() {
		return idBovino;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdBovino(Integer idLote) {
		this.idBovino = idLote;
	}
}