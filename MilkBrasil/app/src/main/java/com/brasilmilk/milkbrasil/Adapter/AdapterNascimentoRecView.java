package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.brasilmilk.milkbrasil.DAO.MontaDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterNascimentoRecView extends  RecyclerView.Adapter<AdapterNascimentoRecView.RecyclerTesteViewHolder> {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Nascimento> mList;




public AdapterNascimentoRecView(Context ctx, List<Nascimento> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;

        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_nascimento,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        Nascimento nascimento = mList.get(position);
        MontaDAO montaDAO = new MontaDAO(mctx );
        Bovino matriz = montaDAO.getMatrizByNascimento(nascimento.getIdMonta().toString());

            viewHolder.txtMatriz.setText("Apelido Matriz: " + matriz.getApelido());
            viewHolder.txtCodNascimento.setText("COD: " + nascimento.getIdNascimento().toString());
            viewHolder.txtDtNascimento.setText("Data Nascimento" + DateUtil.formatFieldToTxt(nascimento.getDtParto()));
            viewHolder.txtFlgVivo.setText( nascimento.isFlgVivo()?"Nascido Vivo":"Nascido Morto");
            viewHolder.txtSexo.setText(nascimento.getSexo().equals("M")?"Sexo: MACHO":"Sexo: FEMEA");
        }

@Override
public int getItemCount() {
        return mList.size();
        }


protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

    protected  TextView txtMatriz, txtCodNascimento, txtDtNascimento, txtFlgVivo, txtSexo ;

    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtMatriz = itemView.findViewById(R.id.txtMatriz);
        txtCodNascimento = itemView.findViewById(R.id.txtIdNascimento);
        txtDtNascimento = itemView.findViewById(R.id.txtDtNascimento);
        txtFlgVivo = itemView.findViewById(R.id.txtFlgVivo);
        txtSexo = itemView.findViewById(R.id.txtSexo);




        //Setup the click listener
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));

            }
        });
    }
}
}