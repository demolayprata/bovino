package com.brasilmilk.milkbrasil.Util;

import android.annotation.SuppressLint;
import android.database.Cursor;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.Spinner;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;
import com.brasilmilk.milkbrasil.Enum.IdadeBovinoEnum;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by ricar on 20/11/2017.
 */
public class Util {

    public static String[] getColumView(final Class<?> clazz){

        try {
            int i = 0;
            String[] colunasApresentacao = new String[clazz.getDeclaredFields().length];
            for(Field field : clazz.getDeclaredFields()){
                if(field.isAnnotationPresent(ApresentacaoAN.class)){
                    ApresentacaoAN anotation = field.getAnnotation(ApresentacaoAN.class);
                    if(anotation.apresentar()){
                        if(field.isAnnotationPresent(AtributoAnotation.class)) {
                            AtributoAnotation anotationAtributo = field.getAnnotation(AtributoAnotation.class);
                            colunasApresentacao[i] = anotationAtributo.nomeAtributo();
                            i++;
                        }
                    }
                }


            }
            return  colunasApresentacao;
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return  null;
    }

    public static String[] setColumn(final Object obj){
        String[] colunasApresentacao = new String[]{};
        try {
            Class<?> clazz = obj.getClass();
            int i = 0;
            for(Field field : clazz.getDeclaredFields()){
                if(field.isAnnotationPresent(ApresentacaoAN.class)){
                    ApresentacaoAN anotation = field.getAnnotation(ApresentacaoAN.class);
                    if(anotation.apresentar()){
                        colunasApresentacao[i] = field.getName();
                    }
                }
                i++;
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return  colunasApresentacao;
    }
    public static Object createNewInstance(final Class clazz)
            throws InstantiationException, IllegalAccessException,
            InvocationTargetException {
        Constructor<?> ctor;
        try {
            ctor = clazz.getConstructors()[0];
            Object object = ctor.newInstance();
            return object;

        } catch (SecurityException | IllegalArgumentException e) {
            e.printStackTrace();

        }

        return null;
    }

    public static Object createNewInstance(Object obj)
            throws InstantiationException, IllegalAccessException,
            InvocationTargetException {
        Constructor<?> ctor;
        try {
            Class<?> clazz = obj.getClass();
            ctor = clazz.getConstructors()[0];
            Object object = ctor.newInstance();
            return object;

        } catch (SecurityException | IllegalArgumentException e) {
            e.printStackTrace();

        }

        return null;
    }


    public static HashMap<String, String> getColumKey(Object obj) {
        HashMap<String, String> mapKey = new HashMap<>();
        try {
            Class<?> clazz = obj.getClass();
            for(Field field : clazz.getDeclaredFields()){
                if(field.isAnnotationPresent(AtributoAnotation.class)){
                    AtributoAnotation anotation = field.getAnnotation(AtributoAnotation.class);
                    mapKey.put(anotation.nomeAtributo(), anotation.nomeGet());
                }
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }

        return mapKey;
    }
    public static HashMap<String, String> getColumSet(Class<?> clazz) {
        HashMap<String, String> mapKey = new HashMap<>();
        try {
            for(Field field : clazz.getDeclaredFields()){
                if(field.isAnnotationPresent(AtributoAnotation.class)){
                    AtributoAnotation anotation = field.getAnnotation(AtributoAnotation.class);
                    mapKey.put(anotation.nomeAtributo(), anotation.nomeSet());
                }
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }

        return mapKey;
    }

    public static Object getValueMethod(final Object obj, String metodo) {
        try {
            Class<?> clazz = obj.getClass();
            for(Method method : clazz.getDeclaredMethods()){
                if(method.getName().equals(metodo)){
                    if(method.isAnnotationPresent(MetodoAnotation.class)){
                        MetodoAnotation anotation = method.getAnnotation(MetodoAnotation.class);
                        return method.invoke(obj);

                    }

                }
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return null;
    }

    public static String getTipReturnMethod(final Object obj, String metodo) {
        try {
            Class<?> clazz = obj.getClass();
            for(Method method : clazz.getDeclaredMethods()){
                if(method.getName().equals(metodo)){
                    if(method.isAnnotationPresent(MetodoAnotation.class)){
                        MetodoAnotation anotation = method.getAnnotation(MetodoAnotation.class);
                        return anotation.tipoRetorno();
                    }

                }
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return null;
    }

    public static String getTipSETMethod(final Class<?> clazz, String metodo) {
        try {

            for(Method method : clazz.getDeclaredMethods()){
                if(method.getName().equals(metodo)){
                    if(method.isAnnotationPresent(MetodoAnotation.class)){
                        MetodoAnotation anotation = method.getAnnotation(MetodoAnotation.class);
                        return anotation.tipoSet();
                    }

                }
            }
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return null;
    }

    public static String getNomeTabela(final Class<?> clazz){
        try {
            if(clazz.isAnnotationPresent(ClassAnotation.class)){
                ClassAnotation anotation = clazz.getAnnotation(ClassAnotation.class);
                return anotation.nomeTabela();
            }

        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return null;
    }
    public static String getNomeTabela(final Object obj){
        try {
            Class<?> clazz = obj.getClass();
            if(clazz.isAnnotationPresent(ClassAnotation.class)){
                ClassAnotation anotation = clazz.getAnnotation(ClassAnotation.class);
                return anotation.nomeTabela();
            }

        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return null;
    }

    public static String getPrimaryKey(final Class<?> clazz) {
        try {

            for(Field field : clazz.getDeclaredFields()){
                if(field.isAnnotationPresent(AtributoAnotation.class)){
                    AtributoAnotation anotation = field.getAnnotation(AtributoAnotation.class);
                    if(anotation.primaryKeyAtributo()){
                        return anotation.nomeAtributo();
                    }
                }
            }

        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }

        return null;
    }

    public static String getSQLCreate(final Class<?> clazz) {
        StringBuilder scritpBuilder = new StringBuilder();
        StringBuilder scritpForengKey = new StringBuilder();
        scritpBuilder.append("CREATE TABLE IF NOT EXISTS ");
        scritpBuilder.append(getNomeTabela(clazz));
        scritpBuilder.append(" ( ");
        //Class<?> clazz = obj.getClass();
        int cont = 0;
        for (Field field : clazz.getDeclaredFields()){
            if(field.isAnnotationPresent(AtributoAnotation.class)){
                AtributoAnotation anotation = field.getAnnotation(AtributoAnotation.class);
                scritpBuilder.append(anotation.nomeAtributo());
                scritpBuilder.append(" ");
                //scritpBuilder.append(anotation.tipoAtributo().toUpperCase());
                switch (anotation.tipoAtributo()){
                    case ("INTEGER"):
                        scritpBuilder.append(anotation.tipoAtributo());
                        break;
                    case ("VARCHAR"):
                        scritpBuilder.append(anotation.tipoAtributo());
                        scritpBuilder.append(" ( ");
                        scritpBuilder.append(anotation.tamanhoAtributo());
                        scritpBuilder.append(" ) ");
                        break;
                    case ("CHAR"):
                        scritpBuilder.append(anotation.tipoAtributo());
                        scritpBuilder.append(" ( ");
                        scritpBuilder.append(anotation.tamanhoAtributo());
                        scritpBuilder.append(" ) ");
                        break;
                }
                scritpBuilder.append(" ");

                if(anotation.primaryKeyAtributo()){
                    scritpBuilder.append(" PRIMARY KEY ");
                }

                if(anotation.autoIncrementAtributo()){
                    scritpBuilder.append(" AUTOINCREMENT ");
                }

                if(anotation.notNull()){
                    scritpBuilder.append(" NOT NULL ");
                }
                if(anotation.unique()){
                    scritpBuilder.append(" UNIQUE ");
                }
                if(anotation.forengKey()){
                    //FOREIGN KEY(id_categoria) REFERENCES categoria(id_categoria)
                    scritpForengKey.append(", FOREIGN KEY( ");
                    scritpForengKey.append(anotation.nomeAtributo());
                    scritpForengKey.append(") REFERENCES ");
                    scritpForengKey.append(anotation.referencesTable());
                    scritpForengKey.append("(");
                    scritpForengKey.append(anotation.fieldReferences());
                    scritpForengKey.append(")");
                    if(anotation.cascade()){
                        scritpForengKey.append(" ON DELETE CASCADE ");
                    }
                }



            }
            cont++;
            if(cont < clazz.getDeclaredFields().length){
                scritpBuilder.append(", ");
            }
        }
        if(scritpForengKey.toString().length()>0){
            scritpBuilder.append(scritpForengKey.toString());
        }

        scritpBuilder.append(");");

        return scritpBuilder.toString();
    }
    //"DROP TABLE IF EXISTS VENDEDOR"
    public static String getDROPSQLite( final Class<?> obj){
        StringBuilder scritpBuilder = new StringBuilder();
        scritpBuilder.append("DROP TABLE IF EXISTS ");
        scritpBuilder.append(getNomeTabela(obj));
        scritpBuilder.append("; ");
        return scritpBuilder.toString();
    }

    @SuppressLint("NewApi")
    public static void setValueAtributoByCursor(String nomeAtributo, String nomeMetodo, final Cursor cursor, Class<?> clazz, final Object objLinha) {
        //Class<?> clazz = objClazz.getClass();
        //Object obj = null;
        try {
            //obj = createNewInstance(clazz);
            String tipoSet = Util.getTipSETMethod(clazz, nomeMetodo);
            for(Method method : clazz.getDeclaredMethods()){
                if(method.getName().equals(nomeMetodo)){
                    switch (tipoSet){
                        case "String":
                            if(cursor.getString(cursor.getColumnIndex(nomeAtributo)) != null) {
                                method.invoke(objLinha, cursor.getString(cursor.getColumnIndex(nomeAtributo)));
                            }
                            break;
                        case "boolean":
                            if( cursor.getInt(cursor.getColumnIndex(nomeAtributo)) > 0) {
                                method.invoke(objLinha, cursor.getInt(cursor.getColumnIndex(nomeAtributo)) == 1 ? true : false);
                            }else {
                                method.invoke(objLinha, false);
                            }
                            break;
//                            case "Byte":
//                                method.invoke(obj, cursor.getB(cursor.getColumnIndex(nomeAtributo)));
//                                break;
                        case "Double":
                            method.invoke(objLinha, cursor.getDouble(cursor.getColumnIndex(nomeAtributo)));
                            break;
                        case "Float":
                            method.invoke(objLinha, cursor.getFloat(cursor.getColumnIndex(nomeAtributo)));
                            break;
                        case "Integer":
                            //vendedorLinha.setID_VENDEDOR(cursor.getInt(cursor.getColumnIndex("ID_VENDEDOR")));
                            if(cursor.getInt(cursor.getColumnIndex(nomeAtributo)) > 0) {
                                method.invoke(objLinha, cursor.getInt(cursor.getColumnIndex(nomeAtributo)));
                            }
                            break;
                        case "Long":
                            method.invoke(objLinha, cursor.getLong(cursor.getColumnIndex(nomeAtributo)));
                            break;
                        case "Short":
                            method.invoke(objLinha, cursor.getShort(cursor.getColumnIndex(nomeAtributo)));
                            break;
                        case "BigDecimal":
                            method.invoke(objLinha, BigDecimal.valueOf(cursor.getDouble(cursor.getColumnIndex(nomeAtributo))));
                            break;
                        case "Date":
                            try{
                                method.invoke(objLinha, DateUtil.covertToDateSql(cursor.getString(cursor.getColumnIndex(nomeAtributo))));
                            }catch (Exception e){
                                Log.e("Erro:", e.getMessage());
                            }
                            break;
                    }

                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

    }

    public static  void setMaskEditText(final EditText edt, String mask){
        MaskedTextChangedListener nirfMask = InputMask.getMask(edt, mask);
        edt.addTextChangedListener(nirfMask);
        edt.setOnFocusChangeListener(nirfMask);
    }
    // metodo para retornar a posicao do valor na spinner
    public static  int getIndexSpinner(Spinner spinner, String myValue){
        int index = 0;
        for(int i = 0; i< spinner.getCount();i++){
            if(spinner.getItemAtPosition(i).equals(myValue)){
                index = i;
            }
        }
        return  index;
    }

    public static double DMSToDecimal(String valor) {
        int grados =0;
        int minutos =0;
        int segundos=0;
        String direccion="";
        int anterior = 0;
        anterior = valor.indexOf("º");
        grados = Integer.parseInt(valor.substring(0,anterior));
        anterior++;
        minutos = Integer.parseInt(valor.substring(anterior,valor.indexOf("'")));
        anterior = valor.indexOf("'") +1;
        segundos = Integer.parseInt(valor.substring(anterior,valor.indexOf("''")));
        anterior = valor.indexOf("''")+2;
        direccion = valor.substring(anterior,valor.length());
        double decimal = Math.signum(grados) * (Math.abs(grados) + (minutos / 60.0) + (segundos / 3600.0));

        //reverse for south or west coordinates; north is assumed
        if (direccion.equals("S") || direccion.equals("W")) {
            decimal *= -1;
        }

        return decimal;
    }

    public static String[] separararDMS(String coordenada, int type) {

        int dir = Integer.signum((int) Double.parseDouble(coordenada));
        coordenada = String.valueOf(Math.abs(Double.parseDouble(coordenada)));
        String grados = null;
        String minutos = null;
        String segundos = null;
        String direccion = null;
        String decimaisGrados = null;
        String minAux = null;
        String decimaisMinAux = null;

        grados = coordenada.substring(0,coordenada.indexOf("."));
        decimaisGrados = String.valueOf(Math.abs(Double.parseDouble(grados) - Double.parseDouble(coordenada)));
        minAux = String.valueOf(Double.parseDouble(decimaisGrados)*60);

        minutos = minAux.substring(0,minAux.indexOf("."));
        minutos = minutos.length()<=1?"0"+minutos:minutos;
        decimaisMinAux = String.valueOf(Math.abs(Double.parseDouble(minutos) - Double.parseDouble(minAux)));

        segundos = String.valueOf(Double.parseDouble(decimaisMinAux)*60);
        segundos = segundos.substring(0,segundos.indexOf("."));
        segundos = segundos.length()<=1?"0"+segundos:segundos;

        switch (type) {
            case 1: // latitude
                if(dir>0){
                 direccion = "N";
                }else {
                    direccion ="S";
                }
                break;
            case 2: // longitude
                if(dir>0){
                    direccion = "E";
                }else {
                    direccion ="W";
                }
                break;
            default:
        }
        String[] data = {grados, minutos, segundos, direccion};
        return data;
    }

    public static TextWatcher monetario(final EditText ediTxt) {
        return new TextWatcher() {
            // Mascara monetaria para o preço do produto
            private String current = "";
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void afterTextChanged(Editable s) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(!s.toString().equals(current)){
                    ediTxt.removeTextChangedListener(this);

                    String cleanString = s.toString().replaceAll("[R$,.]", "");

                    double parsed = Double.parseDouble(cleanString);
                    String formatted = NumberFormat.getCurrencyInstance().format((parsed/100));

                    current = formatted.replaceAll("[R$]", "");
                    ediTxt.setText(current);
                    ediTxt.setSelection(current.length());

                    ediTxt.addTextChangedListener(this);
                }
            }

        };
    }

    public static BigDecimal parseToBigDecimal(String value, Locale locale) {
        String replaceable = String.format("[%s,.\\s]", NumberFormat.getCurrencyInstance(locale).getCurrency().getSymbol());


        String cleanString = value.replaceAll(replaceable, "");

        return new BigDecimal(cleanString).setScale(
                2, BigDecimal.ROUND_FLOOR).divide(new BigDecimal(100), BigDecimal.ROUND_FLOOR
        );
    }

    public static Double parseBigDecimalToDouble(BigDecimal valor){
        return valor.doubleValue();
    }






}
