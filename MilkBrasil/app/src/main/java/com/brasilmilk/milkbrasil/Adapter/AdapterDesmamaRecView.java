package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.DAO.MontaDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterDesmamaRecView extends  RecyclerView.Adapter<AdapterDesmamaRecView.RecyclerTesteViewHolder> {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Desmama> mList;




public AdapterDesmamaRecView(Context ctx, List<Desmama> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;

        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_desmama,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        Desmama desmama = mList.get(position);

            viewHolder.txtDtDesmama.setText("Data Desmama: " + DateUtil.formatFieldToTxt(desmama.getDtDesmama()));
            viewHolder.txtCodDesmama.setText("COD: " + desmama.getIdDesmama());
            viewHolder.txtIdadeDesmama.setText("Idade Desamama: " + desmama.getIdade());
            viewHolder.txtObsDesmama.setText("OBS: " + desmama.getObs());
        }

@Override
public int getItemCount() {
        return mList.size();
        }


protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

    protected  TextView txtDtDesmama, txtCodDesmama, txtIdadeDesmama, txtObsDesmama ;

    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtDtDesmama = itemView.findViewById(R.id.txtDtDesmama);
        txtCodDesmama = itemView.findViewById(R.id.txtIdDesmama);
        txtIdadeDesmama = itemView.findViewById(R.id.txtIdade);
        txtObsDesmama = itemView.findViewById(R.id.txtObs);




        //Setup the click listener
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));

            }
        });
    }
}
}