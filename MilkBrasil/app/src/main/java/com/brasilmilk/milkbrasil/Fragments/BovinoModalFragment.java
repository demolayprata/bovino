package com.brasilmilk.milkbrasil.Fragments;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Activity.BovinoCadastro;
import com.brasilmilk.milkbrasil.Adapter.BovinoAdapter;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.DAO.PesoDAO;
import com.brasilmilk.milkbrasil.Helper.BovinoHelper;
import com.brasilmilk.milkbrasil.Helper.DesmamaHelper;
import com.brasilmilk.milkbrasil.Helper.PesoHelper;
import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.MoneyTextWatcher;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;


public class BovinoModalFragment extends Fragment implements View.OnClickListener, Validator.ValidationListener {
    private Context ctx;
    private Button   btnPesoLista, btnSelecionarRaca,btnCancelar, btnSalvar, btnNovoRacaModal;
    private CheckBox chkAtivo, chkLeite, chkEngorda, chkReprodutorMatriz;
    private RadioButton rbMacho, rbFemea, rbIdade0_12Meses, rbIdade13_24Meses, rbIdade25_36Meses, rbIdadeAcima_36Meses,  rbIdade0_12MesesDesmama, rbIdade13_24MesesDesmama, rbIdade25_36MesesDesmama,
            rbIdadeAcima_36MesesDesmama, rbCheia, rbVazia;;
    private RadioGroup rdGroup;
    private  RadioGroup rdGroupIdadeDesmama, rdGroupIdadeBovino, rGroupStatus;

    //private int ano, mes, dia;
    private Calendar cal;
    static final int DATE_DIALOG_ID = 0;

    private EditText edtIdBovino;

    @NotEmpty(message = "Apeldio Obrigatorio")
    private EditText edtApelido;

    private EditText edtCodBarras;

    @NotEmpty(message = "Cor da PelagemObrigatorio")
    private EditText edtCorPelagem;

    private EditText edtDtDesmama;

    @NotEmpty(message = "Data da Entrada Obrigatorio")
    private EditText edtDtEntrada;

    @NotEmpty(message = "Data Inicio Obrigatorio")
    private EditText edtDtInicio;

    @NotEmpty(message = "Data Nascimento Obrigatorio")
    private  EditText edtDtNascimento;

    private EditText edtNrEletronico;

    private EditText edtNrManejoSisBov;



    @NotEmpty(message = "Raca Obrigatorio")
    private EditText edtIdRaca;

    @NotEmpty(message = "Raca Obrigatorio")
    private EditText edtNmRaca;


    private boolean validacaoOk = false;
    private String sexo = "M";
    private BovinoDAO bovinoDAO;
    private PesoDAO pesoDAO;
    private  Bovino bovinoObj;
    private Validator validator;

    private  String campoDataSelecionado;
    private BovinoAdapter bovinoAdapter;
    private Locale mLocale;
    private AlertDialog alerta;
    private DatePickerDialog datePickerDialog;
    private BovinoHelper bovinoHelper;
    private Peso peso;

    private Desmama desmamaObj;
    DesmamaHelper desmamaHelper;

    public BovinoModalFragment() {
        // Required empty public constructor
    }


    public static BovinoModalFragment newInstance(String param1, String param2) {
        BovinoModalFragment fragment = new BovinoModalFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bovino_modal, container, false);


        rdGroup = (RadioGroup) view.findViewById(R.id.radioGroup);
        rbMacho=(RadioButton) view.findViewById(R.id.rbMasculino);
        rbFemea = (RadioButton) view.findViewById(R.id.rbFeminino);


        rGroupStatus = (RadioGroup) view.findViewById(R.id.radioGroupStatus);
        rbVazia = (RadioButton) view.findViewById(R.id.rbVazia);
        rbCheia = (RadioButton) view.findViewById(R.id.rbCheia);
        rbVazia.setChecked(true);



        rbIdade0_12Meses = (RadioButton) view.findViewById(R.id.rbIdade_0_A_12_meses);
        rbIdade13_24Meses = (RadioButton) view.findViewById(R.id.rbIdade_13_A_24_meses);
        rbIdade25_36Meses = (RadioButton) view.findViewById(R.id.rbIdade_25_A_36_meses);
        rbIdadeAcima_36Meses = (RadioButton) view.findViewById(R.id.rbIdade_ACIMA_36_meses);

        rbIdade0_12Meses.setChecked(true);


        chkAtivo = (CheckBox) view.findViewById(R.id.chkAtivo);
        chkAtivo.setChecked(true);

        chkEngorda = (CheckBox) view.findViewById(R.id.chkEngorda);
        chkLeite = (CheckBox) view.findViewById(R.id.chkLeite);
        chkReprodutorMatriz = (CheckBox) view.findViewById(R.id.chkReprodutor);


        validator = new Validator(this);
        validator.setValidationListener(this);

        edtNrEletronico = (EditText) view.findViewById(R.id.edtNrEletronico);
        edtNrManejoSisBov = (EditText) view.findViewById(R.id.edtNrManjSisbov);
        edtCodBarras = (EditText)view. findViewById(R.id.edtCodBarras);
        edtIdBovino = (EditText) view.findViewById(R.id.edtIdBovino);
        edtApelido = (EditText) view.findViewById(R.id.edtApelido);
        edtCodBarras = (EditText) view.findViewById(R.id.edtCodBarras);
        edtCorPelagem = (EditText) view.findViewById(R.id.edtCorPelagem);

        edtDtEntrada = (EditText) view.findViewById(R.id.edtDtEntrada);
        edtDtInicio = (EditText) view.findViewById(R.id.edtDtInicio);
        edtDtNascimento =(EditText) view.findViewById(R.id.edtDtNascimento);
        edtIdRaca = (EditText) view.findViewById(R.id.edtIdRaca);
        edtNmRaca = (EditText) view.findViewById(R.id.edtNomeRaca);


        edtIdRaca.setEnabled(false);
        edtNmRaca.setEnabled(false);


        mLocale = new Locale("pt", "BR");

        chkAtivo.setChecked(true);
        chkAtivo.setEnabled(false);
        chkLeite.setChecked(true);
        cal = Calendar.getInstance();

        ctx = inflater.getContext();
        btnPesoLista = (Button) view.findViewById(R.id.btnListarPesos);
//        btnNovoRacaModal = (Button) view.findViewById()
//        btnNovoRacaModal.setOnClickListener(this);
        btnSelecionarRaca = (Button)view.findViewById(R.id.btnSelecionarRaca);
        btnSelecionarRaca.setOnClickListener(this);
        btnCancelar = (Button) view.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
        btnSalvar = (Button) view.findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(this);

        bovinoObj = new Bovino();
//        bovinoHelper = new BovinoHelper(ctx, edtNmRaca, edtIdRaca, edtIdBovino, edtApelido, edtCodBarras, edtCorPelagem,
//                edtDtEntrada, edtDtInicio, edtDtNascimento, edtNrEletronico, edtNrManejoSisBov,  sexo, chkAtivo, rbMacho,
//                rbFemea, cal, mLocale, bovinoObj, rbIdade0_12Meses, rbIdade13_24Meses, rbIdade25_36Meses, rbIdadeAcima_36Meses,
//                rdGroup,  rdGroupIdadeBovino, chkLeite, chkEngorda, chkReprodutorMatriz, rGroupStatus, rbVazia,rbCheia);

        return view;

    }


    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
////            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        if(view == btnPesoLista){
            LayoutInflater li = getLayoutInflater();

            //inflamos o layout alerta.xml na view
            final View viewModalPesoList = li.inflate(R.layout.modal_pesos_lista, null);
            //definimos para o botão do layout um clickListener

                    EditText edtPesoNovo = (EditText) viewModalPesoList.findViewById(R.id.edtPesoValorNovo);
                    this.peso = new Peso();
                    this.peso.setQtdQuilos(BigDecimal.valueOf(Double.parseDouble(edtPesoNovo.getText().toString())));
                    this.peso.setDtPesagem(DateUtil.gerarDateNowSql());

                    alerta.dismiss();
                }
        if(view == btnSelecionarRaca){
            LayoutInflater li = getLayoutInflater();
            //inflamos o layout alerta.xml na view
            final View viewModalRacaList = li.inflate(R.layout.modal_raca_lista, null);
            bovinoHelper.inflaterModalRacaList(viewModalRacaList, edtIdBovino.getText().toString(), alerta);
            btnNovoRacaModal =(Button) viewModalRacaList.findViewById(R.id.btnNovo);
            btnNovoRacaModal.setOnClickListener(this);

        }
        if(view.equals(btnNovoRacaModal)){
            RacaHelper racaHelper = new RacaHelper(ctx);
            LayoutInflater li = getLayoutInflater();
            final View viewModalRacaCrud = li.inflate(R.layout.modal_raca_crud,null);
            racaHelper.inflaterModalRacaCrud(viewModalRacaCrud, alerta);
            btnSelecionarRaca.setClickable(true);
        }

        if(view.equals(btnSalvar)){
            try{
                validator.validate();
                if(validacaoOk) {
                     Integer id = bovinoHelper.salvar();

                    validacaoOk = false;
                }
            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }


        }

    }

    @Override
    public void onValidationSucceeded() {
        validacaoOk = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(ctx));
            }
            iterator.remove();
        }
    }
}
