package com.brasilmilk.milkbrasil.Enum;

/**
 * Created by ricar on 10/01/2018.
 */

public enum IdadeBovinoEnum {
    BEZERROS_0_A_12_MESES("BEZERRO (OS/AS) 0 A 12 MESES"),
    NOVILHOS_13_A_24_MESES("NOVILHOS (OS/AS) 13 A 24 MESES"),
    NOVILHOS_25_A_36_MESES("NOVILHOS (OS/AS) 25 A 36 MESES"),
    NOVILHOS_ACIMA_37_MESES("NOVILHOS (OS/AS) ACIMA DE 37 MESES");
    private String idade;

    IdadeBovinoEnum(String idade) {
        this.idade = idade;
    }
    public String getIdadeBovino(){
        return this.idade;
    }
}
