package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.sql.Date;



@ClassAnotation(nomeTabela = "DESMAMA")
public class Desmama  {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_DESMAMA", tipoAtributo = "INTEGER", nomeSet = "setIdDesmama", nomeGet = "getIdDesmama", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idDesmama;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTDESMAMA", nomeGet = "getDesmama", nomeSet = "setDesmama")
	private Date dtDesmama;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "IDADE", nomeGet = "getIdade", nomeSet = "setIdade")
	private String idade;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "OBS", nomeGet = "getObs", nomeSet = "setObs")
	private String obs;

	public Desmama(Integer idDesmama, Date dtDesmama, String idade, String obs) {
		this.idDesmama = idDesmama;
		this.dtDesmama = dtDesmama;
		this.idade = idade;
		this.obs = obs;
	}

	public Desmama() {
			}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdDesmama() {
		return idDesmama;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdDesmama(Integer idDesmama) {
		this.idDesmama = idDesmama;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtDesmama() {
		return dtDesmama;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtDesmama(Date dtDesmama) {
		this.dtDesmama = dtDesmama;
	}

	@MetodoAnotation()
	public String getIdade() {
		return idade;
	}

	@MetodoAnotation()
	public void setIdade(String idade) {
		this.idade = idade;
	}

	@MetodoAnotation()
	public String getObs() {
		return obs;
	}

	@MetodoAnotation()
	public void setObs(String obs) {
		this.obs = obs;
	}
}