package com.brasilmilk.milkbrasil.Fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Helper.BovinoHelper;
import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.Helper.MontaHelper;
import com.brasilmilk.milkbrasil.Helper.NascimentoHelper;
import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Iterator;
import java.util.List;

public class NascimentoFragment extends Fragment implements Validator.ValidationListener, View.OnClickListener, TabHost.OnTabChangeListener, ClickRecyclerView_Interface {


    private FragmentActivity myContext;
    private ButtonReferenceInterface buttonReferenceInterface;

    private MontaResultadoFragment.OnItemSelectedListener listener;

    private EditText edtCodNascimento;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtDtParto;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodMonta;
    private EditText edtDtMonta;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodBovino;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtNmBovino;
    private RadioButton rbMacho;
    private RadioButton rbFemea;
    private RadioButton rbVivo;
    private RadioButton rbMorto;
    private RadioGroup rgSexo;
    private RadioGroup rgStatusNascimento;
    private TabHost tabHost;
    private RecyclerView recyclerView;
    private Button  btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar, btnSelecBovino, btnOk, btnNovoBovino, btnSelecMonta;
    private NascimentoHelper nascimentoHelper;
    private Nascimento nascimento;
    private Validator validator;
    private boolean validacaoOk = false;
    private Context ctx;
    private AlertDialog alerta;
    private FragmentManager fm;
    private FrameLayout frameLayout;
    public NascimentoFragment() {
        // Required empty public constructor
    }


    public static NascimentoFragment newInstance(String param1, String param2) {
        NascimentoFragment fragment = new NascimentoFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_nascimento, container, false);
        edtCodMonta = (EditText) view.findViewById(R.id.edtCodMonta);
        edtCodNascimento = (EditText) view.findViewById(R.id.edtCodNascimento);
        edtDtMonta = (EditText) view.findViewById(R.id.edtDtMonta);
        edtDtParto = (EditText) view.findViewById(R.id.edtDtParto);
        edtCodBovino = (EditText) view.findViewById(R.id.edtCodBovino);
        edtNmBovino = (EditText) view.findViewById(R.id.edtNmBov);
        btnSelecBovino = (Button) view.findViewById(R.id.btnSelecBovino);
        this.btnSelecMonta =(Button) view.findViewById(R.id.btnSelecMonta);
        this.btnSelecBovino.setOnClickListener(this);
        this.btnSelecMonta.setOnClickListener(this);
        rgSexo = (RadioGroup) view.findViewById(R.id.rgSexo);
        rgStatusNascimento = (RadioGroup) view.findViewById(R.id.rgStatusNascimento);
        rbFemea = (RadioButton) view.findViewById(R.id.rbFemea);
        rbMacho = (RadioButton) view.findViewById(R.id.rbMacho);
        rbMacho.setChecked(true);
        rbMorto = (RadioButton) view.findViewById(R.id.rbMorto);
        this.rbMorto.setChecked(true);
        rbVivo = (RadioButton) view.findViewById(R.id.rbVivo);

        ctx = inflater.getContext();
        nascimento = new Nascimento();
        nascimentoHelper = new NascimentoHelper(ctx,nascimento, recyclerView, edtCodNascimento, edtDtParto, edtCodMonta,
                edtDtMonta, edtCodBovino, edtNmBovino, rbMacho, rbFemea, rbVivo, rbMorto,rgSexo, rgStatusNascimento);
        validator = new Validator(this);
        validator.setValidationListener(this);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
       ctx = context;
        if(context instanceof ButtonReferenceInterface){
            buttonReferenceInterface = (ButtonReferenceInterface) context;
        }

        myContext=(FragmentActivity) context;
        super.onAttach(context);
        this.fm = myContext.getSupportFragmentManager();

    }

    @Override
    public void onDetach() {
        super.onDetach();

    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        if(view.equals(btnCadastrar)){
            try{
                if(
                        ((rbVivo.isChecked() && !rbMorto.isChecked())||(rbMorto.isChecked() && !rbVivo.isChecked())) &&
                        ((rbFemea.isChecked() && !rbMacho.isChecked()) || (!rbFemea.isChecked() && rbMacho.isChecked()))
                    ){
                    validator.validate();
                    if(validacaoOk) {
                        nascimentoHelper.salvar();
                        validacaoOk = false;
                    }
                }else {
                    Toast.makeText(ctx,"Selecionar Macho ou Femea, Morto ou Vivo",Toast.LENGTH_SHORT);
                }

            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }

        }
        if(view.equals(btnExcluir)){

        }
        if(view.equals(btnLimpar)){
            nascimentoHelper.limparCampos();
        }
        if(view.equals(btnPesquisar)){

        }
        if(view.equals(btnListar)){
            nascimentoHelper.setaRecyclerView(this.recyclerView);
            nascimentoHelper.preencherListView(this.recyclerView,this);
            tabHost.setCurrentTab(1);
        }
        if(view.equals(btnSelecBovino)){
            LayoutInflater li = getLayoutInflater();
            //inflamos o layout alerta.xml na view
            final View viewModalBovinoList = li.inflate(R.layout.modal_bovino_lista, null);
            nascimentoHelper.inflaterModalBovinoList(viewModalBovinoList, alerta, this);
            btnNovoBovino =(Button) viewModalBovinoList.findViewById(R.id.btnNovo);
            btnNovoBovino.setOnClickListener(this);
            btnOk =(Button) viewModalBovinoList.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(this);
        }
        if(view.equals(btnSelecMonta)){
            LayoutInflater li = getLayoutInflater();
            final View viewModalMontaList = li.inflate(R.layout.modal_monta_lista, null);
            nascimentoHelper.inflaterModalMontaList(viewModalMontaList, alerta, this);
            btnOk = (Button) viewModalMontaList.findViewById(R.id.btnOk);
            btnOk.setOnClickListener(this);

        }
        if(view.equals(btnOk)){
            nascimentoHelper.fecharModalList();

        }
        if(view.equals(btnNovoBovino)){
            BovinoHelper bovinoHelper = new BovinoHelper(ctx);
            fm = getFragmentManager();
            FragmentHelper fragmentHelper;
            fragmentHelper = new FragmentHelper(fm);
            BovinoModalFragment bovinoModalFragment = new BovinoModalFragment();

            LayoutInflater li = getLayoutInflater();
            final View view1 = li.inflate(R.layout.modal_bovino_crud, null);

            FrameLayout frameLayout2 = (FrameLayout) view1.findViewById(R.id.fragmentContentBovCrud);
            fragmentHelper.adicionaFragment(frameLayout2.getId(),bovinoModalFragment);
            DialogFragment dialogFragment = new DialogFragment();

            dialogFragment.onViewCreated(view1,null);
            dialogFragment.getShowsDialog();
            dialogFragment.show(fm.beginTransaction(),"teste");



        }

    }

    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "ABA LISTA":
                nascimentoHelper.setaRecyclerView(this.recyclerView);
                nascimentoHelper.preencherListView(this.recyclerView, this);
                break;
            default:
                break;
        }
    }

    @Override
    public void onCustomClick(Object object) {
        try{
            this.nascimento = (Nascimento)object;
            nascimentoHelper.setarCampos();
            tabHost.setCurrentTab(0);
        }catch (Exception e){

           try{
               Monta monta = (Monta) object;
               edtCodMonta.setText(monta.getIdMonta().toString());
               edtDtMonta.setText(DateUtil.formatFieldToTxt(monta.getDtMonta()));
               nascimentoHelper.fecharModalList();
           }catch (Exception e1){
               Bovino bovino = (Bovino) object;
               edtCodBovino.setText(bovino.getIdBovino().toString());
               edtNmBovino.setText(bovino.getApelido());
               nascimentoHelper.fecharModalList();
           }

        }

    }

    @Override
    public void onValidationSucceeded() {
        validacaoOk = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(ctx));
            }
            iterator.remove();
        }
    }


    public interface OnFragmentInteractionListener {

        void onFragmentInteraction(Uri uri);
    }

    public void passarReferenciaButton(final Button btnCadastrar, final Button btnEditar, final Button btnExcluir
            , final Button btnLimpar, final Button btnPesquisar, final Button btnListar, final TabHost tabHost,
                                       final RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnCadastrar.setOnClickListener(this);
        this.btnExcluir = btnExcluir;
        this.btnExcluir.setOnClickListener(this);
        this.btnLimpar = btnLimpar;
        this.btnLimpar.setOnClickListener(this);
        this.btnPesquisar = btnPesquisar;
        this.btnPesquisar.setOnClickListener(this);
        this.btnListar = btnListar;
        this.btnListar.setOnClickListener(this);
        this.btnEditar = btnEditar;
        this.btnEditar.setOnClickListener(this);
        this.tabHost = tabHost;
        this.tabHost.setOnTabChangedListener(this);
        this.recyclerView = recyclerView;


    }
}
