package com.brasilmilk.milkbrasil.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.R;

import java.util.List;

/**
 * Created by ricar on 20/12/2017.
 */

public class BovinoAdapter extends BaseAdapter {
    private Context ctx;
    private List<Bovino> listaBovino;

    public BovinoAdapter(Context act, List<Bovino> listaBovino) {
        this.ctx = act;
        this.listaBovino = listaBovino;
    }

    @Override
    public int getCount() {
        return listaBovino.size();
    }

    @Override
    public Object getItem(int i) {
        return listaBovino.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.adpater_bovino,null);

        TextView txtApelido = (TextView) view.findViewById(R.id.txtApelidoAd);
        TextView txtNrSisBov = (TextView) view.findViewById(R.id.txtNrSisBovAd);
        TextView txtDtInicio = (TextView) view.findViewById(R.id.txtDtInicioAd);
        TextView txtCodigo = (TextView) view.findViewById(R.id.txtCodAd);
        Bovino bov = listaBovino.get(position);
        if(bov != null){
            txtCodigo.setText("Cod: "+bov.getIdBovino());
            txtApelido.setText("Apelido: "+bov.getApelido());
            txtDtInicio.setText("Sexo: "+bov.getSexo());
            txtNrSisBov.setText("Nº SISBOV: "+bov.getNrManejosisbov());
        }


        return view;
    }
}
