package com.brasilmilk.milkbrasil.Interface;

import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TabHost;

/**
 * Created by ricar on 17/01/2018.
 */

public interface ButtonReferenceInterface {
    public void passarReferencia(final Button btnCadastrar, final Button btnEditar, final Button btnExcluir,
                                 final Button btnLimpar, final Button btnPesquisar, final Button btnListar,
                                 final TabHost tabHost, final RecyclerView recyclerView);
}
