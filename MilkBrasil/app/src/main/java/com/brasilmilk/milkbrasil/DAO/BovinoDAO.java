package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class BovinoDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Bovino> listaBovino;
    public BovinoDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Propriedade.class);
            scriptSQLDelete = Util.getDROPSQLite(Propriedade.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaBovino = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objBovino) {
        return super.insert(objBovino, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_BOVINO");
        return super.delete(this.db, Bovino.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_BOVINO");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdBovino"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaBovino.clear();
        listaBovino.addAll((Collection<? extends Bovino>) super.getAll(db, Bovino.class));
        return listaBovino;
    }

    public List<?> getLike(String nome) {
        listaBovino.clear();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("APELIDO");
            whereBuilder.append(" LIKE %?%");

            String where = whereBuilder.toString();
            String[]args = new String[]{nome};

            cursor = db.query(Util.getNomeTabela(Bovino.class), Util.getColumView(Bovino.class), where, args, null, null, null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Bovino.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, objLinha );
                    }
                    listaBovino.add((Bovino) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return listaBovino;
    }

    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        try {
            Integer paramCod = Integer.valueOf((String) param);
            compoList.add("ID_BOVINO");
        }catch (Exception e){
            compoList.add("APELIDO");
        }
        String[] parametros = new String[]{(String) param};
        return super.getByParam(db, Bovino.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Bovino.class);
    }


    public List<Bovino> getMatrizesByVazia() {
       listaBovino.clear();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT B.* FROM BOVINO B ");
            selectBuilder.append(" WHERE B.SEXO = 'F' ");
            selectBuilder.append(" AND B.FLGATIVO = 1  ");
            selectBuilder.append(" AND B.FLGVAZIA = 1 ");
            selectBuilder.append(" AND B.FLGDOADOR = 1 ");
            selectBuilder.append(" AND B.FLGBEZERROS012 = 0 ");
            selectBuilder.append(" AND B.ID_BOVINO NOT IN ( ");
            selectBuilder.append(" SELECT M.ID_MATRIZ FROM MONTA M  ");
            selectBuilder.append(" WHERE ");
            selectBuilder.append(" M.DTRESULTADO = 'null')");

            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Bovino.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, objLinha );
                    }
                    listaBovino.add((Bovino) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }

        return listaBovino;
    }
    public List<Bovino> getBovinoByNascimento(){

        listaBovino.clear();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT B.* FROM BOVINO B ");
            selectBuilder.append(" WHERE B.FLGATIVO = 1 ");


            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Bovino.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, objLinha );
                    }
                    listaBovino.add((Bovino) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }
        return  listaBovino;
    }


}
