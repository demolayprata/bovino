package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Cidade;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ricar on 25/11/2017.
 */

public class CidadeDAO extends AbstratroDAO implements InterfaceDAO {

    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Cidade> listaCidade;
    public CidadeDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Cidade.class);
            scriptSQLDelete = Util.getDROPSQLite(Cidade.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaCidade = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objCidade) {
        return super.insert(objCidade, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_CIDADE");
        return super.delete(this.db, Cidade.class, campoList, parametros);
    }

    @Override
    public boolean alterar(Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_CIDADE");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getCodCidade"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaCidade.clear();
        listaCidade.addAll((Collection<? extends Cidade>) super.getAll(db, Cidade.class));
        return  listaCidade;
    }

    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        String paramStr = String.valueOf (param);
        try {

            int paramCod = Integer.parseInt(paramStr);
            compoList.add("ID_CIDADE");
        }catch (Exception e){
            compoList.add("NM_CIDADE");
        }
        String[] parametros = new String[]{paramStr};
        return super.getByParam(db, Cidade.class, compoList, parametros);
    }

    @Override
    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Cidade.class);
    }
}
