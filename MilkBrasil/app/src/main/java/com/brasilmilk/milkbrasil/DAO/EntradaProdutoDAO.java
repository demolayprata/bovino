package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.EntradaProduto;
import com.brasilmilk.milkbrasil.Model.Produto;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Model.Semem;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class EntradaProdutoDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<EntradaProduto> listaEntradaProduto;
    public EntradaProdutoDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(EntradaProduto.class);
            scriptSQLDelete = Util.getDROPSQLite(EntradaProduto.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaEntradaProduto = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objBovino) {
        return super.insert(objBovino, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_ENTRADAPRODUTO");
        return super.delete(this.db, EntradaProduto.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_ENTRADAPRODUTO");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdEntradaBovino"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaEntradaProduto.clear();
        listaEntradaProduto.addAll((Collection<? extends EntradaProduto>) super.getAll(db, EntradaProduto.class));
        return listaEntradaProduto;
    }

    public List<?> getSememByEntradaProduto(String idEntradaProduto) {
        List<Semem> sememList = new ArrayList<>();
        List<Produto> produtoList = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("ID_ENTRADAPRODUTO");
            whereBuilder.append(" = ?");

            String where = whereBuilder.toString();
            String[]args = new String[]{idEntradaProduto};

            cursor = db.query(Util.getNomeTabela(EntradaProduto.class), Util.getColumView(EntradaProduto.class), where, args, null, null, null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(EntradaProduto.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(EntradaProduto.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Semem.class, objLinha );
                    }
                    listaEntradaProduto.add((EntradaProduto) objLinha);
                }
            }

            whereBuilder.setLength(0);
            whereBuilder.append("ID_PRODUTO");
            whereBuilder.append(" = ?");

            where = whereBuilder.toString();

            for(EntradaProduto entrada : listaEntradaProduto){
                args = new String[]{entrada.getIdProduto().toString()};
                cursor = db.query(Util.getNomeTabela(Semem.class), Util.getColumView(Semem.class), where, args, null, null, null);
                if(cursor.getCount()>0){
                    HashMap<String, String> mapKey = Util.getColumSet(Semem.class);
                    while (cursor.moveToNext()){
                        Object objLinha = Util.createNewInstance(Semem.class);
                        for(Map.Entry<String, String> entry : mapKey.entrySet()){
                            Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Semem.class, objLinha );
                        }
                        sememList.add((Semem) objLinha);
                    }
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return sememList;
    }

    public List<?> getProdutoByEntradaProduto(String idEntradaProduto) {
        List<Produto> produtoList = new ArrayList<>();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("ID_ENTRADAPRODUTO");
            whereBuilder.append(" = ?");

            String where = whereBuilder.toString();
            String[]args = new String[]{idEntradaProduto};

            cursor = db.query(Util.getNomeTabela(EntradaProduto.class), Util.getColumView(EntradaProduto.class), where, args, null, null, null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(EntradaProduto.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(EntradaProduto.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Semem.class, objLinha );
                    }
                    listaEntradaProduto.add((EntradaProduto) objLinha);
                }
            }

            whereBuilder.setLength(0);
            whereBuilder.append("ID_PRODUTO");
            whereBuilder.append(" = ?");

            where = whereBuilder.toString();

            for(EntradaProduto entrada : listaEntradaProduto){
                args = new String[]{entrada.getIdProduto().toString()};
                cursor = db.query(Util.getNomeTabela(Produto.class), Util.getColumView(Produto.class), where, args, null, null, null);
                if(cursor.getCount()>0){
                    HashMap<String, String> mapKey = Util.getColumSet(Produto.class);
                    while (cursor.moveToNext()){
                        Object objLinha = Util.createNewInstance(Produto.class);
                        for(Map.Entry<String, String> entry : mapKey.entrySet()){
                            Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Produto.class, objLinha );
                        }
                        produtoList.add((Produto) objLinha);
                    }
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return produtoList;
    }

    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        try {
            Integer paramCod = Integer.valueOf((String) param);
            compoList.add("ID_ENTRADAPRODUTO");
        }catch (Exception e){
            compoList.add("APELIDO");
        }
        String[] parametros = new String[]{(String) param};
        return super.getByParam(db, EntradaProduto.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(EntradaProduto.class);
    }

}
