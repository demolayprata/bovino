package com.brasilmilk.milkbrasil.Fragments;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;

import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * A simple {@link Fragment} subclass.
 */
public class MapFazendaFragment extends SupportMapFragment implements OnMapReadyCallback, GoogleMap.OnMapClickListener {


    // declaração da interface de comunicação
    public interface InterfaceComunicao {

        // aqui, um ou mais métodos de comunicação
        void getLocation(LatLng provider);
        void setLocation(Integer latitude, Integer longitude);
    }
    private InterfaceComunicao listener;
    private String provider;
    private GoogleMap mMap;
    private LocationManager locationManager;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getMapAsync(this);

    }

    @Override
    public void onResume() {

        super.onResume();

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        locationManager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);

        try {

            mMap = googleMap;
            mMap.setOnMapClickListener(this);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            mMap.setMyLocationEnabled(true);
        }catch (SecurityException e){
            Log.e("Erro:", e.getMessage());
        }

        // Add a marker in Sydney and move the camera
        @SuppressLint("MissingPermission") Location location =  locationManager.getLastKnownLocation(provider);
        LatLng localidade = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions marker = new MarkerOptions();
        marker.draggable(true);
        marker.position(localidade);
        marker.title("Local Propriedade");

        mMap.addMarker(marker);
        mMap.moveCamera(CameraUpdateFactory.newLatLng(localidade));
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                getLatLong(marker.getPosition());
            }
        });
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Toast.makeText(getContext(),"coordenadas: "+ latLng.toString(),Toast.LENGTH_SHORT).show();
    }

    /*
    onAttach faz parte do ciclo de vida do fragment, é executado
    quando o fragment é associado à activity
        */
    @Override
    public void onAttach(Activity activity) {

        super.onAttach(activity);
        if(activity instanceof InterfaceComunicao){
            listener = (InterfaceComunicao) activity;
        }else {
            Log.e("Erro", String.valueOf(new ClassCastException()));
        }
    }
    public void getLatLong(LatLng location){

        listener.getLocation(location);

    }
    public void setLocation(Integer latitude, Integer longitude){

    }

}
