package com.brasilmilk.milkbrasil.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TabHost;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceMenuMontaInterface;
import com.brasilmilk.milkbrasil.R;

public class MenuMontaFragment extends Fragment {

    private Button btnLancarMonta, btnLancarResultadoMonta;

    private ButtonReferenceMenuMontaInterface buttonReferenceListener;
    private Context ctx;
    public MenuMontaFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu_monta, container, false);
        this.btnLancarMonta = view.findViewById(R.id.btnLancarMonta);
        this.btnLancarResultadoMonta = view.findViewById(R.id.btnLancarResultado);


        buttonReferenceListener.passarReferencia(btnLancarMonta, btnLancarResultadoMonta);
        ctx = inflater.getContext();
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ButtonReferenceMenuMontaInterface){
            buttonReferenceListener = (ButtonReferenceMenuMontaInterface) context;
        }


    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
