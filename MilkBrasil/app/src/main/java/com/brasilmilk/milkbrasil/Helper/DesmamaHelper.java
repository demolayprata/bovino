package com.brasilmilk.milkbrasil.Helper;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Activity.PropriedadeCadastro;
import com.brasilmilk.milkbrasil.Adapter.AdapterDesmamaRecView;
import com.brasilmilk.milkbrasil.Adapter.AdapterNascimentoRecView;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.DAO.DesmamaDAO;
import com.brasilmilk.milkbrasil.DAO.NascimentoDAO;
import com.brasilmilk.milkbrasil.DAO.PropriedadeDAO;
import com.brasilmilk.milkbrasil.DAO.RacaDAO;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.Util;

import java.sql.Date;
import java.util.List;

/**
 * Created by ricar on 24/12/2017.
 */

public class DesmamaHelper  implements View.OnClickListener{
    private Context ctx;
    private DesmamaDAO desmamaDAO;
    private AdapterDesmamaRecView desmamaAdapter;
    private List<Desmama> desmamaListt;
    private RecyclerView.LayoutManager mLayoutManager;

    private EditText edtDtDesmama, edtIdDesmama, edtIdade, edttObs;
    private Button btnSalvar, btnCancelar;

    private AlertDialog alertaDesmamaHelper;

    public DesmamaHelper(Context ctx){
        this.ctx = ctx;

    }
    public void preencherListView(final RecyclerView mRecyclerView, String idBovino, ClickRecyclerView_Interface clickRecyclerView_Interface) {
//        if(idBovino.length()>0){
            desmamaDAO = new DesmamaDAO(ctx);
            desmamaListt = (List<Desmama>) desmamaDAO.getAll();

            desmamaAdapter = new AdapterDesmamaRecView(ctx, desmamaListt, clickRecyclerView_Interface);
           // mRecyclerView.removeAllViewsInLayout();
            mRecyclerView.setAdapter(desmamaAdapter);
//        }
    }


    @RequiresApi(api = Build.VERSION_CODES.O)
    @SuppressLint("WrongViewCast")
    public void inflaterModalDesmamaCrud(View view, AlertDialog alerta, final Date dateNascimento,  Desmama desmamaObj) {
        this.alertaDesmamaHelper = alerta;
        this.edtDtDesmama = view.findViewById(R.id.edtDtDesmama);
       // this.edtIdDesmama  = view.findViewById(R.id.txtIdDesmama);
        this.edtIdade = view.findViewById(R.id.edtIdadeDesmama);
        this.edttObs = view.findViewById(R.id.edtObsDesmama);

        setarMascara(edtDtDesmama);
        edtIdade.setEnabled(false);


        if(desmamaObj.getIdade() != null){
            if(!desmamaObj.getIdade().equals("")) {
                setarCampos(desmamaObj);
            }
        }

        edtDtDesmama.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try{
                    if(edtDtDesmama.getText().toString().length() ==10) {
                        Date dateDesmama = DateUtil.covertToDateSql(edtDtDesmama.getText().toString());
                        edtIdade.setText(DateUtil.calculaIdadeByDesmama(dateNascimento, dateDesmama));
                    }
                }catch (Exception e){
                    Log.e("Erro: ", e.getMessage());
                    edtIdade.setText("");
                }
            }
        });

        this.btnCancelar = view.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
        this.btnSalvar = view.findViewById(R.id.btnSalvar);
       // btnSalvar.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("LANÇAR DESMAMA");
        builder.setView(view);
        alertaDesmamaHelper = builder.create();
        alertaDesmamaHelper.show();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        if(view.equals(btnCancelar)){
            alertaDesmamaHelper.dismiss();
        }
        if(view.equals(btnSalvar)){
            if(edtIdade.getText().toString().equals("") || edtDtDesmama.getText().toString().equals("")){
                Toast.makeText(ctx,"Campos Idade e Data Desmama Obrigatorio", Toast.LENGTH_SHORT);
            }else {
                Desmama desmamaObjSalvar = new Desmama();
                recuperarCampos(desmamaObjSalvar);
                desmamaDAO = new DesmamaDAO(ctx);
                desmamaDAO.salvar(desmamaObjSalvar);
                alertaDesmamaHelper.dismiss();
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void recuperarCampos(final Desmama desamaObj){
        try{
            desamaObj.setDtDesmama(DateUtil.covertToDateSql(edtDtDesmama.getText().toString()));
            desamaObj.setIdade(edtIdade.getText().toString());
            desamaObj.setObs(edttObs.getText().toString());
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    public void  setarCampos(Desmama desmamaObjs){
       // edtIdDesmama.setText(desmamaObjs.getIdDesmama().toString());
        edtDtDesmama.setText(DateUtil.formatFieldToTxt(desmamaObjs.getDtDesmama()));
        edtIdade.setText(desmamaObjs.getIdade());
        edttObs.setText(desmamaObjs.getObs());
    }
    public void setarMascara(final EditText edtDtDesmam){
        Util.setMaskEditText(edtDtDesmama, Mask.DATA.getMascara());
    }

    public boolean pesquisaDesmama(Integer idDesmama){
        boolean isDesmama = false;
        if(!idDesmama.equals(null)){
            desmamaDAO = new DesmamaDAO(ctx);
            Desmama desmamaObj = (Desmama) desmamaDAO.getById(idDesmama);
            if(!desmamaObj.equals(null)){
                setarCampos(desmamaObj);
                isDesmama = true;
            }
        }
        return  isDesmama;
    }

    public int salvar(Desmama desmamaObj){
            desmamaDAO = new DesmamaDAO(ctx);
            int idDesmama = desmamaDAO.salvar(desmamaObj);
            return idDesmama;
    }

    public void closeModalDesmamaCrud(){
        alertaDesmamaHelper.dismiss();
    }


}
