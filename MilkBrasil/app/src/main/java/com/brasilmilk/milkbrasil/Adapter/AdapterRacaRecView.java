package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterRacaRecView extends  RecyclerView.Adapter<AdapterRacaRecView.RecyclerTesteViewHolder> {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Raca> mList;




public AdapterRacaRecView(Context ctx, List<Raca> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;

        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_raca,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        Raca raca = mList.get(position);

            viewHolder.txtDesAbrevRaca.setText(raca.getDesAbrev());
            viewHolder.txtCodRaca.setText( raca.getIdRaca().toString());
            viewHolder.txtNomeRaca.setText( raca.getNomeRaca());
            viewHolder.txtObsRaca.setText( raca.getObs());
        }

@Override
public int getItemCount() {
        return mList.size();
        }


protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

    protected  TextView txtDesAbrevRaca, txtCodRaca, txtNomeRaca, txtObsRaca;

    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtDesAbrevRaca = itemView.findViewById(R.id.txtDesAbrevRaca);
        txtCodRaca = itemView.findViewById(R.id.txtCodRaca);
        txtNomeRaca = itemView.findViewById(R.id.txtNomeRaca);
        txtObsRaca = itemView.findViewById(R.id.txtObsRaca);




        //Setup the click listener
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));

            }
        });
    }
}
}