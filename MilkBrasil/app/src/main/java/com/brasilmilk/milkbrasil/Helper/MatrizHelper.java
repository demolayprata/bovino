package com.brasilmilk.milkbrasil.Helper;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Adapter.AdapterMatrizesRecView;
import com.brasilmilk.milkbrasil.Adapter.BovinoAdapter;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.Enum.IdadeBovinoEnum;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.Util;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricar on 25/12/2017.
 */

public class MatrizHelper implements ClickRecyclerView_Interface, View.OnClickListener{
    private Context ctx;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private AlertDialog.Builder builder;
    private AlertDialog alertaBovinoHelper;

    private EditText  edtCodMatriz, edtNmMatriz;

    private AdapterMatrizesRecView matrizesAdapter;
    //////////////////////

    private CheckBox chkAtivo;

    //private int ano, mes, dia;
    private Calendar cal;

    private Locale mLocale;
    private Bovino bovinoObj;

    private Button btnOk;


    ////////////
    private  DesmamaHelper desmamaHelper;


    public MatrizHelper(Context ctx, final EditText edtNmMatriz, final EditText edtCodMatriz ){
        this.ctx = ctx;
        this.edtCodMatriz = edtCodMatriz;
        this.edtNmMatriz = edtNmMatriz;
    }

    @Override
    public void onCustomClick(Object object) {

            bovinoObj = (Bovino) object;
            edtNmMatriz.setText(bovinoObj.getApelido());
            edtCodMatriz.setText(bovinoObj.getIdBovino().toString());

        alertaBovinoHelper.dismiss();
    }



    public  void inflaterModalMatrizesList(final View view,  AlertDialog alerta){
        this.alertaBovinoHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewMatrizes);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        preencherListView(mRecyclerView,  this);

        this.btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR MATRIZ");
        builder.setView(view);
        alertaBovinoHelper = builder.create();
        alertaBovinoHelper.show();

    }

    public void preencherListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {
        //  if(idBovino.length()>0){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        List<Bovino> bovinoList = (List<Bovino>) bovinoDAO.getMatrizesByVazia();

        matrizesAdapter = new AdapterMatrizesRecView(ctx, bovinoList, clickRecyclerView_Interface);
        // mRecyclerView.removeAllViewsInLayout();
        mRecyclerView.setAdapter(matrizesAdapter);
        //    }
    }


    @Override
    public void onClick(View view) {
        if(view.equals(btnOk)){
            if(alertaBovinoHelper != null){
                alertaBovinoHelper.dismiss();
            }
        }
    }
}
