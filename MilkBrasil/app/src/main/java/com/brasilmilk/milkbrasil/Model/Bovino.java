package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;
import com.brasilmilk.milkbrasil.Enum.IdadeBovinoEnum;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by ricar on 13/12/2017.
 */
@ClassAnotation(nomeTabela = "BOVINO")
public class Bovino {
    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "ID_BOVINO", tipoAtributo = "INTEGER", nomeSet = "setIdBovino", nomeGet = "getIdBovino", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
    private Integer idBovino;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "APELIDO", nomeGet = "getApelido", nomeSet = "setApelido", notNull = true)
    private String apelido;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "CODBARRAS", nomeSet = "setCodBarras", nomeGet = "getCodBarras")
    private String codBarras;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "CORPELAGEM", nomeGet = "getCorPelagem", nomeSet = "setCorPelagem")
    private String corPelagem;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTDESMAMA", nomeGet = "getDtDesmama", nomeSet = "setDtDesmama")
    private Date dtDesmama;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTENTRADA", nomeGet = "getDtEntrada", nomeSet = "setDtEntrada")
    private Date dtEntrada;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTINICIO", nomeGet = "getDtInicio", nomeSet = "setDtInicio")
    private Date dtInicio;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTNASCIMENTO", nomeGet = "getDtNascimento", nomeSet = "setDtNascimento")
    private Date dtNascimento;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGATIVO", tipoAtributo = "INTEGER",nomeGet = "isFlgAtivo", nomeSet = "setFlgAtivo")
    private boolean flgAtivo;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "NRCHIPELETRONICO", nomeGet = "getNrChipEletronico", nomeSet = "setNrChipEletronico")
    private String nrChipEletronico;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "NRMANEJOSISBOV", nomeSet = "setNrManejosisbov", nomeGet = "getNrManejosisbov")
    private String nrManejosisbov;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "SEXO", nomeGet = "getSexo", nomeSet = "setSexo")
    private String sexo;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "VLRCUSTO", nomeGet = "getVlCusto", nomeSet = "setVlCusto", tipoAtributo = "DOUBLE")
    private BigDecimal vlCusto;


    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS012", tipoAtributo = "INTEGER",nomeGet = "isFlgBezerros012", nomeSet = "setFlgBezerros012")
    private boolean flgBezerros012;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS1324", tipoAtributo = "INTEGER",nomeGet = "isFlgNovilhos1324", nomeSet = "setFlgNovilhos1324")
    private boolean flgNovilhos1324;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS2536", tipoAtributo = "INTEGER",nomeGet = "isFlgNovilhos2536", nomeSet = "setFlgNovilhos2536")
    private boolean flgNovilhos2536;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROSACIMA36",tipoAtributo = "INTEGER", nomeGet = "isFlgNovilhos2536", nomeSet = "setFlgNovilhos2536")
    private boolean flgNovilhosAcima36;


    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS012DESMAMA", tipoAtributo = "INTEGER",nomeGet = "isFlgBezerros012Desmama", nomeSet = "setFlgBezerros012Desmama")
    private boolean flgBezerros012Desmama;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS1324DESMAMA", tipoAtributo = "INTEGER",nomeGet = "isFlgNovilhos1324Desmama", nomeSet = "setFlgNovilhos1324Desmama")
    private boolean flgNovilhos1324Desmama;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROS2536DESMAMA", tipoAtributo = "INTEGER",nomeGet = "isFlgNovilhos2536Desmama", nomeSet = "setFlgNovilhos2536Desmama")
    private boolean flgNovilhos2536Desmama;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGBEZERROSACIMA36DESMAMA",tipoAtributo = "INTEGER", nomeGet = "isFlgNovilhos2536Desmama", nomeSet = "setFlgNovilhos2536Desmama")
    private boolean flgNovilhosAcima36Desmama;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGDOADOR", tipoAtributo = "INTEGER",nomeGet = "isFlgDoador", nomeSet = "setFlgDoador")
    private boolean flgDoador;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGLEITE", tipoAtributo = "INTEGER",nomeGet = "isFlgLeite", nomeSet = "setFlgLeite")
    private boolean flgLeite;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGENGORDA", tipoAtributo = "INTEGER",nomeGet = "isFlgEngorda", nomeSet = "setFlgEngorda")
    private boolean flgEngorda;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGFERTILIDADE",tipoAtributo = "INTEGER", nomeGet = "isFlgFertilidade", nomeSet = "setFlgFertilidade")
    private boolean flgFertilidade;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGMAMANDO", tipoAtributo = "INTEGER",nomeGet = "isFlgMamando", nomeSet = "setFlgMamando")
    private boolean flgMamando;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGVAZIA", tipoAtributo = "INTEGER",nomeGet = "isFlgVazia", nomeSet = "setFlgVazia")
    private boolean flgVazia;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "FLGCHEIA", tipoAtributo = "INTEGER",nomeGet = "isFlgCheia", nomeSet = "setFlgCheia")
    private boolean flgCheia;


    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "ID_RACA",tipoAtributo = "INTEGER", nomeGet = "getIdRaca", nomeSet = "setIdRaca",  forengKey = true, referencesTable = "RACA", fieldReferences = "ID_RACA")
    private Integer idRaca;


    @MetodoAnotation(tipoRetorno = "Integer")
    public Integer getIdBovino() {
        return idBovino;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setIdBovino(Integer idBovino) {
        this.idBovino = idBovino;
    }

    @MetodoAnotation()
    public String getApelido() {
        return apelido;
    }

    @MetodoAnotation()
    public void setApelido(String apelido) {
        this.apelido = apelido;
    }
    @MetodoAnotation()
    public String getCodBarras() {
        return codBarras;
    }

    @MetodoAnotation()
    public void setCodBarras(String codBarras) {
        this.codBarras = codBarras;
    }

    @MetodoAnotation()
    public String getCorPelagem() {
        return corPelagem;
    }

    @MetodoAnotation()
    public void setCorPelagem(String corPelagem) {
        this.corPelagem = corPelagem;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtDesmama() {
        return dtDesmama;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtDesmama(Date dtDesmama) {
        this.dtDesmama = dtDesmama;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtEntrada() {
        return dtEntrada;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtEntrada(Date dtEntrada) {
        this.dtEntrada = dtEntrada;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtInicio() {
        return dtInicio;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtInicio(Date dtInicio) {
        this.dtInicio = dtInicio;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtNascimento() {
        return dtNascimento;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtNascimento(Date dtNascimento) {
        this.dtNascimento = dtNascimento;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgAtivo() {
        return flgAtivo;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgAtivo(boolean flgAtivo) {
        this.flgAtivo = flgAtivo;
    }

    @MetodoAnotation()
    public String getNrChipEletronico() {
        return nrChipEletronico;
    }

    @MetodoAnotation()
    public void setNrChipEletronico(String nrChipEletronico) {
        this.nrChipEletronico = nrChipEletronico;
    }

    @MetodoAnotation()
    public String getNrManejosisbov() {
        return nrManejosisbov;
    }

    @MetodoAnotation()
    public void setNrManejosisbov(String nrManejosisbov) {
        this.nrManejosisbov = nrManejosisbov;
    }

    @MetodoAnotation()
    public String getSexo() {
        return sexo;
    }

    @MetodoAnotation()
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public BigDecimal getVlCusto() {
        return vlCusto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public void setVlCusto(BigDecimal vlCusto) {
        this.vlCusto = vlCusto;
    }



    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgBezerros012() {
        return flgBezerros012;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgBezerros012(boolean flgBezerros012) {
        this.flgBezerros012 = flgBezerros012;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhos1324() {
        return flgNovilhos1324;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhos1324(boolean flgNovilhos1324) {
        this.flgNovilhos1324 = flgNovilhos1324;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhos2536() {
        return flgNovilhos2536;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhos2536(boolean flgNovilhos2536) {
        this.flgNovilhos2536 = flgNovilhos2536;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhosAcima36() {
        return flgNovilhosAcima36;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhosAcima36(boolean flgNovilhosAcima36) {
        this.flgNovilhosAcima36 = flgNovilhosAcima36;
    }


    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgDoador() {
        return flgDoador;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgDoador(boolean flgDoador) {
        this.flgDoador = flgDoador;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgFertilidade() {
        return flgFertilidade;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgFertilidade(boolean flgFertilidade) {
        this.flgFertilidade = flgFertilidade;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgMamando() {
        return flgMamando;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgMamando(boolean flgMamando) {
        this.flgMamando = flgMamando;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgVazia() {
        return flgVazia;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgVazia(boolean flgVazia) {
        this.flgVazia = flgVazia;
    }





    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getIdRaca() {
        return idRaca;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setIdRaca(Integer idRaca) {
        this.idRaca = idRaca;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgBezerros012Desmama() {
        return flgBezerros012Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgBezerros012Desmama(boolean flgBezerros012Desmama) {
        this.flgBezerros012Desmama = flgBezerros012Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhos1324Desmama() {
        return flgNovilhos1324Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhos1324Desmama(boolean flgNovilhos1324Desmama) {
        this.flgNovilhos1324Desmama = flgNovilhos1324Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhos2536Desmama() {
        return flgNovilhos2536Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhos2536Desmama(boolean flgNovilhos2536Desmama) {
        this.flgNovilhos2536Desmama = flgNovilhos2536Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgNovilhosAcima36Desmama() {
        return flgNovilhosAcima36Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgNovilhosAcima36Desmama(boolean flgNovilhosAcima36Desmama) {
        this.flgNovilhosAcima36Desmama = flgNovilhosAcima36Desmama;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgLeite() {
        return flgLeite;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgLeite(boolean flgLeite) {
        this.flgLeite = flgLeite;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgEngorda() {
        return flgEngorda;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgEngorda(boolean flgEngorda) {
        this.flgEngorda = flgEngorda;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isFlgCheia() {
        return flgCheia;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setFlgCheia(boolean flgCheia) {
        this.flgCheia = flgCheia;
    }

    public String getIdadeBovino(){
        String idadeBov = "";
        if(this.flgBezerros012){
            idadeBov = IdadeBovinoEnum.BEZERROS_0_A_12_MESES.getIdadeBovino();
        }else
        if(this.flgNovilhos1324){
            idadeBov = IdadeBovinoEnum.NOVILHOS_13_A_24_MESES.getIdadeBovino();
        }else
        if(this.flgNovilhos2536){
            idadeBov = IdadeBovinoEnum.NOVILHOS_25_A_36_MESES.getIdadeBovino();
        }else {
            idadeBov = IdadeBovinoEnum.NOVILHOS_ACIMA_37_MESES.getIdadeBovino();
        }
        return idadeBov;
    }
}
