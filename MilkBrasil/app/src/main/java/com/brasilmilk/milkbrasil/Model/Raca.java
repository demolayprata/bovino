package com.brasilmilk.milkbrasil.Model;
import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;



/**
 * The persistent class for the raca database table.
 * 
 */
@ClassAnotation(nomeTabela = "RACA")
public class Raca {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_RACA", tipoAtributo = "INTEGER", nomeSet = "setIdRaca", nomeGet = "getIdRaca", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idRaca;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DESABREV", nomeGet = "getDesAbrev", nomeSet = "setDesAbrev")
	private String desAbrev;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "NOMERACA", nomeGet = "getNomeRaca", nomeSet = "setNomeRaca")
	private String nomeRaca;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "OBS", nomeGet = "getObs", nomeSet = "setObs")
	private String obs;


	public Raca() {
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdRaca() {
		return idRaca;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdRaca(Integer idRaca) {
		this.idRaca = idRaca;
	}

	@MetodoAnotation()
	public String getDesAbrev() {
		return desAbrev;
	}

	@MetodoAnotation()
	public void setDesAbrev(String desAbrev) {
		this.desAbrev = desAbrev;
	}

	@MetodoAnotation()
	public String getNomeRaca() {
		return nomeRaca;
	}

	@MetodoAnotation()
	public void setNomeRaca(String nomeRaca) {
		this.nomeRaca = nomeRaca;
	}

	@MetodoAnotation()
	public String getObs() {
		return obs;
	}

	@MetodoAnotation()
	public void setObs(String obs) {
		this.obs = obs;
	}
}