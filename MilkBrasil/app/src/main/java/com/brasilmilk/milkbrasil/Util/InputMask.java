package com.brasilmilk.milkbrasil.Util;

import android.util.Log;
import android.widget.EditText;

import com.brasilmilk.milkbrasil.MainActivity;
import com.redmadrobot.inputmask.MaskedTextChangedListener;

/**
 * Created by ricar on 24/11/2017.
 */

public class InputMask {
    public static MaskedTextChangedListener getMask(EditText editText, String mascara){
        return  new MaskedTextChangedListener(mascara, true, editText, null, new MaskedTextChangedListener.ValueListener() {
            @Override
            public void onTextChanged(boolean b, String s) {
              Log.d(MainActivity.class.getSimpleName(), String.valueOf(b));
              Log.d(MainActivity.class.getSimpleName(), s);
            }
        });
    }

}
