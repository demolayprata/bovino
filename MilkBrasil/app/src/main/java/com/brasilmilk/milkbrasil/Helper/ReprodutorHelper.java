package com.brasilmilk.milkbrasil.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.brasilmilk.milkbrasil.Adapter.AdapterMatrizesRecView;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.DAO.SememDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.R;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricar on 25/12/2017.
 */

public class ReprodutorHelper implements ClickRecyclerView_Interface, View.OnClickListener{
    private Context ctx;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private AlertDialog.Builder builder;
    private AlertDialog alertaReprodutorHelper;

    private EditText edtCodReprodutor, edtNmReprodutor;

    private AdapterMatrizesRecView matrizesAdapter;
    private Button btnOk;
    //////////////////////

    //private int ano, mes, dia;
    private Calendar cal;

    private Locale mLocale;
    private Bovino bovinoObj;



    public ReprodutorHelper(Context ctx, final EditText edNmReprodutor, final EditText edtCodReprodutor ){
        this.ctx = ctx;
        this.edtCodReprodutor = edtCodReprodutor;
        this.edtNmReprodutor = edNmReprodutor;
    }

    @Override
    public void onCustomClick(Object object) {

            bovinoObj = (Bovino) object;
            edtNmReprodutor.setText(bovinoObj.getApelido());
            edtCodReprodutor.setText(bovinoObj.getIdBovino().toString());

        alertaReprodutorHelper.dismiss();
    }



    public  void inflaterModalMatrizesList(final View view,  AlertDialog alerta){
        this.alertaReprodutorHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewMatrizes);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        preencherListView(mRecyclerView,  this);
        this.btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR REPRODUTOR");
        builder.setView(view);
        alertaReprodutorHelper = builder.create();
        alertaReprodutorHelper.show();

    }

    public void preencherListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {
        //  if(idBovino.length()>0){
        SememDAO sememDAO = new SememDAO(ctx);
        List<Bovino> bovinoList = (List<Bovino>) sememDAO.getReprodutoresBySemem();

        matrizesAdapter = new AdapterMatrizesRecView(ctx, bovinoList, clickRecyclerView_Interface);
        // mRecyclerView.removeAllViewsInLayout();
        mRecyclerView.setAdapter(matrizesAdapter);
        //    }
    }


    @Override
    public void onClick(View view) {
        if(view.equals(btnOk)){
            if(alertaReprodutorHelper != null){
                alertaReprodutorHelper.dismiss();
            }
        }
    }
}
