package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterMontaRecView extends  RecyclerView.Adapter<AdapterMontaRecView.RecyclerTesteViewHolder>   {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Monta> mList;





public AdapterMontaRecView(Context ctx, List<Monta> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;


        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_monta,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        final Monta monta = mList.get(position);

        viewHolder.txtCodMonta.setText(monta.getIdMonta().toString());
        viewHolder.txtDtMonta.setText(DateUtil.formatFieldToTxt(monta.getDtMonta()));
        viewHolder.txtResultado.setText( !monta.isFlgCheia() && !monta.isFlgVazia()?"AGUARDANDO RESULTADO":monta.isFlgCheia()?"CHEIA":"VAZIA");

        }


@Override
public int getItemCount() {
        return mList.size();
        }


    protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

        protected TextView txtCodMonta, txtDtMonta, txtResultado;


    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtCodMonta = itemView.findViewById(R.id.txtCodMonta);
        txtDtMonta = itemView.findViewById(R.id.txtDtMonta);
        txtResultado = itemView.findViewById(R.id.txtResultadoMonta);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));
            }
        });

    }
}


}