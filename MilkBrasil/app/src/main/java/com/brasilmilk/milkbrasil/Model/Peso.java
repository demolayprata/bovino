package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;


@ClassAnotation(nomeTabela = "PESO")
public class Peso {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_PESO", tipoAtributo = "INTEGER", nomeSet = "setIdPeso", nomeGet = "getIdPeso", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true, unique = true)
	private Integer idPeso;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTPESAGEM", nomeGet = "getDtPesagem", nomeSet = "setDtPesagem", notNull = true, unique = true)
	private Date dtPesagem;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDQUILOS", nomeGet = "getQtdQuilos", nomeSet = "setQtdQuilos", tipoAtributo = "DOUBLE", notNull = true)
	private BigDecimal qtdQuilos;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_BOVINO", nomeGet = "getIdBovino", nomeSet = "setIdBovino", tipoAtributo = "INTEGER", notNull = true, forengKey = true, referencesTable = "BOVINO", fieldReferences = "ID_BOVINO", cascade = true)
	private Integer idBovino;

	public Peso() {
	}
	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdPeso() {
		return this.idPeso;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdPeso(Integer idPeso) {
		this.idPeso = idPeso;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtPesagem() {
		return this.dtPesagem;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtPesagem(Date dtPesagem) {
		this.dtPesagem = dtPesagem;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdQuilos() {
		return this.qtdQuilos;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdQuilos(BigDecimal qtdQuilos) {
		this.qtdQuilos = qtdQuilos;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdBovino() {
		return idBovino;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdBovino(Integer bovinos) {
		this.idBovino = bovinos;
	}





}