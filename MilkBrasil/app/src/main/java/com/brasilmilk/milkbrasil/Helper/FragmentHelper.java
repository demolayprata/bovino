package com.brasilmilk.milkbrasil.Helper;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.EditText;

import com.brasilmilk.milkbrasil.R;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

/**
 * Created by ricar on 12/01/2018.
 */

public class FragmentHelper {


     private FragmentManager fm;

     public FragmentHelper(final FragmentManager fm){
         this.fm = fm;
     }


    public void adicionaFragment(int containerId, Fragment fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(containerId, fragment);
        ft.commit();
    }

    public  void substituiFragment(int containerId, Fragment fragment) {
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.fragment_content, fragment);
        ft.commit();
     }

    public void removeFragment(int containerId) {
        Fragment fragment = fm.findFragmentById(containerId);
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commit();
    }


}
