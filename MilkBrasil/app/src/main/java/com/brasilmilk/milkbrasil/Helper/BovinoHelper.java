package com.brasilmilk.milkbrasil.Helper;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.renderscript.BaseObj;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Activity.BovinoCadastro;
import com.brasilmilk.milkbrasil.Adapter.AdapterMatrizesRecView;
import com.brasilmilk.milkbrasil.Adapter.AdapterRacaRecView;
import com.brasilmilk.milkbrasil.Adapter.BovinoAdapter;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.DAO.PesoDAO;
import com.brasilmilk.milkbrasil.Enum.IdadeBovinoEnum;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.Model.Semem;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.Util;

import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricar on 25/12/2017.
 */

public class BovinoHelper implements ClickRecyclerView_Interface,  View.OnClickListener, RadioGroup.OnCheckedChangeListener{
    private Context ctx;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private AlertDialog.Builder builder;
    private AlertDialog alertaBovinoHelper;
    private  RacaHelper racaHelper;
    private  Button btnCancelar;
    private EditText edtNmRaca, edtIdRaca, edtIdBovino, edtApelido, edtCodBarras, edtCorPelagem, edtDtDesmama, edtDtEntrada, edtDtInicio, edtDtNascimento,
            edtNrEletronico, edtNrManejoSisBov, edtVlrCusto, edtCodMatriz, edtNmMatriz, edtCodReprodutor, edtNmReprodutor;
    private String sexo = "M";
    private AdapterMatrizesRecView matrizesAdapter;
    //////////////////////

    private CheckBox chkAtivo, chkLeite, chkEngorda, chkReprodutorMatriz;
    private RadioButton rbMacho, rbFemea, rbIdade0_12Meses, rbIdade13_24Meses, rbIdade25_36Meses, rbIdadeAcima_36Meses,  rbIdade0_12MesesDesmama, rbIdade13_24MesesDesmama, rbIdade25_36MesesDesmama,
            rbIdadeAcima_36MesesDesmama, rbCheia, rbVazia;
    private RadioGroup rdGroup, rdGroupIdadeDesmama, rdGroupIdadeBovino, rGroupStatus;

    //private int ano, mes, dia;
    private Calendar cal;

    private Locale mLocale;
    private Bovino bovinoObj;
    private Peso peso;



    ////////////
    private  DesmamaHelper desmamaHelper;

    @RequiresApi(api = Build.VERSION_CODES.N)
    public BovinoHelper(Context ctx, final EditText edtNmRaca, final EditText edtIdRaca, final EditText edtIdBovino,
                        final EditText edtApelido, final EditText edtCodBarras, final EditText edtCorPelagem,
                        final EditText edtDtDesmama, final EditText edtDtEntrada, final EditText edtDtInicio,
                        final EditText edtDtNascimento, final EditText edtNrEletronico, final EditText edtNrManejoSisBov,
                        final EditText edtVlrCusto, String sexo, final CheckBox chkAtivo, final RadioButton rbMacho,
                        final RadioButton rbFemea, final Calendar cal, final Locale mLocale, final  Bovino bovinoObj,
                        final RadioButton rbIdade0_12Meses, final RadioButton rbIdade13_24Meses,
                        final RadioButton rbIdade25_36Meses, final RadioButton rbIdadeAcima_36Meses,
                        final RadioButton rbIdade0_12MesesDesmama, final RadioButton rbIdade13_24MesesDesmama,
                        final RadioButton rbIdade25_36MesesDesmama, final RadioButton rbIdadeAcima_36MesesDesmama,
                        final RadioGroup rdGroup, final  RadioGroup rdGroupIdadeDesmama, final RadioGroup rdGroupIdadeBovino,
                        final CheckBox chkLeite, final CheckBox chkEngorda, final  CheckBox chkReprodutorMatriz,
                        final RadioGroup rGroupStatus, final RadioButton rbVazia, final RadioButton rbCheia) {
        this.ctx = ctx;
        this.edtNmRaca = edtNmRaca;
        this.edtIdRaca = edtIdRaca;
        this.edtIdBovino = edtIdBovino;
        this.edtApelido = edtApelido;
        this.edtCodBarras = edtCodBarras;
        this.edtCorPelagem = edtCorPelagem;
        this.edtDtDesmama = edtDtDesmama;
        this.edtDtEntrada = edtDtEntrada;
        this.edtDtInicio = edtDtInicio;
        this.edtDtNascimento = edtDtNascimento;
        this.edtNrEletronico = edtNrEletronico;
        this.edtNrManejoSisBov = edtNrManejoSisBov;
        this.edtVlrCusto = edtVlrCusto;
        this.sexo = sexo;
        this.chkAtivo = chkAtivo;
        this.rbMacho = rbMacho;
        this.rbFemea = rbFemea;
        this.cal = cal;
        this.mLocale = mLocale;
        this.bovinoObj = bovinoObj;

        this.rdGroup = rdGroup;
        this.rdGroup.setOnCheckedChangeListener(this);
        this.rdGroupIdadeDesmama = rdGroupIdadeDesmama;
        rdGroupIdadeDesmama.setOnCheckedChangeListener(this);
        this.rdGroupIdadeBovino = rdGroupIdadeBovino;
        rdGroupIdadeBovino.setOnCheckedChangeListener(this);
        this.rbIdade0_12Meses = rbIdade0_12Meses;

        this.rbIdade13_24Meses = rbIdade13_24Meses;
        this.rbIdade25_36Meses = rbIdade25_36Meses;
        this.rbIdadeAcima_36Meses = rbIdadeAcima_36Meses;
        this.rbIdade0_12MesesDesmama = rbIdade0_12MesesDesmama;
        this.rbIdade13_24MesesDesmama = rbIdade13_24MesesDesmama;
        this.rbIdade25_36MesesDesmama = rbIdade25_36MesesDesmama;
        this.rbIdadeAcima_36MesesDesmama = rbIdadeAcima_36MesesDesmama;

        this.chkEngorda = chkEngorda;
        this.chkLeite = chkLeite;
        this.chkReprodutorMatriz = chkReprodutorMatriz;

        this.rGroupStatus = rGroupStatus;
        this.rbCheia = rbCheia;
        this.rbVazia = rbVazia;
        this.rbVazia.setChecked(true);

        setarMascara(edtDtDesmama, edtDtEntrada, edtDtInicio, edtDtNascimento);

        limparCampos();
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public BovinoHelper(Context ctx, final EditText edtNmRaca, final EditText edtIdRaca, final EditText edtIdBovino,
                        final EditText edtApelido, final EditText edtCodBarras, final EditText edtCorPelagem,
                        final EditText edtDtEntrada, final EditText edtDtInicio,
                        final EditText edtDtNascimento, final EditText edtNrEletronico, final EditText edtNrManejoSisBov,
                        String sexo, final CheckBox chkAtivo, final RadioButton rbMacho,
                        final RadioButton rbFemea, final Calendar cal, final Locale mLocale, final  Bovino bovinoObj,
                        final RadioButton rbIdade0_12Meses, final RadioButton rbIdade13_24Meses,
                        final RadioButton rbIdade25_36Meses, final RadioButton rbIdadeAcima_36Meses,
                        final RadioGroup rdGroup, final RadioGroup rdGroupIdadeBovino,
                        final CheckBox chkLeite, final CheckBox chkEngorda, final  CheckBox chkReprodutorMatriz,
                        final RadioGroup rGroupStatus, final RadioButton rbVazia, final RadioButton rbCheia) {
        this.ctx = ctx;
        this.edtNmRaca = edtNmRaca;
        this.edtIdRaca = edtIdRaca;
        this.edtIdBovino = edtIdBovino;
        this.edtApelido = edtApelido;
        this.edtCodBarras = edtCodBarras;
        this.edtCorPelagem = edtCorPelagem;

        this.edtDtEntrada = edtDtEntrada;
        this.edtDtInicio = edtDtInicio;
        this.edtDtNascimento = edtDtNascimento;
        this.edtNrEletronico = edtNrEletronico;
        this.edtNrManejoSisBov = edtNrManejoSisBov;

        this.sexo = sexo;
        this.chkAtivo = chkAtivo;
        this.rbMacho = rbMacho;
        this.rbFemea = rbFemea;
        this.cal = cal;
        this.mLocale = mLocale;
        this.bovinoObj = bovinoObj;

        this.rdGroup = rdGroup;
        this.rdGroup.setOnCheckedChangeListener(this);

        this.rdGroupIdadeBovino = rdGroupIdadeBovino;
        rdGroupIdadeBovino.setOnCheckedChangeListener(this);
        this.rbIdade0_12Meses = rbIdade0_12Meses;

        this.rbIdade13_24Meses = rbIdade13_24Meses;
        this.rbIdade25_36Meses = rbIdade25_36Meses;
        this.rbIdadeAcima_36Meses = rbIdadeAcima_36Meses;


        this.chkEngorda = chkEngorda;
        this.chkLeite = chkLeite;
        this.chkReprodutorMatriz = chkReprodutorMatriz;

        this.rGroupStatus = rGroupStatus;
        this.rbCheia = rbCheia;
        this.rbVazia = rbVazia;
        this.rbVazia.setChecked(true);

        setarMascara(edtDtDesmama, edtDtEntrada, edtDtInicio, edtDtNascimento);

        limparCampos();
    }
    public BovinoHelper(Context ctx, final Bovino bovino){
        this.ctx = ctx;
        this.bovinoObj = bovino;
    }
    public BovinoHelper(Context ctx, final EditText edtNmRaca, final EditText edtIdRaca ){
        this.ctx = ctx;
        this.edtIdRaca = edtIdRaca;
        this.edtNmRaca = edtNmRaca;
    }

    public BovinoHelper(Context ctx, final EditText edtNmMatriz, final EditText edtCodMatriz, final EditText edtCodReprodutor, final  EditText edtNmReprodutor ){
        this.ctx = ctx;
        this.edtIdRaca = edtIdRaca;
        this.edtNmRaca = edtNmRaca;
        this.edtCodReprodutor = edtCodReprodutor;
        this.edtNmReprodutor = edtNmReprodutor;
    }



    public BovinoHelper(Context ctx){
        this.ctx = ctx;
    }
    public void setarMascara(final EditText edtDtDesmama, final EditText edtDtEntrada, final EditText edtDtInicio, final EditText edtDtNascimento){
        Util.setMaskEditText(edtDtDesmama, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtEntrada, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtInicio, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtNascimento, Mask.DATA.getMascara());
    }
    public void preencherListView(List<Bovino> all, ListView listViewBovinos, BovinoAdapter bovinoAdapter) {
        bovinoAdapter = new BovinoAdapter(ctx,all);
        listViewBovinos.removeAllViewsInLayout();
        listViewBovinos.setAdapter(bovinoAdapter);
    }
    public  void inflaterModalRacaList(final View view, String idBovino,  AlertDialog alerta){
         racaHelper = new RacaHelper(ctx);
        this.alertaBovinoHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewRaca);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        racaHelper.preencherListView(mRecyclerView, idBovino, this);
        this.btnCancelar = view.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);

        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR RAÇA");
        builder.setView(view);
        alertaBovinoHelper = builder.create();
        alertaBovinoHelper.show();

    }


    @Override
    public void onCustomClick(Object object) {
        try{
            Raca raca = (Raca) object;
            edtNmRaca.setText(raca.getDesAbrev());
            edtIdRaca.setText(raca.getIdRaca().toString());
        }catch (Exception e){
            bovinoObj = (Bovino) object;
            edtNmMatriz.setText(bovinoObj.getApelido());
            edtCodMatriz.setText(bovinoObj.getIdBovino().toString());
        }

        alertaBovinoHelper.dismiss();
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnCancelar)){
            alertaBovinoHelper.dismiss();
        }
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    public void limparCampos() {
        edtIdBovino.setText("");
        edtApelido.setText("");
        edtCodBarras.setText("");
        edtCorPelagem.setText("");
        if(edtDtDesmama!=null){
            edtDtDesmama.setText( java.text.DateFormat.getDateInstance(DateFormat.MEDIUM).format( new java.util.Date()));
        }
        edtDtEntrada.setText(java.text.DateFormat.getDateInstance(DateFormat.MEDIUM).format( new java.util.Date()));
        edtDtInicio.setText(java.text.DateFormat.getDateInstance(DateFormat.MEDIUM).format( new java.util.Date()));
        edtDtNascimento.setText(java.text.DateFormat.getDateInstance(DateFormat.MEDIUM).format( new java.util.Date()));
        chkAtivo.setChecked(true);
        edtNrEletronico.setText("");
        edtNrManejoSisBov.setText("");
        sexo = "M";
        rbFemea.setChecked(false);
        rbMacho.setChecked(true);
        edtVlrCusto.setText("0");
        if(edtDtDesmama!=null){
            calcularIdadeDesmama();
        }


        calcularIdadeBovino();

        chkLeite.setChecked(true);
        chkReprodutorMatriz.setChecked(false);
        chkEngorda.setChecked(false);

        bovinoObj = new Bovino();
    }




    public void setarCampos() {
        edtIdBovino.setText(bovinoObj.getIdBovino().toString());
        edtApelido.setText(bovinoObj.getApelido());
        edtCodBarras.setText(bovinoObj.getCodBarras());
        edtCorPelagem.setText(bovinoObj.getCorPelagem());
        edtDtDesmama.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtDesmama()));
        edtDtEntrada.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        edtDtInicio.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        edtDtNascimento.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        chkAtivo.setChecked(bovinoObj.isFlgAtivo());
        edtNrEletronico.setText(bovinoObj.getNrChipEletronico());
        edtNrManejoSisBov.setText(bovinoObj.getNrManejosisbov());
        sexo = bovinoObj.getSexo();
        rbFemea.setChecked(bovinoObj.getSexo().equals("F")?true: false);
        rbMacho.setChecked(bovinoObj.getSexo().equals("M")?true: false);
        edtVlrCusto.setText(bovinoObj.getVlCusto().multiply(new BigDecimal(10)).toString());

        racaHelper = new RacaHelper(ctx);
        Raca raca = racaHelper.getRacaById(bovinoObj.getIdRaca());
        edtIdRaca.setText(raca.getIdRaca().toString());
        edtNmRaca.setText(raca.getNomeRaca());

        rbIdade0_12Meses.setChecked(bovinoObj.isFlgBezerros012());
        rbIdade13_24Meses.setChecked(bovinoObj.isFlgNovilhos1324());
        rbIdade25_36Meses.setChecked(bovinoObj.isFlgNovilhos2536());
        rbIdadeAcima_36Meses.setChecked(bovinoObj.isFlgNovilhosAcima36());
        rbIdade0_12MesesDesmama.setChecked(bovinoObj.isFlgBezerros012Desmama());
        rbIdade13_24MesesDesmama.setChecked(bovinoObj.isFlgNovilhos1324Desmama());
        rbIdade25_36MesesDesmama.setChecked(bovinoObj.isFlgNovilhos2536Desmama());
        rbIdadeAcima_36MesesDesmama.setChecked(bovinoObj.isFlgNovilhosAcima36Desmama());
        chkLeite.setChecked(bovinoObj.isFlgLeite());
        chkEngorda.setChecked(bovinoObj.isFlgEngorda());
        chkReprodutorMatriz.setChecked(bovinoObj.isFlgDoador());

        rbVazia.setChecked(bovinoObj.isFlgVazia());
        rbCheia.setChecked(bovinoObj.isFlgCheia());
    }

    public void setarCampos(Bovino bovino) {
        bovinoObj = bovino;
        edtIdBovino.setText(bovinoObj.getIdBovino().toString());
        edtApelido.setText(bovinoObj.getApelido());
        edtCodBarras.setText(bovinoObj.getCodBarras());
        edtCorPelagem.setText(bovinoObj.getCorPelagem());
        edtDtDesmama.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtDesmama()));
        edtDtEntrada.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        edtDtInicio.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        edtDtNascimento.setText(DateUtil.formatFieldToTxt(bovinoObj.getDtEntrada()));
        chkAtivo.setChecked(bovinoObj.isFlgAtivo());
        edtNrEletronico.setText(bovinoObj.getNrChipEletronico());
        edtNrManejoSisBov.setText(bovinoObj.getNrManejosisbov());
        sexo = bovinoObj.getSexo();
        rbFemea.setChecked(bovinoObj.getSexo().equals("F")?true: false);
        rbMacho.setChecked(bovinoObj.getSexo().equals("M")?true: false);
        edtVlrCusto.setText(bovinoObj.getVlCusto().multiply(new BigDecimal(10)).toString());

        racaHelper = new RacaHelper(ctx);
        Raca raca = racaHelper.getRacaById(bovinoObj.getIdRaca());
        edtIdRaca.setText(raca.getIdRaca().toString());
        edtNmRaca.setText(raca.getNomeRaca());

        rbIdade0_12Meses.setChecked(bovinoObj.isFlgBezerros012());
        rbIdade13_24Meses.setChecked(bovinoObj.isFlgNovilhos1324());
        rbIdade25_36Meses.setChecked(bovinoObj.isFlgNovilhos2536());
        rbIdadeAcima_36Meses.setChecked(bovinoObj.isFlgNovilhosAcima36());
        rbIdade0_12MesesDesmama.setChecked(bovinoObj.isFlgBezerros012Desmama());
        rbIdade13_24MesesDesmama.setChecked(bovinoObj.isFlgNovilhos1324Desmama());
        rbIdade25_36MesesDesmama.setChecked(bovinoObj.isFlgNovilhos2536Desmama());
        rbIdadeAcima_36MesesDesmama.setChecked(bovinoObj.isFlgNovilhosAcima36Desmama());

        chkLeite.setChecked(bovinoObj.isFlgLeite());
        chkEngorda.setChecked(bovinoObj.isFlgEngorda());
        chkReprodutorMatriz.setChecked(bovinoObj.isFlgDoador());
        rbVazia.setChecked(bovinoObj.isFlgVazia());
        rbCheia.setChecked(bovinoObj.isFlgCheia());


    }

    @SuppressLint("NewApi")
    public void recuperaDadosCampos(){

        bovinoObj = new Bovino();
        try {
            if(edtIdBovino.length() > 0){
                bovinoObj.setIdBovino(Integer.parseInt(edtIdBovino.getText().toString()));
            }
            bovinoObj.setApelido(edtApelido.getText().toString());
            bovinoObj.setCodBarras(edtCodBarras.getText().toString());
            bovinoObj.setCorPelagem(edtCorPelagem.getText().toString());

            bovinoObj.setDtDesmama(DateUtil.covertToDateSql(edtDtDesmama.getText().toString()));
            bovinoObj.setDtEntrada(DateUtil.covertToDateSql(edtDtEntrada.getText().toString()));
            bovinoObj.setDtInicio(DateUtil.covertToDateSql(edtDtInicio.getText().toString()));
            bovinoObj.setDtNascimento(DateUtil.covertToDateSql(edtDtNascimento.getText().toString()));
            bovinoObj.setFlgAtivo(chkAtivo.isChecked());
            bovinoObj.setNrChipEletronico(edtNrEletronico.getText().toString());
            bovinoObj.setNrManejosisbov(edtNrManejoSisBov.getText().toString());
            bovinoObj.setSexo(sexo);
            bovinoObj.setVlCusto(Util.parseToBigDecimal(edtVlrCusto.getText().toString(),mLocale));
            bovinoObj.setIdRaca(Integer.valueOf(edtIdRaca.getText().toString()));
            bovinoObj.setFlgBezerros012(rbIdade0_12Meses.isChecked());
            bovinoObj.setFlgNovilhos1324(rbIdade13_24Meses.isChecked());
            bovinoObj.setFlgNovilhos2536(rbIdade25_36Meses.isChecked());
            bovinoObj.setFlgNovilhosAcima36(rbIdadeAcima_36Meses.isChecked());


            bovinoObj.setFlgBezerros012Desmama(rbIdade0_12MesesDesmama.isChecked());
            bovinoObj.setFlgNovilhos1324Desmama(rbIdade13_24MesesDesmama.isChecked());
            bovinoObj.setFlgNovilhos2536Desmama(rbIdade25_36MesesDesmama.isChecked());
            bovinoObj.setFlgNovilhosAcima36Desmama(rbIdadeAcima_36MesesDesmama.isChecked());

            bovinoObj.setFlgVazia(rbVazia.isChecked());
            bovinoObj.setFlgCheia(rbCheia.isChecked());

            bovinoObj.setFlgLeite(chkLeite.isChecked());
            bovinoObj.setFlgEngorda(chkEngorda.isChecked());
            bovinoObj.setFlgDoador(chkReprodutorMatriz.isChecked());

        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }


    }

    public void onRadioButtonClicked(View view) {

        boolean checked = ((RadioButton) view).isChecked();

        switch(view.getId()) {
            case R.id.rbFeminino:
                if (checked)
                    sexo = "F";
                break;
            case R.id.rbMasculino:
                if (checked)
                    sexo="M";
                break;
        }

    }

    @RequiresApi(api = Build.VERSION_CODES.N)

    public boolean alterar(){
        boolean isUpdate = false;
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        recuperaDadosCampos();
        isUpdate = bovinoDAO.alterar(bovinoObj);
        if(bovinoObj.isFlgDoador()){
            Semem semem = new Semem();
            semem.setIdDoador(bovinoObj.getIdBovino());
            SememHelper sememHelper = new SememHelper(ctx, semem);
            sememHelper.salvar();
        }
        limparCampos();
        if (isUpdate == true) {
            Toast.makeText(ctx, "Bovino Alterada", Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(ctx, "Selecione um Bovino para Alterar", Toast.LENGTH_LONG).show();
        }
        return isUpdate;

    }

    public boolean alterar(Bovino bovino){
        boolean isUpdate = false;
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        isUpdate = bovinoDAO.alterar(bovino);
        return isUpdate;

    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    public int salvar(){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        recuperaDadosCampos();
        int idBovino = bovinoDAO.salvar(bovinoObj);
        bovinoDAO.close();

        if( idBovino > 0){
            if(bovinoObj.isFlgDoador() && bovinoObj.getSexo().equals("M")){
                Semem semem = new Semem();
                semem.setIdDoador(idBovino);
                SememHelper sememHelper = new SememHelper(ctx, semem);
                sememHelper.salvar();
            }
            if(peso != null){
                peso.setIdBovino(idBovino);
                PesoDAO pesoDAO = new PesoDAO(ctx);
                pesoDAO.salvar(this.peso);
            }
            Toast.makeText(ctx, "Bovino Cadastrada", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(ctx, "Bovino Não Cadastrada", Toast.LENGTH_LONG).show();
        }
        limparCampos();
        return  idBovino;
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean excluir(){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        String[] parametros = new String[]{edtIdBovino.getText().toString()};
        boolean isDelete = bovinoDAO.excluir(parametros);
        limparCampos();
        if(isDelete){
            Toast.makeText(ctx, "Bovino Removida", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(ctx, "Selecione um Bovino para excluir", Toast.LENGTH_LONG).show();
        }
        return  isDelete;
    }

    public void pesquisaBovino(){

        String bovinoApelido = edtApelido.getText().toString().length() >0 ? edtApelido.getText().toString(): edtIdBovino.getText().toString() ;
        if(bovinoApelido.length() >0){
            BovinoDAO bovinoDAO = new BovinoDAO(ctx);
            bovinoObj = (Bovino) bovinoDAO.getById(bovinoApelido);
            if(bovinoObj != null){
                setarCampos();
            }
        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    private void calcularIdadeDesmama() {
        String idadeStr = DateUtil.calculaIdadeByDesmama(DateUtil.covertToDateSql(edtDtNascimento.getText().toString()), DateUtil.covertToDateSql(edtDtDesmama.getText().toString()));
        switch (idadeStr){
            case "NOVILHOS (OS/AS) 13 A 24 MESES":
                rbIdade13_24MesesDesmama.setChecked(true);
                break;
            case "NOVILHOS (OS/AS) 25 A 36 MESES":
                rbIdade25_36MesesDesmama.setChecked(true);
                break;
            case "NOVILHOS (OS/AS) ACIMA DE 37 MESES":
                rbIdadeAcima_36MesesDesmama.setChecked(true);
                break;
            default:
                rbIdade0_12MesesDesmama.setChecked(true);
                break;

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void calcularIdadeBovino() {

        String idadeStr = DateUtil.calculaIdade(DateUtil.covertToDateSql(edtDtNascimento.getText().toString()));
        switch (idadeStr){
            case "NOVILHOS (OS/AS) 13 A 24 MESES":
                rbIdade13_24Meses.setChecked(true);
                break;
            case "NOVILHOS (OS/AS) 25 A 36 MESES":
                rbIdade25_36Meses.setChecked(true);
                break;
            case "NOVILHOS (OS/AS) ACIMA DE 37 MESES":
                rbIdadeAcima_36Meses.setChecked(true);
                break;
            default:
                rbIdade0_12Meses.setChecked(true);
                break;

        }
    }





    @Override
    public void onCheckedChanged(RadioGroup radioGroup, int i) {
        RadioButton rb = (RadioButton) radioGroup.findViewById(i);
        java.sql.Date dtNasc = null;
        java.sql.Date dtDes = null;
        if(rb.equals(rbIdade0_12Meses)){
            dtNasc = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.BEZERROS_0_A_12_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdade13_24Meses)){
            dtNasc = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_13_A_24_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdade25_36Meses)){
            dtNasc = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_25_A_36_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdadeAcima_36Meses)){
            dtNasc = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_ACIMA_37_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdade0_12MesesDesmama)){
            dtDes = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.BEZERROS_0_A_12_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdade13_24MesesDesmama)){
            dtDes = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_13_A_24_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdade25_36MesesDesmama)){
            dtDes = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_25_A_36_MESES.getIdadeBovino());
        }
        if(rb.equals(rbIdadeAcima_36MesesDesmama)){
            dtDes = DateUtil.ajusteDateNascimentoByIdadeRB(IdadeBovinoEnum.NOVILHOS_ACIMA_37_MESES.getIdadeBovino());
        }
        if(dtNasc != null){
            edtDtNascimento.setText(DateUtil.formatFieldToTxt(dtNasc));
        }
        if(dtDes != null){
            edtDtDesmama.setText(DateUtil.formatFieldToTxt(dtDes));
        }
        if(rb.equals(rbFemea)){
            sexo = "F";
        }
        if(rb.equals(rbMacho)){
            sexo="M";
        }
    }


    public List<Bovino> getMatrizByVazia(){

            BovinoDAO bovinoDAO = new BovinoDAO(ctx);
            List<Bovino> matrizesVazias =  bovinoDAO.getMatrizesByVazia();
            return  matrizesVazias;

    }

    public  void inflaterModalMatrizesList(final View view,  AlertDialog alerta){
        this.alertaBovinoHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewMatrizes);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        preencherListView(mRecyclerView,  this);

        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR MATRIZ");
        builder.setView(view);
        alertaBovinoHelper = builder.create();
        alertaBovinoHelper.show();

    }

    public void preencherListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {
        //  if(idBovino.length()>0){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        List<Bovino> bovinoList = (List<Bovino>) bovinoDAO.getAll();

        matrizesAdapter = new AdapterMatrizesRecView(ctx, bovinoList, clickRecyclerView_Interface);
        // mRecyclerView.removeAllViewsInLayout();
        mRecyclerView.setAdapter(matrizesAdapter);
        //    }
    }
    public void preencherBovinoNascimentoListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {
        //  if(idBovino.length()>0){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        List<Bovino> bovinoList = (List<Bovino>) bovinoDAO.getBovinoByNascimento();

        matrizesAdapter = new AdapterMatrizesRecView(ctx, bovinoList, clickRecyclerView_Interface);
        // mRecyclerView.removeAllViewsInLayout();
        mRecyclerView.setAdapter(matrizesAdapter);
        //    }
    }




//    public void setaRecyclerViewMatrizes(RecyclerView mRecyclerView){
//        //Aqui é instanciado o Recyclerview
//        mRecyclerView = (RecyclerView) mRecyclerView.findViewById(R.id.recycler_listViewMatrizes);
//        mLayoutManager = new LinearLayoutManager(ctx);
//        mRecyclerView.setLayoutManager(mLayoutManager);
//
//    }
    public  void inflaterModalReprodutorList(final View view,  AlertDialog alerta){
        this.alertaBovinoHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewMatrizes);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        preencherListView(mRecyclerView,  this);

        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR REPRODUTOR");
        builder.setView(view);
        alertaBovinoHelper = builder.create();
        alertaBovinoHelper.show();

    }

    public Bovino getBovinoById(Integer idBovino){
        BovinoDAO bovinoDAO = new BovinoDAO(ctx);
        bovinoObj = (Bovino) bovinoDAO.getById(idBovino.toString());
        return bovinoObj;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void setarDataCampo(String campoSelecionado, String data){
        if(campoSelecionado!=null) {
            switch (campoSelecionado) {
                case "DESMAMA":
                    edtDtDesmama.setText(data);
                    calcularIdadeBovino( campoSelecionado,data);

                    break;
                case "NASCIMENTO":
                    edtDtNascimento.setText(data);
                    calcularIdadeBovino( campoSelecionado,data);
                    break;
                case "ENTRADA":
                    edtDtEntrada.setText(data);
                    break;
                case "INICIO":
                    edtDtInicio.setText(data);
                    break;
                default:
                    break;
            }

        }
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    private void calcularIdadeBovino(String camposSelecionado, String date) {

        String idadeStr = DateUtil.calculaIdade(DateUtil.covertToDateSql(date));
        switch (camposSelecionado){
            case "DESMAMA":
                switch (idadeStr) {
                    case "NOVILHOS (OS/AS) 13 A 24 MESES":
                        rbIdade13_24MesesDesmama.setChecked(true);
                        break;
                    case "NOVILHOS (OS/AS) 25 A 36 MESES":
                        rbIdade25_36MesesDesmama.setChecked(true);
                        break;
                    case "NOVILHOS (OS/AS) ACIMA DE 37 MESES":
                        rbIdadeAcima_36MesesDesmama.setChecked(true);
                        break;
                    default:
                        rbIdade0_12MesesDesmama.setChecked(true);
                        break;
                }
                break;
            case "NASCIMENTO":
                switch (idadeStr) {
                    case "NOVILHOS (OS/AS) 13 A 24 MESES":
                        rbIdade13_24Meses.setChecked(true);
                        break;
                    case "NOVILHOS (OS/AS) 25 A 36 MESES":
                        rbIdade25_36Meses.setChecked(true);
                        break;
                    case "NOVILHOS (OS/AS) ACIMA DE 37 MESES":
                        rbIdadeAcima_36Meses.setChecked(true);
                        break;
                    default:
                        rbIdade0_12Meses.setChecked(true);
                        break;
                }
                break;
        }




        }
        public void setPeso(Peso peso){
            this.peso = peso;
        }


}
