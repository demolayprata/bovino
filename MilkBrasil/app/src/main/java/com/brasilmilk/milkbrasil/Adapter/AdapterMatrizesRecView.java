package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterMatrizesRecView extends  RecyclerView.Adapter<AdapterMatrizesRecView.RecyclerTesteViewHolder>   {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Bovino> mList;
//    private List<Bovino> mListSelecMatriz;




public AdapterMatrizesRecView(Context ctx, List<Bovino> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;
//        mListSelecMatriz = new ArrayList<Bovino>();

        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_matriz,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        final Bovino bovino = mList.get(position);

        viewHolder.txtApelido.setText(bovino.getApelido());
        viewHolder.txtCodBov.setText( bovino.getIdBovino().toString());
        viewHolder.txtSisBov.setText( bovino.getNrManejosisbov());

        RacaHelper racaHelper = new RacaHelper(mctx);
        Raca raca = racaHelper.getRacaById(bovino.getIdRaca());

        viewHolder.txtRaca.setText( raca.getNomeRaca());
        viewHolder.txtIdade.setText(bovino.getIdadeBovino());

//        viewHolder.chkSelecMatriz.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                if(b) {
//                    mListSelecMatriz.add(bovino);
//                }else {
//                    if(mListSelecMatriz.contains(bovino)){
//                        mListSelecMatriz.remove(bovino);
//                    }
//                }
//            }
//        });

        }


@Override
public int getItemCount() {
        return mList.size();
        }


    protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

    protected  TextView txtApelido, txtCodBov, txtSisBov, txtRaca, txtIdade;


    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtApelido = itemView.findViewById(R.id.txtApelido);
        txtCodBov = itemView.findViewById(R.id.txtCodBov);
        txtSisBov = itemView.findViewById(R.id.txtNrSisBov);
        txtRaca = itemView.findViewById(R.id.txtRaca);
        txtIdade = itemView.findViewById(R.id.txtIdade);
      //  chkSelecMatriz = itemView.findViewById(R.id.chkSelecMatriz);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));

            }
        });
    }

    }


//    public List<Bovino> getListMatriz(){
//        return this.mListSelecMatriz;
//    }

}