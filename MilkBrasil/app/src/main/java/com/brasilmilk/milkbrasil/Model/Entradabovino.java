package com.brasilmilk.milkbrasil.Model;
import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;


/**
 * The persistent class for the entradabovino database table.
 * 
 */
@ClassAnotation(nomeTabela = "ENTRADABOVINO")
public class EntradaBovino {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_ENTRADABOVINO", tipoAtributo = "INTEGER", nomeSet = "setIdEntradabovino", nomeGet = "getIdEntradabovino", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idEntradabovino;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTENTRADA", nomeGet = "getDtEntrada", nomeSet = "setDtEntrada")
	private Date dtEntrada;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "DOUBLE",nomeAtributo = "KGMORTOMEDIOINDIVIDUAL", nomeGet = "getKgMortoMedidoIndividual", nomeSet = "setKgMortoMedidoIndividual")
	private BigDecimal kgMortoMedidoIndividual;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "INTEGER",nomeAtributo = "QTDBOVINO", nomeGet = "getQtdBovino", nomeSet = "setQtdBovino")
	private Integer qtdBovino;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "DOUBLE",nomeAtributo = "QTDKGMEDIOINDIVIDUAL", nomeGet = "getQtdKgMedioIndividual", nomeSet = "setQtdKgMedioIndividual")
	private BigDecimal qtdKgMedioIndividual;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "DOUBLE",nomeAtributo = "VLRARROBA", nomeGet = "getVlrArroba", nomeSet = "setVlrArroba")
	private BigDecimal vlrArroba;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "DOUBLE",nomeAtributo = "VLRTOTAL", nomeGet = "getVlrTotal", nomeSet = "setVlrTotal")
	private BigDecimal vlrTotal;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(tipoAtributo = "DOUBLE",nomeAtributo = "VLRUNITARIO", nomeGet = "getVlrUnitario", nomeSet = "setVlrUnitario")
	private BigDecimal vlrUnitario;


	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_NOTAFISCAL", nomeGet = "getIdNotafiscal", nomeSet = "setIdNotafiscal",  forengKey = true, referencesTable = "NOTAFISCAL", fieldReferences = "ID_NOTAFISCAL", notNull = true)
	private Integer idNotafiscal;



	public EntradaBovino(Integer idEntradabovino, Date dtEntrada, BigDecimal kgMortoMedidoIndividual, Integer qtdBovino, BigDecimal qtdKgMedioIndividual, BigDecimal vlrArroba, BigDecimal vlrTotal, BigDecimal vlrUnitario, Integer idNotafiscal, Integer idLote) {
		this.idEntradabovino = idEntradabovino;
		this.dtEntrada = dtEntrada;
		this.kgMortoMedidoIndividual = kgMortoMedidoIndividual;
		this.qtdBovino = qtdBovino;
		this.qtdKgMedioIndividual = qtdKgMedioIndividual;
		this.vlrArroba = vlrArroba;
		this.vlrTotal = vlrTotal;
		this.vlrUnitario = vlrUnitario;
		this.idNotafiscal = idNotafiscal;
		;
	}
	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdEntradabovino() {
		return idEntradabovino;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdEntradabovino(Integer idEntradabovino) {
		this.idEntradabovino = idEntradabovino;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtEntrada() {
		return dtEntrada;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getKgMortoMedidoIndividual() {
		return kgMortoMedidoIndividual;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setKgMortoMedidoIndividual(BigDecimal kgMortoMedidoIndividual) {
		this.kgMortoMedidoIndividual = kgMortoMedidoIndividual;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getQtdBovino() {
		return qtdBovino;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setQtdBovino(Integer qtdBovino) {
		this.qtdBovino = qtdBovino;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdKgMedioIndividual() {
		return qtdKgMedioIndividual;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdKgMedioIndividual(BigDecimal qtdKgMedioIndividual) {
		this.qtdKgMedioIndividual = qtdKgMedioIndividual;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrArroba() {
		return vlrArroba;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrArroba(BigDecimal vlrArroba) {
		this.vlrArroba = vlrArroba;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrTotal() {
		return vlrTotal;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrTotal(BigDecimal vlrTotal) {
		this.vlrTotal = vlrTotal;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrUnitario() {
		return vlrUnitario;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrUnitario(BigDecimal vlrUnitario) {
		this.vlrUnitario = vlrUnitario;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdNotafiscal() {
		return idNotafiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdNotafiscal(Integer idNotafiscal) {
		this.idNotafiscal = idNotafiscal;
	}


}