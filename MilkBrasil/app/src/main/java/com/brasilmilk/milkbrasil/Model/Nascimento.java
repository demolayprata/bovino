package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.sql.Date;



@ClassAnotation(nomeTabela = "NASCIMENTO")
public class Nascimento {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_NASCIMENTO", tipoAtributo = "INTEGER", nomeSet = "setIdNascimento", nomeGet = "getIdNascimento", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idNascimento;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTPARTO", nomeGet = "getDtParto", nomeSet = "setDtParto")
	private Date dtParto;

	@AtributoAnotation(nomeAtributo = "FLGVIVO", nomeGet = "isFlgVivo", nomeSet = "setFlgVivo", tipoAtributo = "INTEGER")
	private boolean flgVivo;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "SEXO", nomeGet = "getSexo", nomeSet = "setSexo")
	private String sexo;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_MONTA", tipoAtributo = "INTEGER", nomeGet = "getIdMonta", nomeSet = "setIdMonta",  forengKey = true, referencesTable = "MONTA", fieldReferences = "ID_MONTA", notNull = true)
	private Integer idMonta;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_BOVINO", tipoAtributo = "INTEGER", nomeGet = "getIdBovino", nomeSet = "setIdBovino",  forengKey = true, referencesTable = "BOVINO", fieldReferences = "ID_BOVINO", notNull = true)
	private Integer idBovino;

	public Nascimento(Integer idNascimento, Date dtParto, boolean flgVivo, String sexo, Integer idMonta) {
		this.idNascimento = idNascimento;
		this.dtParto = dtParto;
		this.flgVivo = flgVivo;
		this.sexo = sexo;
		this.idMonta = idMonta;
	}

	public Nascimento() {
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdNascimento() {
		return idNascimento;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdNascimento(Integer idNascimento) {
		this.idNascimento = idNascimento;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtParto() {
		return dtParto;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtParto(Date dtParto) {
		this.dtParto = dtParto;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public boolean isFlgVivo() {
		return flgVivo;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgVivo(boolean flgVivo) {
		this.flgVivo = flgVivo;
	}
	@MetodoAnotation()
	public String getSexo() {
		return sexo;
	}

	@MetodoAnotation()
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdMonta() {
		return idMonta;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdMonta(Integer idMonta) {
		this.idMonta = idMonta;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdBovino() {
		return idBovino;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdBovino(Integer idBovino) {
		this.idBovino = idBovino;
	}
}