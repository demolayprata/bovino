package com.brasilmilk.milkbrasil.Activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TabHost;

import com.brasilmilk.milkbrasil.Fragments.MenuFragment;
import com.brasilmilk.milkbrasil.Fragments.MenuMontaFragment;
import com.brasilmilk.milkbrasil.Fragments.MontaFragment;
import com.brasilmilk.milkbrasil.Fragments.MontaResultadoFragment;
import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceMenuMontaInterface;
import com.brasilmilk.milkbrasil.R;

public class MenuMontaActivity extends AppCompatActivity implements View.OnClickListener,ButtonReferenceMenuMontaInterface, ButtonReferenceInterface{
    private FragmentHelper fragmentHelper;
    private FragmentManager fm;
    private Button btnLancarMonta, btnLancarResultado;
    private Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar;
    private TabHost tabHost;
    private RecyclerView recyclerViewList;
    private MontaFragment montaFragment;
    private MontaResultadoFragment montaResultadoFragment;
    private boolean isLancarMonta = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_monta);
        fm = getSupportFragmentManager();
        fragmentHelper = new FragmentHelper(fm);

        MenuMontaFragment menuMontaFragment = new MenuMontaFragment();
        fragmentHelper.adicionaFragment(R.id.fragment_content, menuMontaFragment);
    }

    @Override
    public void passarReferencia(Button btnLancarMonta, Button btnLancarResultado) {
        this.btnLancarMonta = btnLancarMonta;
        this.btnLancarMonta.setOnClickListener(this);
        this.btnLancarResultado = btnLancarResultado;
        this.btnLancarResultado.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnLancarMonta)){
            MenuFragment menuFragment = new MenuFragment();
            fragmentHelper.substituiFragment(R.id.fragment_content, menuFragment);
            //fragmentHelper.adicionaFragment(R.id.fragment_content, menuFragment);
        }
        if(view.equals(btnLancarResultado)){
            isLancarMonta = false;
            MenuFragment menuFragment = new MenuFragment();
            fragmentHelper.substituiFragment(R.id.fragment_content, menuFragment);
        }

    }

    @Override
    public void passarReferencia(Button btnCadastrar, Button btnEditar, Button btnExcluir, Button btnLimpar, Button btnPesquisar, Button btnListar, TabHost tabHost, RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnEditar = btnEditar;
        this.btnExcluir = btnExcluir;
        this.btnLimpar = btnLimpar;
        this.btnPesquisar = btnPesquisar;
        this.btnListar = btnListar;
        this.tabHost = tabHost;

        this.tabHost.setup();

        TabHost.TabSpec tabCadastro = tabHost.newTabSpec("ABA CADASTRO");
        TabHost.TabSpec tabLista = tabHost.newTabSpec("ABA LISTA");
        if(isLancarMonta) {
            tabCadastro.setIndicator("Lançar Monta");
            tabCadastro.setContent(R.id.fragment_crud);

            tabLista.setIndicator("Lista");
            tabLista.setContent(R.id.recycler_listVMonta);
//
            tabHost.addTab(tabCadastro);
            tabHost.addTab(tabLista);

            montaFragment = new MontaFragment();
            fragmentHelper.adicionaFragment(R.id.fragment_crud, montaFragment);

            recyclerViewList = recyclerView;

            montaFragment.passarReferenciaButton(this.btnCadastrar, this.btnEditar, this.btnExcluir, this.btnLimpar,
                    this.btnPesquisar, this.btnListar, this.tabHost, recyclerViewList);
        }else {
            tabCadastro.setIndicator("Lançar Resultado Monta");
            tabCadastro.setContent(R.id.fragment_crud);

            tabLista.setIndicator("Lista");
            tabLista.setContent(R.id.recycler_listVMonta);
//
            tabHost.addTab(tabCadastro);
            tabHost.addTab(tabLista);

            montaResultadoFragment = new MontaResultadoFragment();
            fragmentHelper.adicionaFragment(R.id.fragment_crud, montaResultadoFragment);

            recyclerViewList = recyclerView;

            montaResultadoFragment.passarReferenciaButton(this.btnCadastrar, this.btnEditar, this.btnExcluir, this.btnLimpar,
                    this.btnPesquisar, this.btnListar, this.tabHost, recyclerViewList);
        }
    }
}
