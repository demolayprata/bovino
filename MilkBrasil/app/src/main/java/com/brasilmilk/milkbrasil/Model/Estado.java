package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

/**
 * Created by ricar on 25/11/2017.
 */
@ClassAnotation(nomeTabela = "ESTADO")
public class Estado {
    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "ID_ESTADO", tipoAtributo = "INTEGER", nomeSet ="setCodEstado", nomeGet = "getCodEstado", primaryKeyAtributo = true, autoIncrementAtributo = true)
    private Integer codEstado;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "NM_ESTADO", nomeGet = "getNomeEstado", nomeSet = "setNomeEstado", unique = true)
    private String nomeEstado;

    public Estado() {

    }
    public Estado(Integer codEstado, String nomeUf) {
        this.codEstado = codEstado;
        this.nomeEstado = nomeUf;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getCodEstado() {
        return codEstado;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setCodEstado(Integer codEstado) {
        this.codEstado = codEstado;
    }

    @MetodoAnotation
    public String getNomeEstado() {
        return nomeEstado;
    }

    @MetodoAnotation
    public void setNomeEstado(String nomeEstado) {
        this.nomeEstado = nomeEstado;
    }
}
