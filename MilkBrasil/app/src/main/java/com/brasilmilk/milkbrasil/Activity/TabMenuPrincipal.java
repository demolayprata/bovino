package com.brasilmilk.milkbrasil.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.brasilmilk.milkbrasil.CadastroMenu;
import com.brasilmilk.milkbrasil.Fragments.MovimentoFragment;
import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.R;

public class TabMenuPrincipal extends AppCompatActivity implements View.OnClickListener{

    private Button btnMovimento;

    private MovimentoFragment movimentoFragment;
    private ImageButton imgBttCadastros;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_menu_principal);
        imgBttCadastros = (ImageButton) findViewById(R.id.imgBttCad);
        imgBttCadastros.setOnClickListener(this);
        btnMovimento = (Button) findViewById(R.id.btnMovimento);
        btnMovimento.setOnClickListener(this);



    }

    @Override
    public void onClick(View view) {
        if(view.equals(imgBttCadastros)){
            Intent intent = new Intent(TabMenuPrincipal.this, CadastroMenu.class);
            startActivity(intent);
        }
        if(view.equals(btnMovimento)){
            Intent intent = new Intent(this, MenuMovimentoActivity.class);
            startActivity(intent);
        }
    }
}
