package com.brasilmilk.milkbrasil.Activity;

import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TabHost;

import com.brasilmilk.milkbrasil.Fragments.BovinoModalFragment;
import com.brasilmilk.milkbrasil.Fragments.MenuFragment;
import com.brasilmilk.milkbrasil.Fragments.MontaFragment;
import com.brasilmilk.milkbrasil.Fragments.MontaResultadoFragment;
import com.brasilmilk.milkbrasil.Fragments.NascimentoFragment;
import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceMenuMontaInterface;
import com.brasilmilk.milkbrasil.R;

public class NascimentoActivity extends AppCompatActivity implements View.OnClickListener, ButtonReferenceInterface{
    private FragmentHelper fragmentHelper;
    private FragmentManager fm;


    private Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar, btnSelecBov;
    private TabHost tabHost;
    private RecyclerView recyclerViewList;
    private NascimentoFragment nascimentoFragment;
    private MenuFragment menuFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nascimento);
        fm = getSupportFragmentManager();
        fragmentHelper = new FragmentHelper(fm);
        menuFragment = new MenuFragment();

       fragmentHelper.adicionaFragment(R.id.fragment_content, menuFragment);
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public void passarReferencia(Button btnCadastrar, Button btnEditar, Button btnExcluir, Button btnLimpar, Button btnPesquisar, Button btnListar, TabHost tabHost, RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnEditar = btnEditar;
        this.btnExcluir = btnExcluir;
        this.btnLimpar = btnLimpar;
        this.btnPesquisar = btnPesquisar;
        this.btnListar = btnListar;
        this.tabHost = tabHost;

        this.tabHost.setup();

        TabHost.TabSpec tabCadastro = tabHost.newTabSpec("ABA CADASTRO");
        TabHost.TabSpec tabLista = tabHost.newTabSpec("ABA LISTA");
            tabCadastro.setIndicator("Lançar Nascimento");
            tabCadastro.setContent(R.id.fragment_crud);

            tabLista.setIndicator("Lista");
            tabLista.setContent(R.id.recycler_listVMonta);
//
            tabHost.addTab(tabCadastro);
            tabHost.addTab(tabLista);

            nascimentoFragment = new NascimentoFragment();
            fragmentHelper.adicionaFragment(R.id.fragment_crud, nascimentoFragment);

            recyclerViewList = recyclerView;

            nascimentoFragment.passarReferenciaButton(this.btnCadastrar, this.btnEditar, this.btnExcluir, this.btnLimpar,
                    this.btnPesquisar, this.btnListar, this.tabHost, recyclerViewList);
    }


}
