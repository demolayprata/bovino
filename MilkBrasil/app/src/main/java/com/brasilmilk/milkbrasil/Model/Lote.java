package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.sql.Date;

/**
 * The persistent class for the lote database table.
 * 
 */
@ClassAnotation(nomeTabela = "LOTE")
public class Lote  {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_LOTE", tipoAtributo = "INTEGER", nomeSet = "setIdLote", nomeGet = "getIdLote", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idLote;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTCRIACAO", nomeGet = "getDtCriacao", nomeSet = "setDtCriacao")
	private Date dtCriacao;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGATIVO", nomeGet = "getFlgAtivo", nomeSet = "setFlgAtivo", tipoAtributo = "INTEGER")
	private boolean flgAtivo;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "NMLOTE", nomeGet = "getNmLote", nomeSet = "setNmLote")
	private String nmLote;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "OBS", nomeGet = "getObs", nomeSet = "setObs")
	private String obs;

	public Lote(Integer idLote, Date dtCriacao, boolean flgAtivo, String nmLote, String obs) {
		this.idLote = idLote;
		this.dtCriacao = dtCriacao;
		this.flgAtivo = flgAtivo;
		this.nmLote = nmLote;
		this.obs = obs;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdLote() {
		return idLote;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdLote(Integer idLote) {
		this.idLote = idLote;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtCriacao() {
		return dtCriacao;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtCriacao(Date dtCriacao) {
		this.dtCriacao = dtCriacao;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public boolean isFlgAtivo() {
		return flgAtivo;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgAtivo(boolean flgAtivo) {
		this.flgAtivo = flgAtivo;
	}

	@MetodoAnotation()
	public String getNmLote() {
		return nmLote;
	}

	@MetodoAnotation()
	public void setNmLote(String nmLote) {
		this.nmLote = nmLote;
	}

	@MetodoAnotation()
	public String getObs() {
		return obs;
	}

	@MetodoAnotation()
	public void setObs(String obs) {
		this.obs = obs;
	}
}