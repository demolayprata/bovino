package com.brasilmilk.milkbrasil.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Helper.MatrizHelper;
import com.brasilmilk.milkbrasil.Helper.MontaHelper;
import com.brasilmilk.milkbrasil.Helper.ReprodutorHelper;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Iterator;
import java.util.List;


public class MontaFragment extends Fragment implements Validator.ValidationListener, View.OnClickListener, TabHost.OnTabChangeListener, ClickRecyclerView_Interface {

    private OnItemSelectedListener listener;
    private MontaHelper montaHelper;
    private Monta monta;

    private Context ctx;
    private AlertDialog alerta;
    private EditText edtCodMonta;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtDtMonta;
    private EditText edtDtResultado;

    private EditText edtPrevParto;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtDtPrevResultado;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodMatriz;
    private EditText edtNmMatriz;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodReprodutor;
    private EditText edtNmReprodutor;

   private CheckBox chkCheia, chkVaiza;

    private Button btnSelecMatrizes;
    private Button btnSelecReprodutor;
    private MatrizHelper matrizHelper;
    private ReprodutorHelper reprodutorHelper;
    private Button  btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar;
    private TabHost tabHost;
    private RecyclerView recyclerView;


    private  Validator validator;
    private boolean validacaoOk = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_monta, container, false);
        edtCodMonta = (EditText) view.findViewById(R.id.edtIdMonta);
        edtDtMonta = (EditText) view.findViewById(R.id.edtDtMonta);
        edtDtResultado = (EditText) view.findViewById(R.id.edtDtResultado);
        edtDtPrevResultado = (EditText) view.findViewById(R.id.edtPrevResultado);
        edtPrevParto = (EditText) view.findViewById(R.id.edtDtPrevParto);
        edtCodMatriz = (EditText) view.findViewById(R.id.edtIdMatriz);
        edtNmMatriz = (EditText) view.findViewById(R.id.edtDtNomeMatriz);
        edtCodReprodutor = (EditText) view.findViewById(R.id.edtIdReprodutor);
        edtNmReprodutor = (EditText) view.findViewById(R.id.edtDtNomeReprodutor);


        chkCheia = (CheckBox) view.findViewById(R.id.chkCheia);
        chkVaiza = (CheckBox) view.findViewById(R.id.chkVazia);

        btnSelecMatrizes = (Button) view.findViewById(R.id.btnSelecMatrizes);
        btnSelecMatrizes.setOnClickListener(this);

        btnSelecReprodutor = (Button) view.findViewById(R.id.btnSelecReprodutor);
        btnSelecReprodutor.setOnClickListener(this);



        ctx = inflater.getContext();

        montaHelper = new MontaHelper(ctx, edtCodMonta, edtDtMonta, edtPrevParto,edtDtPrevResultado,
                edtDtResultado, edtCodReprodutor,   edtNmReprodutor,  edtCodMatriz, edtNmMatriz,
                chkCheia, chkVaiza,  monta,  recyclerView);

        matrizHelper = new MatrizHelper(ctx,  edtNmMatriz, edtCodMatriz);
        reprodutorHelper = new ReprodutorHelper(ctx, edtNmReprodutor, edtCodReprodutor);
        validator = new Validator(this);
        validator.setValidationListener(this);

        return view;



    }

    @Override
    public void onValidationSucceeded() {
        validacaoOk = true;

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(ctx));
            }
            iterator.remove();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {
        if(view.equals(btnSelecMatrizes)){
            LayoutInflater li = getLayoutInflater();
            //inflamos o layout alerta.xml na view
            final View viewModalMatrizesList = li.inflate(R.layout.modal_matrizes_lista, null);
            matrizHelper.inflaterModalMatrizesList(viewModalMatrizesList, alerta);
        }
        if(view.equals(btnSelecReprodutor)){
            LayoutInflater li = getLayoutInflater();
            final  View viewModalReprodutor = li.inflate(R.layout.modal_matrizes_lista, null);
            reprodutorHelper.inflaterModalMatrizesList(viewModalReprodutor, alerta);
        }
        if(view.equals(btnCadastrar)){
            try{
                validator.validate();
                if(validacaoOk) {
                    if(edtCodMonta.getText().length()>0){

                    }else {

                        Integer id = montaHelper.salvar();
                    }
                    validacaoOk = false;
                }
            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }

        }
        if(view.equals(btnExcluir)){

        }
        if(view.equals(btnLimpar)){
            montaHelper.limpaCampos();
        }
        if(view.equals(btnPesquisar)){

        }
        if(view.equals(btnListar)){
            montaHelper.setaRecyclerView();
            montaHelper.preencherListView(this);
            tabHost.setCurrentTab(1);
        }
    }


    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "ABA LISTA":
                montaHelper.setaRecyclerView();
                montaHelper.preencherListView(this);
                tabHost.setCurrentTab(1);
                break;
            default:
                break;
        }

    }

    @Override
    public void onCustomClick(Object object) {
        monta = (Monta)object;
        montaHelper.setarCampos(monta);
        tabHost.setCurrentTab(0);
    }

    public interface  OnItemSelectedListener{
        public void onItemSelected(Monta monta);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        ctx = context;

    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void passarReferenciaButton(final Button btnCadastrar, final Button btnEditar, final Button btnExcluir
            , final Button btnLimpar, final Button btnPesquisar, final Button btnListar, final TabHost tabHost, final RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnCadastrar.setOnClickListener(this);
        this.btnExcluir = btnExcluir;
        this.btnExcluir.setOnClickListener(this);
        this.btnLimpar = btnLimpar;
        this.btnLimpar.setOnClickListener(this);
        this.btnPesquisar = btnPesquisar;
        this.btnPesquisar.setOnClickListener(this);
        this.btnListar = btnListar;
        this.btnListar.setOnClickListener(this);
        this.btnEditar = btnEditar;
        this.btnEditar.setOnClickListener(this);
        this.tabHost = tabHost;
        this.tabHost.setOnTabChangedListener(this);
        this.recyclerView = recyclerView;


    }
}
