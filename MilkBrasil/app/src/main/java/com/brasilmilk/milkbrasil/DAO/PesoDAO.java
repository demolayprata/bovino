package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class PesoDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Peso> listaPeso;
    public PesoDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Peso.class);
            scriptSQLDelete = Util.getDROPSQLite(Peso.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaPeso = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objPeso) {
        Peso peso = (Peso) objPeso;
        return super.insert(peso, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_PESO");
        return super.delete(this.db, Peso.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_PESO");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdPeso"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaPeso.clear();
        listaPeso.addAll((Collection<? extends Peso>) super.getAll(db, Peso.class));
        return listaPeso;
    }

    public List<?> getPesosByBovino(Integer idBovino) {
        listaPeso.clear();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("ID_BOVINO");
            whereBuilder.append(" = ?");

            String where = whereBuilder.toString();
            String[]args = new String[]{idBovino.toString()};

            cursor = db.query(Util.getNomeTabela(Peso.class), Util.getColumView(Peso.class), where, args, null, null, "DTPESAGEM");
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Peso.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Peso.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Peso.class, objLinha );
                    }
                    listaPeso.add((Peso) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }
        return listaPeso;
    }

    @Override
    public Object getById(Object param) {
//        List<String> compoList = new ArrayList<String>();
//        try {
//            Integer paramCod = Integer.valueOf((String) param);
//            compoList.add("ID_BOVINO");
//        }catch (Exception e){
//            compoList.add("APELIDO");
//        }
//        String[] parametros = new String[]{(String) param};
        return null;//super.getByParam(db, Bovino.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Peso.class);
    }

}
