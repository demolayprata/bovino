package com.brasilmilk.milkbrasil.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.brasilmilk.milkbrasil.Adapter.AdapterDesmamaRecView;
import com.brasilmilk.milkbrasil.Adapter.AdapterRacaRecView;
import com.brasilmilk.milkbrasil.DAO.DesmamaDAO;
import com.brasilmilk.milkbrasil.DAO.RacaDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.R;

import java.util.List;

/**
 * Created by ricar on 24/12/2017.
 */

public class RacaHelper implements View.OnClickListener {
    private Context ctx;
    private RacaDAO racaDAO;
    private AdapterRacaRecView racaAdapter;
    private List<Raca> racaList;
    private RecyclerView.LayoutManager mLayoutManager;

    private EditText edtDesAbrevRaca, edtNomeRaca, edtObsRaca;
    private Button btnSalvar, btnCancelar;
    AlertDialog alertaRacaHelper;
    public RacaHelper(Context ctx){
        this.ctx = ctx;
    }
    public void preencherListView(final RecyclerView mRecyclerView, String idBovino, ClickRecyclerView_Interface clickRecyclerView_Interface) {
      //  if(idBovino.length()>0){
            racaDAO = new RacaDAO(ctx);
            racaList = (List<Raca>) racaDAO.getAll();

            racaAdapter = new AdapterRacaRecView(ctx, racaList, clickRecyclerView_Interface);
           // mRecyclerView.removeAllViewsInLayout();
            mRecyclerView.setAdapter(racaAdapter);
    //    }
    }

    public void setaRecyclerView(RecyclerView mRecyclerView){
        //Aqui é instanciado o Recyclerview
        mRecyclerView = (RecyclerView) mRecyclerView.findViewById(R.id.recycler_listViewRaca);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }


    public void inflaterModalRacaCrud(View view, AlertDialog alerta) {
        this.alertaRacaHelper = alerta;
        this.edtDesAbrevRaca = view.findViewById(R.id.edtDesAbrevRaca);
        this.edtNomeRaca  = view.findViewById(R.id.edtNomeRaca);
        this.edtObsRaca = view.findViewById(R.id.edtObs);
        this.btnCancelar = view.findViewById(R.id.btnCancelar);
        btnCancelar.setOnClickListener(this);
        this.btnSalvar = view.findViewById(R.id.btnSalvar);
        btnSalvar.setOnClickListener(this);
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("CADASTRAR RAÇA");
        builder.setView(view);
        alertaRacaHelper = builder.create();
        alertaRacaHelper.show();
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnCancelar)){
            alertaRacaHelper.dismiss();
        }
        if(view.equals(btnSalvar)){
            Raca racaSalvar = new Raca();
            recuperarCampos(racaSalvar);
            racaDAO = new RacaDAO(ctx);
            racaDAO.salvar(racaSalvar);
            alertaRacaHelper.dismiss();

        }
    }

    public void recuperarCampos(final Raca raca){
        raca.setNomeRaca(edtNomeRaca.getText().toString());
        raca.setDesAbrev(edtDesAbrevRaca.getText().toString());
        raca.setObs(edtObsRaca.getText().toString());
    }

    public Raca getRacaById(Integer idRaca){
        racaDAO = new RacaDAO(ctx);
        Raca raca = (Raca) racaDAO.getById(idRaca);

       return raca;
    }
}
