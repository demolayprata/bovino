package com.brasilmilk.milkbrasil.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Adapter.AdapterSemeReprodutorRecView;
import com.brasilmilk.milkbrasil.DAO.SememDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Interface.InterfaceHelper;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Semem;
import com.brasilmilk.milkbrasil.R;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by ricar on 25/12/2017.
 */

public class SememHelper implements ClickRecyclerView_Interface, View.OnClickListener, InterfaceHelper{
    private Context ctx;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;

    private AlertDialog.Builder builder;
    private AlertDialog alertaSememHelper;

    private EditText edtCodSememMonta, edtNmReprodutorMonta;
    private TextView txtLabelDescMonta;

    private AdapterSemeReprodutorRecView sememAdapter;
    private Button btnOk;
    //////////////////////

    //private int ano, mes, dia;
    private Calendar cal;

    private Locale mLocale;

    private Semem sememObj;
    private SememDAO sememDAO;



    public SememHelper(Context ctx){
        this.ctx = ctx;
    }
    public SememHelper(Context ctx, final Semem semem){
        this.ctx = ctx;
        this.sememObj = semem;
    }

    public SememHelper(Context ctx, final EditText edtCodReprodutorMonta, final EditText edtNmReprodutorMonta, final  TextView txtLabelDescMonta){
        this.ctx = ctx;
        this.edtCodSememMonta = edtCodReprodutorMonta;
        this.edtNmReprodutorMonta = edtNmReprodutorMonta;
        this.txtLabelDescMonta = txtLabelDescMonta;
    }

    @Override
    public void onCustomClick(Object object) {

            sememObj = (Semem) object;
            if(sememObj.getIdDoador()!=null){
                BovinoHelper bovinoHelper = new BovinoHelper(ctx);
                Bovino bovino = bovinoHelper.getBovinoById(sememObj.getIdDoador());
                txtLabelDescMonta.setText("NOME REPRODUTOR");
                edtNmReprodutorMonta.setText(bovino.getApelido());
                edtCodSememMonta.setText(sememObj.getIdSemem().toString());
            }else {
                txtLabelDescMonta.setText("DESCRIÇÃO PRODUTO");
                //// seta os valores da descricao do produto e do codigo do produto
            }


        alertaSememHelper.dismiss();
    }





    @Override
    public void limparCampos() {

    }

    @Override
    public void setarCampos() {

    }

    @Override
    public void recuperarCampos() {

    }

    @Override
    public boolean excluir() {
        String[] parametros = new String[]{sememObj.getIdSemem().toString()};
        boolean isDelete = sememDAO.excluir(parametros);

        return isDelete;
    }

    @Override
    public int salvar() {
        sememDAO = new SememDAO(ctx);
        int idSemem = sememDAO.salvar(sememObj);
        return idSemem;
    }

    public void preencherListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {
        //  if(idBovino.length()>0){
        sememDAO = new SememDAO(ctx);
        List<Semem> bovinoList = (List<Semem>) sememDAO.getAll();

        sememAdapter = new AdapterSemeReprodutorRecView(ctx, bovinoList, clickRecyclerView_Interface);
        // mRecyclerView.removeAllViewsInLayout();
        mRecyclerView.setAdapter(sememAdapter);
        //    }
    }

    @Override
    public void setaRecyclerView(RecyclerView mRecyclerView) {
        mRecyclerView = (RecyclerView) mRecyclerView.findViewById(R.id.recycler_listViewSemem);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void setarMascara() {

    }

    @Override
    public void inflateModalList(View view, AlertDialog alerta) {
        this.alertaSememHelper = alerta;
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_listViewSemem);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        preencherListView(mRecyclerView,  this);
        this.btnOk = view.findViewById(R.id.btnOk);
        btnOk.setOnClickListener(this);
        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR SEMEM");
        builder.setView(view);
        alertaSememHelper = builder.create();
        alertaSememHelper.show();
    }


    @Override
    public void onClick(View view) {
        if(view.equals(btnOk)){
            if(alertaSememHelper != null){
                alertaSememHelper.dismiss();
            }
        }
    }
}
