package com.brasilmilk.milkbrasil.Activity;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Adapter.BovinoAdapter;
import com.brasilmilk.milkbrasil.DAO.BovinoDAO;
import com.brasilmilk.milkbrasil.DAO.PesoDAO;
import com.brasilmilk.milkbrasil.Helper.BovinoHelper;
import com.brasilmilk.milkbrasil.Helper.DesmamaHelper;
import com.brasilmilk.milkbrasil.Helper.PesoHelper;
import com.brasilmilk.milkbrasil.Helper.RacaHelper;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.MoneyTextWatcher;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

public class BovinoCadastro extends FragmentActivity implements View.OnClickListener,AdapterView.OnItemClickListener, Validator.ValidationListener, DatePickerDialog.OnDateSetListener, TabHost.OnTabChangeListener, DatePickerDialog.OnShowListener{

    private class DataBovino{
        private int dia, ano, mes;

    }

    private enum TipoTxtData{
        DTDESMAMA("DESMAMA"), DTENTRADA("ENTRADA"), DTINICIO("INICIO"), DTNASCIMENTO("NASCIMENTO");
        private  String data;
        TipoTxtData(String s) {
            this.data = s;
        }
        public String getTipoDataCampo(){
            return  this.data;
        }
    }

    private Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar, btnPesoLista, btnSelecionarRaca, btnNovoRacaModal, btnLancarDesmama, btnSalvarDesmama;
    private CheckBox chkAtivo, chkLeite, chkEngorda, chkReprodutorMatriz;
    private RadioButton rbMacho, rbFemea, rbIdade0_12Meses, rbIdade13_24Meses, rbIdade25_36Meses, rbIdadeAcima_36Meses,  rbIdade0_12MesesDesmama, rbIdade13_24MesesDesmama, rbIdade25_36MesesDesmama,
            rbIdadeAcima_36MesesDesmama, rbCheia, rbVazia;;
    private RadioGroup rdGroup;
    private  RadioGroup rdGroupIdadeDesmama, rdGroupIdadeBovino, rGroupStatus;

    //private int ano, mes, dia;
    private  Calendar cal;
    static final int DATE_DIALOG_ID = 0;

    private EditText edtIdBovino;

    @NotEmpty(message = "Apeldio Obrigatorio")
    private EditText edtApelido;

    private EditText edtCodBarras;

    @NotEmpty(message = "Cor da PelagemObrigatorio")
    private EditText edtCorPelagem;

    private EditText edtDtDesmama;

    @NotEmpty(message = "Data da Entrada Obrigatorio")
    private EditText edtDtEntrada;

    @NotEmpty(message = "Data Inicio Obrigatorio")
    private EditText edtDtInicio;

    @NotEmpty(message = "Data Nascimento Obrigatorio")
    private  EditText edtDtNascimento;

    private EditText edtNrEletronico;

    private EditText edtNrManejoSisBov;

    @NotEmpty(message = "Valor Custo Obrigatorio")
    private EditText edtVlrCusto;

    @NotEmpty(message = "Raca Obrigatorio")
    private EditText edtIdRaca;

    @NotEmpty(message = "Raca Obrigatorio")
    private EditText edtNmRaca;

    ListView listViewBovinos;

    private TabHost tabHost;

    private List<Bovino> bovinoList;

    private boolean validacaoOk = false;
    private String sexo = "M";
    private  BovinoDAO bovinoDAO;
    private  PesoDAO pesoDAO;
    private  Bovino bovinoObj;
    private  Validator validator;

    private  String campoDataSelecionado;
    private BovinoAdapter bovinoAdapter;
    private Locale mLocale;
    private   AlertDialog alerta;
    private DatePickerDialog datePickerDialog;
    private BovinoHelper bovinoHelper;

    private Desmama desmamaObj;
    DesmamaHelper desmamaHelper;


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bovino_cadastro);

        btnCadastrar = (Button) findViewById(R.id.btnSalvar);
        btnCadastrar.setOnClickListener(this);

        btnPesoLista = (Button) findViewById(R.id.btnListarPesos);
        btnPesoLista.setOnClickListener(this);

        btnSelecionarRaca = (Button) findViewById(R.id.btnSelecionarRaca);
        btnSelecionarRaca.setOnClickListener(this);

        btnEditar = (Button) findViewById((R.id.btnEditar));
        btnEditar.setOnClickListener(this);

        btnExcluir = (Button) findViewById(R.id.btnExcluir);
        btnExcluir.setOnClickListener(this);

        btnLimpar = (Button) findViewById(R.id.btnLimpar);
        btnLimpar.setOnClickListener(this);

        btnPesquisar = (Button) findViewById(R.id.btnPesquisar);
        btnPesquisar.setOnClickListener(this);

        btnListar = (Button) findViewById(R.id.btnListar);
        btnListar.setOnClickListener(this);

        btnLancarDesmama = (Button) findViewById(R.id.btnDesmama);
        btnLancarDesmama.setOnClickListener(this);

        listViewBovinos = (ListView) findViewById(R.id.listViewBovino);
        listViewBovinos.setOnItemClickListener(this);

        rdGroup = (RadioGroup) findViewById(R.id.radioGroup);
        rbMacho=(RadioButton) findViewById(R.id.rbMasculino);
        rbFemea = (RadioButton) findViewById(R.id.rbFeminino);

        rbMacho.setChecked(true);
        rbFemea.setChecked(false);

        rGroupStatus = (RadioGroup) findViewById(R.id.radioGroupStatus);
        rbVazia = (RadioButton) findViewById(R.id.rbVazia);
        rbCheia = (RadioButton) findViewById(R.id.rbCheia);

        rdGroupIdadeDesmama = (RadioGroup) findViewById(R.id.radioGroupIdadeDesmam);
        rdGroupIdadeBovino =  (RadioGroup) findViewById(R.id.radioGroupIdade);

        rbIdade0_12Meses = (RadioButton) findViewById(R.id.rbIdade_0_A_12_meses);
        rbIdade13_24Meses = (RadioButton) findViewById(R.id.rbIdade_13_A_24_meses);
        rbIdade25_36Meses = (RadioButton) findViewById(R.id.rbIdade_25_A_36_meses);
        rbIdadeAcima_36Meses = (RadioButton) findViewById(R.id.rbIdade_ACIMA_36_meses);

        rbIdade0_12Meses.setChecked(true);

        rbIdade0_12MesesDesmama = (RadioButton) findViewById(R.id.rbIdade_0_A_12_mesesDesmama);
        rbIdade13_24MesesDesmama = (RadioButton) findViewById(R.id.rbIdade_13_A_24_mesesDesmama);
        rbIdade25_36MesesDesmama = (RadioButton) findViewById(R.id.rbIdade_25_A_36_mesesDesmama);
        rbIdadeAcima_36MesesDesmama= (RadioButton) findViewById(R.id.rbIdade_ACIMA_36_mesesDesmama);;

        rbIdade0_12MesesDesmama.setChecked(true);

        chkAtivo = (CheckBox) findViewById(R.id.chkAtivo);

        chkEngorda = (CheckBox) findViewById(R.id.chkEngorda);
        chkLeite = (CheckBox) findViewById(R.id.chkLeite);
        chkReprodutorMatriz = (CheckBox) findViewById(R.id.chkReprodutor);
                tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();
        tabHost.setOnTabChangedListener(this);


        TabHost.TabSpec tabCadastro = tabHost.newTabSpec("ABA CADASTRO");
        TabHost.TabSpec tabLista = tabHost.newTabSpec("ABA LISTA");

        tabCadastro.setIndicator("Cadastro");
        tabCadastro.setContent(R.id.cadastroBovino);

        tabLista.setIndicator("Lista");
        tabLista.setContent(R.id.listaBovinos);

        tabHost.addTab(tabCadastro);
        tabHost.addTab(tabLista);
        validator = new Validator(this);
        validator.setValidationListener(this);

        edtNrEletronico = (EditText) findViewById(R.id.edtNrEletronico);
        edtNrManejoSisBov = (EditText) findViewById(R.id.edtNrManjSisbov);
        edtCodBarras = (EditText) findViewById(R.id.edtCodBarras);
        edtIdBovino = (EditText) findViewById(R.id.edtIdBovino);
        edtApelido = (EditText) findViewById(R.id.edtApelido);
        edtCodBarras = (EditText) findViewById(R.id.edtCodBarras);
        edtCorPelagem = (EditText) findViewById(R.id.edtCorPelagem);
        edtDtDesmama =(EditText) findViewById(R.id.edtDtDesmama);
        edtDtEntrada = (EditText) findViewById(R.id.edtDtEntrada);
        edtDtInicio = (EditText) findViewById(R.id.edtDtInicio);
        edtDtNascimento =(EditText) findViewById(R.id.edtDtNascimento);
        edtIdRaca = (EditText) findViewById(R.id.edtIdRaca);
        edtNmRaca = (EditText) findViewById(R.id.edtNomeRaca);


        edtIdRaca.setEnabled(false);
        edtNmRaca.setEnabled(false);

       // bovinoHelper.setarMascara(edtDtDesmama, edtDtEntrada, edtDtInicio, edtDtNascimento);

        edtVlrCusto = (EditText) findViewById(R.id.edtVlCusto);

        mLocale = new Locale("pt", "BR");
        edtVlrCusto.addTextChangedListener(new MoneyTextWatcher(edtVlrCusto, mLocale));

        chkAtivo.setChecked(true);
        chkAtivo.setEnabled(false);
        chkLeite.setChecked(true);
        cal = Calendar.getInstance();


        edtDtDesmama.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                chamaOnTouchListener(TipoTxtData.DTDESMAMA.getTipoDataCampo(), edtDtDesmama.getText().toString());

                return false;
            }
        });

        edtDtNascimento.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                chamaOnTouchListener(TipoTxtData.DTNASCIMENTO.getTipoDataCampo(), edtDtNascimento.getText().toString());
                return false;
            }
        });
        edtDtEntrada.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                chamaOnTouchListener(TipoTxtData.DTENTRADA.getTipoDataCampo(), edtDtEntrada.getText().toString());
                return false;
            }
        });
        edtDtInicio.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                chamaOnTouchListener(TipoTxtData.DTINICIO.getTipoDataCampo(), edtDtInicio.getText().toString());
                return false;
            }
        });
        bovinoObj = new Bovino();
        bovinoHelper = new BovinoHelper(this, edtNmRaca, edtIdRaca, edtIdBovino, edtApelido, edtCodBarras, edtCorPelagem, edtDtDesmama, edtDtEntrada, edtDtInicio, edtDtNascimento,
                edtNrEletronico, edtNrManejoSisBov, edtVlrCusto, sexo, chkAtivo, rbMacho, rbFemea, cal, mLocale, bovinoObj, rbIdade0_12Meses, rbIdade13_24Meses, rbIdade25_36Meses, rbIdadeAcima_36Meses,  rbIdade0_12MesesDesmama, rbIdade13_24MesesDesmama, rbIdade25_36MesesDesmama,
                rbIdadeAcima_36MesesDesmama, rdGroup, rdGroupIdadeDesmama, rdGroupIdadeBovino, chkLeite, chkEngorda, chkReprodutorMatriz, rGroupStatus, rbVazia,rbCheia);
       // limparCampos();

    }

    private void chamaOnTouchListener(String view, String dataP) {
       // Calendar calendario = Calendar.getInstance();

       // String dataP = "";
        switch (view){
            case "DESMAMA":
                campoDataSelecionado = TipoTxtData.DTDESMAMA.getTipoDataCampo();
               // dataP = edtDtDesmama.getText().toString();
                break;
            case "ENTRADA":
                campoDataSelecionado = TipoTxtData.DTENTRADA.getTipoDataCampo();
               // dataP = edtDtEntrada.getText().toString();
                break;
            case "NASCIMENTO":
                campoDataSelecionado = TipoTxtData.DTNASCIMENTO.getTipoDataCampo();
               // dataP = edtDtNascimento.getText().toString();
                break;
            case "INICIO":
                campoDataSelecionado = TipoTxtData.DTINICIO.getTipoDataCampo();
             //   dataP = edtDtInicio.getText().toString();
                break;
            default:
                break;

        }

        showDialog(DATE_DIALOG_ID);
    }

    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        bovinoDAO = new BovinoDAO(this);
        if(view.equals(btnCadastrar)){
            try{
                validator.validate();
                if(validacaoOk) {
                    if(edtIdBovino.getText().length()>0){
                        boolean update = bovinoHelper.alterar();
                    }else {
                        Integer id = bovinoHelper.salvar();
                    }
                    validacaoOk = false;
                }
            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }


        }
        if(view == btnEditar){
            if(edtIdBovino.length()> 0){
                validator.validate();
                if(validacaoOk) {

                    boolean update = bovinoHelper.alterar();
                    validacaoOk = false;
                }
            }
        }
        if(view == btnExcluir){
            if(edtIdBovino.length()>0){
                bovinoHelper.excluir();
            }
        }
        if(view == btnPesquisar){
            bovinoHelper.pesquisaBovino();
        }
        if(view == btnLimpar){
            validator.validate();
            bovinoHelper.limparCampos();

        }
        if(view == btnListar){
            bovinoHelper.preencherListView((List<Bovino>) bovinoDAO.getAll(), listViewBovinos, bovinoAdapter);
            bovinoHelper.limparCampos();
            tabHost.setCurrentTab(2);
        }
        if(view == btnPesoLista){
            LayoutInflater li = getLayoutInflater();

            //inflamos o layout alerta.xml na view
            final View viewModalPesoList = li.inflate(R.layout.modal_pesos_lista, null);
            //definimos para o botão do layout um clickListener
            final ListView listViewPeso = (ListView) viewModalPesoList.findViewById(R.id.listViewPesos);
            final PesoHelper pesoHelper = new PesoHelper(BovinoCadastro.this);
            pesoHelper.preencherListView(listViewPeso, edtIdBovino.getText().toString());
            viewModalPesoList.findViewById(R.id.btnSalvarNovoPeso).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    EditText edtPesoNovo = (EditText) viewModalPesoList.findViewById(R.id.edtPesoValorNovo);
                    Peso pesoNovo = new Peso();
                    pesoNovo.setQtdQuilos(BigDecimal.valueOf(Double.parseDouble(edtPesoNovo.getText().toString())));
                    StringBuilder dataBuilder = new StringBuilder();
                    dataBuilder.append(cal.get(Calendar.DAY_OF_MONTH));
                    dataBuilder.append("/");
                    dataBuilder.append(cal.get(Calendar.MONTH));
                    dataBuilder.append("/");
                    dataBuilder.append(cal.get(Calendar.YEAR));
                    pesoNovo.setDtPesagem(DateUtil.covertToDateSql( dataBuilder.toString()));
                    pesoNovo.setIdBovino(Integer.valueOf(edtIdBovino.getText().toString()));
                    if(!edtIdBovino.getText().toString().equals("")){
                        pesoDAO = new PesoDAO(BovinoCadastro.this);
                        pesoDAO.salvar(pesoNovo);

                        pesoHelper.preencherListView(listViewPeso, edtIdBovino.getText().toString());
                    }

                    //desfaz o alerta.
                    alerta.dismiss();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Pesos");
            builder.setView(viewModalPesoList);
            alerta = builder.create();
            alerta.show();
        }
        if(view == btnSelecionarRaca){
            LayoutInflater li = getLayoutInflater();
            //inflamos o layout alerta.xml na view
            final View viewModalRacaList = li.inflate(R.layout.modal_raca_lista, null);
            bovinoHelper.inflaterModalRacaList(viewModalRacaList, edtIdBovino.getText().toString(), alerta);
            btnNovoRacaModal =(Button) viewModalRacaList.findViewById(R.id.btnNovo);
            btnNovoRacaModal.setOnClickListener(this);

        }
        if(view.equals(btnNovoRacaModal)){
            RacaHelper racaHelper = new RacaHelper(this);
            LayoutInflater li = getLayoutInflater();
            final View viewModalRacaCrud = li.inflate(R.layout.modal_raca_crud,null);
            racaHelper.inflaterModalRacaCrud(viewModalRacaCrud, alerta);
            btnSelecionarRaca.setClickable(true);
        }

        if(view.equals(btnLancarDesmama)){
            if(edtDtNascimento.getText().equals("")){
                Toast.makeText(this,"Para Lançar desmama é necessario informar DATA DE NASCIMENTO",Toast.LENGTH_SHORT);
            }else{
                if(desmamaObj == null){
                    desmamaObj = new Desmama();
                }

                desmamaHelper = new DesmamaHelper(this);
                LayoutInflater li = getLayoutInflater();
                final View viewModalDesmamaCrud = li.inflate(R.layout.modal_desmama_crud,null);
                btnSalvarDesmama = viewModalDesmamaCrud.findViewById(R.id.btnSalvar);
                btnSalvarDesmama.setOnClickListener(this);
                desmamaHelper.inflaterModalDesmamaCrud(viewModalDesmamaCrud, alerta, DateUtil.covertToDateSql(edtDtNascimento.getText().toString()), desmamaObj );
                desmamaHelper.recuperarCampos(desmamaObj);
            }

        }

        if(view.equals(btnSalvarDesmama)){
            if(!desmamaHelper.equals(null)){
                desmamaObj = new Desmama();
               desmamaHelper.recuperarCampos(desmamaObj);
               desmamaHelper.closeModalDesmamaCrud();
            }
        }
    }

    @Override
    public void onValidationSucceeded() {
        validacaoOk = true;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(this));
            }
            iterator.remove();
        }
    }

    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.rbFeminino:
                if (checked)
                    sexo = "F";
                    break;
            case R.id.rbMasculino:
                if (checked)
                    sexo="M";
                    break;
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object bovino =  parent.getItemAtPosition(position);
        if(bovino != null){
            bovinoHelper.setarCampos((Bovino) bovino);
            tabHost.setCurrentTab(0);
        }


    }


    @Override
    protected Dialog onCreateDialog(int id) {
        DataBovino dataBovino = new DataBovino();
        dataBovino = tramentoData();
        switch (id) {
            case DATE_DIALOG_ID:
                datePickerDialog = new DatePickerDialog(this, this, dataBovino.ano, dataBovino.mes+1,  dataBovino.dia);

                datePickerDialog.setOnShowListener(this);
                return datePickerDialog;

        }

        return null;
    }
    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear,
                             int dayOfMonth) {
        int ano, mes, dia;
        dia = dayOfMonth;
        mes = monthOfYear+1;
        ano = year;
        String data = String.valueOf(dia) + "/"
                + String.valueOf(mes) + "/" + String.valueOf(ano);
        if(campoDataSelecionado!=null) {
            switch (campoDataSelecionado) {
                case "DESMAMA":
//                    edtDtDesmama.setText(data);
                    bovinoHelper.setarDataCampo(campoDataSelecionado, data);
                    break;
                case "NASCIMENTO":
//                    edtDtNascimento.setText(data);
                    bovinoHelper.setarDataCampo(campoDataSelecionado, data);
                    break;
                case "ENTRADA":
                    edtDtEntrada.setText(data);
                    break;
                case "INICIO":
                    edtDtInicio.setText(data);
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "ABA LISTA":
                bovinoDAO = new BovinoDAO(this);
                bovinoHelper.preencherListView((List<Bovino>) bovinoDAO.getAll(), listViewBovinos,bovinoAdapter);
              //  preencherListView((List<Bovino>) bovinoDAO.getAll());
                bovinoDAO.close();
                break;
            default:
                break;
        }
    }

    @Override
    public void onShow(DialogInterface dialogInterface) {
        DataBovino dataBovino = tramentoData();


        datePickerDialog.updateDate(dataBovino.ano,dataBovino.mes-1,dataBovino.dia);

    }

    private DataBovino tramentoData() {
        DataBovino dataBovino= new DataBovino();
        int diaD ;
        int mesD ;
        int anoD ;
        String dataP = "";
        switch (campoDataSelecionado){
            case "DESMAMA":
                dataP = edtDtDesmama.getText().toString();
                break;
            case "ENTRADA":
                dataP = edtDtEntrada.getText().toString();
                break;
            case "NASCIMENTO":
                dataP = edtDtNascimento.getText().toString();
                break;
            case "INICIO":
                dataP = edtDtInicio.getText().toString();
                break;
            default:
                break;

        }


        if(dataP.length()> 0){
            dataBovino.dia = Integer.valueOf(dataP.substring(0, dataP.indexOf("/")));
            dataP = dataP.substring(dataP.indexOf("/")+1, dataP.length() );
            dataBovino.mes = Integer.valueOf(dataP.substring(0, dataP.indexOf("/")));
            dataP = dataP.substring(dataP.indexOf("/")+1, dataP.length());
            dataBovino.ano = Integer.valueOf(dataP.substring(0, dataP.length()));

        }else{

            dataBovino.ano = cal.get(Calendar.YEAR);
            dataBovino.mes  = cal.get(Calendar.MONTH);
            dataBovino.dia  = cal.get(Calendar.DAY_OF_MONTH);
        }
        return dataBovino;
    }


}
