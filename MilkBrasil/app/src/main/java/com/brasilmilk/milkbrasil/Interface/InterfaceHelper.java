package com.brasilmilk.milkbrasil.Interface;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

/**
 * Created by ricar on 13/01/2018.
 */

public interface InterfaceHelper {
    public void limparCampos();
    public void setarCampos();
    public void recuperarCampos();
    public boolean excluir();
    public int salvar();
    public void preencherListView(final RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface);
    public void setaRecyclerView(RecyclerView mRecyclerView);
    public void setarMascara();
    public void inflateModalList(final View view, AlertDialog alerta);
}
