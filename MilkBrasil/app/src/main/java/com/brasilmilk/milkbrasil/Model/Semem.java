package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.util.List;



/**
 * The persistent class for the semem database table.
 * 
 */
@ClassAnotation(nomeTabela = "SEMEM")
public class Semem {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_SEMEM", tipoAtributo = "INTEGER", nomeSet = "setIdSemem", nomeGet = "getIdSemem", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idSemem;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "OBS", nomeGet = "getObs", nomeSet = "setObs")
	private String obs;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGSEMEMCOMERCIAL", nomeGet = "isFlgSememComercial", nomeSet = "setFlgSememComercial", tipoAtributo = "INTEGER")
	private boolean flgSememComercial;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_DOADOR", tipoAtributo = "INTEGER", nomeGet = "getIdDoador", nomeSet = "setIdDoador",  forengKey = true, referencesTable = "BOVINO", fieldReferences = "ID_BOVINO")
	private Integer idDoador;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_PRODUTO", tipoAtributo = "INTEGER", nomeGet = "getIdProduto", nomeSet = "setIdProduto",  forengKey = true, referencesTable = "PRODUTO", fieldReferences = "ID_PRODUTO")
	private Integer idProduto;

	public Semem(Integer idSemem, String obs, Integer idDoador) {
		this.idSemem = idSemem;
		this.obs = obs;
		this.idDoador = idDoador;
	}

	public Semem() {

	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdSemem() {
		return idSemem;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdSemem(Integer idSemem) {
		this.idSemem = idSemem;
	}

	@MetodoAnotation()
	public String getObs() {
		return obs;
	}
	@MetodoAnotation()
	public void setObs(String obs) {
		this.obs = obs;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdDoador() {
		return idDoador;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdDoador(Integer idDoador) {
		this.idDoador = idDoador;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdProduto() {
		return idProduto;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public boolean isFlgSememComercial() {
		return flgSememComercial;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgSememComercial(boolean flgSememComercial) {
		this.flgSememComercial = flgSememComercial;
	}
}