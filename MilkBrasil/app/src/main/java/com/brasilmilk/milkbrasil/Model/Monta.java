package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.sql.Date;

@ClassAnotation(nomeTabela = "MONTA")
public class Monta  {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_MONTA", tipoAtributo = "INTEGER", nomeSet = "setIdMonta", nomeGet = "getIdMonta", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idMonta;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTMONTA", nomeGet = "getDtMonta", nomeSet = "setDtMonta")
	private Date dtMonta;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTPREVPARTO", nomeGet = "getDtPrevParto", nomeSet = "setDtPrevParto")
	private Date dtPrevParto;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTPREVRESULTADO", nomeGet = "getDtPrevResultado", nomeSet = "setDtPrevResultado")
	private Date dtPrevResultado;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTRESULTADO", nomeGet = "getDtResultado", nomeSet = "setDtResultado")
	private Date dtResultado;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGVAZIA", tipoAtributo = "INTEGER", nomeGet = "isFlgVazia", nomeSet = "setFlgVazia")
	private Boolean flgVazia;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGCHEIA", tipoAtributo = "INTEGER", nomeGet = "isFlgCheia", nomeSet = "setFlgCheia")
	private Boolean flgCheia;


	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_MATRIZ", tipoAtributo = "INTEGER", nomeGet = "getIdMatriz", nomeSet = "setIdMatriz",  forengKey = true, referencesTable = "BOVINO", fieldReferences = "ID_BOVINO", notNull = true)
	private Integer idMatriz;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_SEMEM", tipoAtributo = "INTEGER", nomeGet = "getIdSemem", nomeSet = "setIdSemem",  forengKey = true, referencesTable = "SEMEM", fieldReferences = "ID_SEMEM", notNull = true)
	private Integer idSemem;

	public Monta() {

	}
	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdMonta() {
		return idMonta;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdMonta(Integer idMonta) {
		this.idMonta = idMonta;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtMonta() {
		return dtMonta;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtMonta(Date dtMonta) {
		this.dtMonta = dtMonta;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtPrevParto() {
		return dtPrevParto;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtPrevParto(Date dtPrevParto) {
		this.dtPrevParto = dtPrevParto;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtPrevResultado() {
		return dtPrevResultado;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtPrevResultado(Date dtPrevResultado) {
		this.dtPrevResultado = dtPrevResultado;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtResultado() {
		return dtResultado;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtResultado(Date dtResultado) {
		this.dtResultado = dtResultado;
	}


	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgVazia(Boolean flgVazia) {
		this.flgVazia = flgVazia;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public Boolean isFlgVazia() {
		return flgVazia;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public Boolean isFlgCheia() {
		return flgCheia;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgCheia(Boolean flgCheia) {
		this.flgCheia = flgCheia;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdSemem() {
		return idSemem;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdSemem(Integer idSemem) {
		this.idSemem = idSemem;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdMatriz() {
		return idMatriz;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdMatriz(Integer idMatriz) {
		this.idMatriz = idMatriz;
	}
}