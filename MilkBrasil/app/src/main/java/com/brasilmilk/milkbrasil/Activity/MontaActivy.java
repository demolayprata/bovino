package com.brasilmilk.milkbrasil.Activity;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TabHost;

import com.brasilmilk.milkbrasil.Fragments.MenuFragment;
import com.brasilmilk.milkbrasil.Fragments.MontaFragment;
import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.R;

public class MontaActivy extends AppCompatActivity implements MontaFragment.OnItemSelectedListener, ButtonReferenceInterface {


    private Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar;
    private TabHost tabHost;
    private FragmentHelper fragmentHelper;
    private FragmentManager fm;
    private MontaFragment montaFragment;
    private RecyclerView recyclerViewList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_monta);


        fm = getSupportFragmentManager();
        fragmentHelper = new FragmentHelper(fm);
        MenuFragment menuFragment = new MenuFragment();
        fragmentHelper.adicionaFragment(R.id.fragment_content, menuFragment);




//
//        MontaFragment montaFragment = new MontaFragment();
//        fragmentHelper.adicionaFragment(R.id.fragment_content, montaFragment);
//        montaFragment.passarReferenciaButton(btnCadastrar, btnEditar, btnExcluir, btnLimpar,btnPesquisar, btnListar, tabHost);


    }

    @Override
    public void onItemSelected(Monta monta) {



        // Código que interague com outros componente, inclusive Fragments
    }

    @Override
    public void passarReferencia(Button btnCadastrar, Button btnEditar, Button btnExcluir, Button btnLimpar,
                                 Button btnPesquisar, Button btnListar, TabHost tabHost, RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnEditar = btnEditar;
        this.btnExcluir = btnExcluir;
        this.btnLimpar = btnLimpar;
        this.btnPesquisar = btnPesquisar;
        this.btnListar = btnListar;
        this.tabHost = tabHost;

       this.tabHost.setup();

        TabHost.TabSpec tabCadastro = tabHost.newTabSpec("ABA CADASTRO");
        TabHost.TabSpec tabLista = tabHost.newTabSpec("ABA LISTA");

        tabCadastro.setIndicator("Lançar Monta");
        tabCadastro.setContent(R.id.fragment_crud);

        tabLista.setIndicator("Lista");
        tabLista.setContent(R.id.recycler_listVMonta);
//
        tabHost.addTab(tabCadastro);
        tabHost.addTab(tabLista);

        montaFragment = new MontaFragment();
        fragmentHelper.adicionaFragment(R.id.fragment_crud, montaFragment);

        recyclerViewList = recyclerView;

        montaFragment.passarReferenciaButton(this.btnCadastrar, this.btnEditar, this.btnExcluir, this.btnLimpar,
                this.btnPesquisar, this.btnListar, this.tabHost, recyclerViewList);
    }
}
