package com.brasilmilk.milkbrasil.Helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.brasilmilk.milkbrasil.Adapter.AdapterDesmamaRecView;
import com.brasilmilk.milkbrasil.Adapter.AdapterLoteRecView;
import com.brasilmilk.milkbrasil.DAO.DesmamaDAO;
import com.brasilmilk.milkbrasil.DAO.LoteDAO;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Lote;

import java.util.List;

/**
 * Created by ricar on 24/12/2017.
 */

public class LoteHelper {
    private Context ctx;
    private LoteDAO loteDAO;
    private AdapterLoteRecView loteAdapter;
    private List<Lote> loteList;

    public LoteHelper(Context ctx){
        this.ctx = ctx;
    }
    public void preencherListView(final RecyclerView mRecyclerView, String idBovino) {
        if(idBovino.length()>0){
            loteDAO = new LoteDAO(ctx);
            loteList = (List<Lote>) loteDAO.getAll();

            loteAdapter = new AdapterLoteRecView(ctx, loteList, (ClickRecyclerView_Interface) this);
           // mRecyclerView.removeAllViewsInLayout();
            mRecyclerView.setAdapter(loteAdapter);
        }
    }

}
