package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

/**
 * Created by ricar on 25/11/2017.
 */
@ClassAnotation(nomeTabela = "CIDADE")
public class Cidade {
    @ApresentacaoAN
    @AtributoAnotation(nomeAtributo = "ID_CIDADE", tipoAtributo = "INTEGER", nomeSet = "setCodCidade", nomeGet = "getCodCidade", primaryKeyAtributo = true, autoIncrementAtributo = true)
    private  Integer codCidade;

    @ApresentacaoAN
    @AtributoAnotation(nomeAtributo = "NM_CIDADE", nomeGet = "getNmCidade", nomeSet = "setNmCidade", unique = true)
    private String nmCidade;

    public Cidade(Integer codCidade, String nmCidade) {
        this.codCidade = codCidade;
        this.nmCidade = nmCidade;
    }
    public Cidade() {
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getCodCidade() {
        return codCidade;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setCodCidade(Integer codCidade) {
        this.codCidade = codCidade;
    }

    @MetodoAnotation
    public String getNmCidade() {
        return nmCidade;
    }

    @MetodoAnotation
    public void setNmCidade(String nmCidade) {
        this.nmCidade = nmCidade;
    }
}
