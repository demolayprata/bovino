package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.math.BigDecimal;
import java.sql.Date;

/**
 * Created by ricar on 26/12/2017.
 */


@ClassAnotation(nomeTabela = "PRODUTO")
public class Produto {
    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "ID_PRODUTO", tipoAtributo = "INTEGER", nomeSet = "setIdProduto", nomeGet = "getIdProduto", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
    private Integer idProduto;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "QTDSEGURANCA", tipoAtributo = "DOUBLE", nomeSet = "setQtdSeguranca", nomeGet = "getQtdSeguranca")
    private BigDecimal qtdSeguranca;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DESPRODUTO",  nomeSet = "setDesProduto", nomeGet = "getDesProduto")
    private String desProduto;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "QTDPRODUTO", tipoAtributo = "DOUBLE", nomeSet = "setQtdProduto", nomeGet = "getQtdProduto")
    private BigDecimal qtdProduto;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "PRECOMEDIO", tipoAtributo = "DOUBLE", nomeSet = "setPrecoMedio", nomeGet = "getPrecoMedio")
    private BigDecimal precoMedio;


    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTVALIDADE", nomeSet = "setDtValidade", nomeGet = "getDtValidade")
    private Date dtValidade;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "DTFABRICACAO", nomeSet = "setDtFabricacao", nomeGet = "getDtFabricacao")
    private Date dtFabricacao;

    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "CUSTOPRODUTO", tipoAtributo = "DOUBLE", nomeSet = "setCustoProduto", nomeGet = "getCustoProduto")
    private BigDecimal vlrCustoProduto;

    public Produto(Integer idProduto, BigDecimal qtdSeguranca, String desProduto, BigDecimal qtdProduto, BigDecimal precoMedio, Date dtValidade, Date dtFabricacao, BigDecimal vlrCustoProduto) {
        this.idProduto = idProduto;
        this.qtdSeguranca = qtdSeguranca;
        this.desProduto = desProduto;
        this.qtdProduto = qtdProduto;
        this.precoMedio = precoMedio;
        this.dtValidade = dtValidade;
        this.dtFabricacao = dtFabricacao;
        this.vlrCustoProduto = vlrCustoProduto;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getIdProduto() {
        return idProduto;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setIdProduto(Integer idProduto) {
        this.idProduto = idProduto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public BigDecimal getQtdSeguranca() {
        return qtdSeguranca;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public void setQtdSeguranca(BigDecimal qtdSeguranca) {
        this.qtdSeguranca = qtdSeguranca;
    }

    @MetodoAnotation
    public String getDesProduto() {
        return desProduto;
    }

    @MetodoAnotation
    public void setDesProduto(String desProduto) {
        this.desProduto = desProduto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public BigDecimal getQtdProduto() {
        return qtdProduto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public void setQtdProduto(BigDecimal qtdProduto) {
        this.qtdProduto = qtdProduto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public BigDecimal getPrecoMedio() {
        return precoMedio;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public void setPrecoMedio(BigDecimal precoMedio) {
        this.precoMedio = precoMedio;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtValidade() {
        return dtValidade;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtValidade(Date dtValidade) {
        this.dtValidade = dtValidade;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public Date getDtFabricacao() {
        return dtFabricacao;
    }

    @MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
    public void setDtFabricacao(Date dtFabricacao) {
        this.dtFabricacao = dtFabricacao;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public BigDecimal getVlrCustoProduto() {
        return vlrCustoProduto;
    }

    @MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
    public void setVlrCustoProduto(BigDecimal vlrCustoProduto) {
        this.vlrCustoProduto = vlrCustoProduto;
    }
}
