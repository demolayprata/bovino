package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;

@ClassAnotation(nomeTabela = "SAIDABOVINO")
public class SaidaBovino {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_SAIDA", tipoAtributo = "INTEGER", nomeSet = "setIdSaida", nomeGet = "getIdSaida", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idSaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTSAIDA", nomeGet = "getDtSaida", nomeSet = "setDtSaida")
	private Date dtSaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGFRIGORIFICO", tipoAtributo = "INTEGER", nomeGet = "isFlgFrigorifico", nomeSet = "setFlgFrigorifico")
	private boolean flgFrigorifico;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "FLGTERCEIROS", tipoAtributo = "INTEGER", nomeGet = "isFlgTerceiros", nomeSet = "setFlgTerceiros")
	private boolean flgTerceiros;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "OBS", nomeGet = "getObs", nomeSet = "setObs")
	private String obs;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDBOVINOS",tipoAtributo = "INTEGER", nomeGet = "getQtdAnimais", nomeSet = "setQtdAnimais")
	private Integer qtdAnimais;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDKGMORTOMEDIOINDIV", nomeGet = "getQtdKgMortoMedioIndiv", nomeSet = "setQtdKgMortoMedioIndiv", tipoAtributo = "DOUBLE")
	private BigDecimal qtdKgMortoMedioIndiv;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDKGVIVOMEDIOINDIV", nomeGet = "getQtdKgVivoMedioIndiv", nomeSet = "setQtdKgVivoMedioIndiv", tipoAtributo = "DOUBLE")
	private BigDecimal qtdKgVivoMedioIndiv;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "VLRARROBA", nomeGet = "getVlrArroba", nomeSet = "setVlrArroba", tipoAtributo = "DOUBLE")
	private BigDecimal vlrArroba;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "VLRTOTAL", nomeGet = "getVlrTotal", nomeSet = "setVlrTotal", tipoAtributo = "DOUBLE")
	private BigDecimal vlrTotal;

	//bi-directional many-to-one association to ProgSaida


	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_PROGSAIDA", nomeGet = "getIdProgSaida", nomeSet = "setIdProgSaida", tipoAtributo = "INTEGER", forengKey = true, referencesTable = "PROGSAIDA", fieldReferences = "ID_PROGSAIDA", notNull = true)
	private Integer idProgSaida;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_NOTAFISCAL", nomeGet = "getIdNotaFiscal", nomeSet = "setIdNotaFiscal", tipoAtributo = "INTEGER", forengKey = true, referencesTable = "NOTAFISCAL", fieldReferences = "ID_NOTAFISCAL", notNull = true)
	private Integer idNotafiscal;


	public SaidaBovino(Integer idSaida, Date dtSaida, boolean flgFrigorifico, boolean flgTerceiros, String obs, Integer qtdAnimais, BigDecimal qtdKgMortoMedioIndiv, BigDecimal qtdKgVivoMedioIndiv, BigDecimal vlrArroba, BigDecimal vlrTotal, Integer idProgSaida, Integer idNotafiscal) {
		this.idSaida = idSaida;
		this.dtSaida = dtSaida;
		this.flgFrigorifico = flgFrigorifico;
		this.flgTerceiros = flgTerceiros;
		this.obs = obs;
		this.qtdAnimais = qtdAnimais;
		this.qtdKgMortoMedioIndiv = qtdKgMortoMedioIndiv;
		this.qtdKgVivoMedioIndiv = qtdKgVivoMedioIndiv;
		this.vlrArroba = vlrArroba;
		this.vlrTotal = vlrTotal;
		this.idProgSaida = idProgSaida;
		this.idNotafiscal = idNotafiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdSaida() {
		return idSaida;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdSaida(Integer idSaida) {
		this.idSaida = idSaida;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtSaida() {
		return dtSaida;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtSaida(Date dtSaida) {
		this.dtSaida = dtSaida;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public boolean isFlgFrigorifico() {
		return flgFrigorifico;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgFrigorifico(boolean flgFrigorifico) {
		this.flgFrigorifico = flgFrigorifico;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public boolean isFlgTerceiros() {
		return flgTerceiros;
	}

	@MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
	public void setFlgTerceiros(boolean flgTerceiros) {
		this.flgTerceiros = flgTerceiros;
	}

	@MetodoAnotation()
	public String getObs() {
		return obs;
	}

	@MetodoAnotation()
	public void setObs(String obs) {
		this.obs = obs;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getQtdAnimais() {
		return qtdAnimais;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setQtdAnimais(Integer qtdAnimais) {
		this.qtdAnimais = qtdAnimais;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdKgMortoMedioIndiv() {
		return qtdKgMortoMedioIndiv;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdKgMortoMedioIndiv(BigDecimal qtdKgMortoMedioIndiv) {
		this.qtdKgMortoMedioIndiv = qtdKgMortoMedioIndiv;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdKgVivoMedioIndiv() {
		return qtdKgVivoMedioIndiv;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdKgVivoMedioIndiv(BigDecimal qtdKgVivoMedioIndiv) {
		this.qtdKgVivoMedioIndiv = qtdKgVivoMedioIndiv;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrArroba() {
		return vlrArroba;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrArroba(BigDecimal vlrArroba) {
		this.vlrArroba = vlrArroba;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrTotal() {
		return vlrTotal;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrTotal(BigDecimal vlrTotal) {
		this.vlrTotal = vlrTotal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdProgSaida() {
		return idProgSaida;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdProgSaida(Integer idProgSaida) {
		this.idProgSaida = idProgSaida;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdNotafiscal() {
		return idNotafiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdNotafiscal(Integer idNotafiscal) {
		this.idNotafiscal = idNotafiscal;
	}
}