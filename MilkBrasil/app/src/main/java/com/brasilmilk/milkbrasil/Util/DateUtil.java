package com.brasilmilk.milkbrasil.Util;

import android.annotation.SuppressLint;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

import android.location.Location;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;


import com.brasilmilk.milkbrasil.Enum.IdadeBovinoEnum;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

/**
 * Created by ricar on 18/12/2017.
 */

public class DateUtil {

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static java.sql.Date covertToDateSql(String data){
        data= formatDDMMAAAA(data);
        java.sql.Date sql = null;
        try {
            SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");

            Date parsed = null;
            parsed =  format.parse(data);


            sql = new java.sql.Date(parsed.getTime());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sql;
    }

    public static String formatFieldDate(String data){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return  format.format(data);
    }

    public  static String formatFieldToTxt(java.sql.Date data){
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(data);
    }

    public static String formatDDMMAAAA(String data){
        int tamanho = data.contains("/")?data.substring(0,data.indexOf("/")).length():data.substring(0,data.indexOf("-")).length();
        if( tamanho >2){
            String ano, mes, dia, separador;
            separador = data.contains("-")?"-":"/";
            ano = data.substring(0,data.indexOf(separador));
            data = data.substring(data.indexOf(separador)+1,data.length());
            mes = data.substring(0,data.indexOf(separador));
            data = data.substring(data.indexOf(separador)+1,data.length());
            dia = data;
            StringBuilder dataBuilder = new StringBuilder();
            dataBuilder.append(dia);
            dataBuilder.append("/");
            dataBuilder.append(mes);
            dataBuilder.append("/");
            dataBuilder.append(ano);
            data = dataBuilder.toString();
        }
        return data;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String calculaIdade(java.sql.Date dtNascimento){

        int idadeMeses =0;
        String idadeStr = "";

        java.sql.Date dtNow = new java.sql.Date(Calendar.getInstance().getTimeInMillis());
        try {

            long dt = (dtNascimento.getTime() - dtNow.getTime()) + 3600000; // 1 hora para compensar horário de verão
            idadeMeses =(int) Math.ceil((dt/86400000L))/30;
           idadeMeses = idadeMeses < -1? idadeMeses * (-1):idadeMeses;
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }


        if(idadeMeses <= 12){
            idadeStr = IdadeBovinoEnum.BEZERROS_0_A_12_MESES.getIdadeBovino();
        }else
            if(idadeMeses > 12 && idadeMeses < 25){
            idadeStr = IdadeBovinoEnum.NOVILHOS_13_A_24_MESES.getIdadeBovino();
            }
            else
                if(idadeMeses > 24 && idadeMeses < 37){
                idadeStr = IdadeBovinoEnum.NOVILHOS_25_A_36_MESES.getIdadeBovino();
                }else{
                    idadeStr = IdadeBovinoEnum.NOVILHOS_ACIMA_37_MESES.getIdadeBovino();
                }

        return idadeStr;
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public static String calculaIdadeByDesmama(java.sql.Date dtNascimento, java.sql.Date dtDesmama){

        int idadeMeses =0;


        try {

            long dt = (dtNascimento.getTime() - dtDesmama.getTime()) + 3600000; // 1 hora para compensar horário de verão
            idadeMeses = (int)Math.ceil((dt/86400000L))*30;
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        String idadeStr = "";

        if(idadeMeses <= 12){
            idadeStr = IdadeBovinoEnum.BEZERROS_0_A_12_MESES.getIdadeBovino();
        }else
        if(idadeMeses > 12 && idadeMeses < 25){
            idadeStr = IdadeBovinoEnum.NOVILHOS_13_A_24_MESES.getIdadeBovino();
        }
        else
        if(idadeMeses > 24 && idadeMeses < 37){
            idadeStr = IdadeBovinoEnum.NOVILHOS_25_A_36_MESES.getIdadeBovino();
        }else{
            idadeStr = IdadeBovinoEnum.NOVILHOS_ACIMA_37_MESES.getIdadeBovino();
        }

        return idadeStr;
    }

    static  public java.sql.Date ajusteDateNascimentoByIdadeRB(String idadeStr){
        int idadeMeses =0;

        Locale mLocale = new Locale("pt", "BR");
        java.sql.Date dtNow = new java.sql.Date(Calendar.getInstance().getTimeInMillis());

        Calendar dataAtual = Calendar.getInstance(mLocale);


        switch (idadeStr){
            case "NOVILHOS (OS/AS) 13 A 24 MESES":
                dataAtual.add(Calendar.DATE, (30*13)*(-1));
                dtNow = new java.sql.Date(dataAtual.getTimeInMillis());
                break;
            case "NOVILHOS (OS/AS) 25 A 36 MESES":
                dataAtual.add(Calendar.DATE, (30*25)*(-1));
                dtNow = new java.sql.Date(dataAtual.getTimeInMillis());
                break;
            case "NOVILHOS (OS/AS) ACIMA DE 37 MESES":
                dataAtual.add(Calendar.DATE, (30*37)*(-1));
                dtNow = new java.sql.Date(dataAtual.getTimeInMillis());
                break;
            default:
                dtNow = new java.sql.Date(dataAtual.getTimeInMillis());
                break;
        }



        return dtNow;
    }

    public static String calculaDataPrevisaoResultado(java.sql.Date dtDiaMonta){
        Locale mLocale = new Locale("pt", "BR");
        GregorianCalendar  dataAtualCalender = new GregorianCalendar();
        dataAtualCalender.setTime(dtDiaMonta);
        dataAtualCalender.add(Calendar.DAY_OF_MONTH,45);
        java.sql.Date dateSql = new java.sql.Date(dataAtualCalender.getTimeInMillis());
        return formatFieldToTxt( dateSql);

    }

    public static String calculaDataPrevisaoParto(java.sql.Date dtDiaMonta){
        Locale mLocale = new Locale("pt", "BR");
        GregorianCalendar  dataAtualCalender = new GregorianCalendar();
        dataAtualCalender.setTime(dtDiaMonta);
        dataAtualCalender.add(Calendar.DAY_OF_MONTH,290);
        java.sql.Date dateSql = new java.sql.Date(dataAtualCalender.getTimeInMillis());
        return formatFieldToTxt( dateSql);

    }

    public static String gerarDateNow(){
        Locale mLocale = new Locale("pt", "BR");
        Calendar dataAtualCalender = Calendar.getInstance(mLocale);
        Date parsed = dataAtualCalender.getTime();
        java.sql.Date dateSql = new java.sql.Date(parsed.getTime());
        return formatFieldToTxt( dateSql);
    }

    public static java.sql.Date gerarDateNowSql(){
        Locale mLocale = new Locale("pt", "BR");
        Calendar dataAtualCalender = Calendar.getInstance(mLocale);
        Date parsed = dataAtualCalender.getTime();
        java.sql.Date dateSql = new java.sql.Date(parsed.getTime());
        return dateSql;
    }

}
