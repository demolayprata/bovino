package com.brasilmilk.milkbrasil.Helper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Cidade;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.EntradaBovino;
import com.brasilmilk.milkbrasil.Model.EntradaProduto;
import com.brasilmilk.milkbrasil.Model.Estado;
import com.brasilmilk.milkbrasil.Model.Lote;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.Model.Notafiscal;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.Model.Produto;
import com.brasilmilk.milkbrasil.Model.ProgSaida;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Model.Raca;
import com.brasilmilk.milkbrasil.Model.SaidaBovino;
import com.brasilmilk.milkbrasil.Model.Semem;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ricar on 20/11/2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {
    public static final String NOME_BD = "APP_MILKBRASIL";
    public static  final  int VERSAO_BD = 1;
    private List<String> scriptCreateList;
    private String scriptDelete;
    public SQLiteHelper(Context context, String nome_bd, int versao_bd,  String scriptCreate, String scriptDelete) {
        super(context, nome_bd, null, versao_bd);
        scriptCreateList = new ArrayList<>();
        geraScriptDelete();
        geraScriptTabela();

//        this.scriptCreate = scriptCreate;
//        this.scriptDelete = scriptDelete;

    }
    private void geraScriptTabela(){
            StringBuilder scriptBuilder = new StringBuilder();
            scriptBuilder.append(Util.getSQLCreate(Estado.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Cidade.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Bovino.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Peso.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Desmama.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(EntradaProduto.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(EntradaBovino.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Lote.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Monta.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Nascimento.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Notafiscal.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Produto.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(ProgSaida.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Raca.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(SaidaBovino.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);

            scriptBuilder.append(Util.getSQLCreate(Semem.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);


            scriptBuilder.append(Util.getSQLCreate(Propriedade.class));
            scriptCreateList.add(scriptBuilder.toString());
            scriptBuilder.setLength(0);
    }
    private void geraScriptDelete(){
        StringBuilder scriptBuilderDelete = new StringBuilder();
        scriptBuilderDelete.append(Util.getDROPSQLite(Peso.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Cidade.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Estado.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Propriedade.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Bovino.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Desmama.class));

        scriptBuilderDelete.append(Util.getDROPSQLite(EntradaBovino.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(EntradaProduto.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Lote.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Monta.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Nascimento.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Notafiscal.class));

        scriptBuilderDelete.append(Util.getDROPSQLite(Produto.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(ProgSaida.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Propriedade.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Raca.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(SaidaBovino.class));
        scriptBuilderDelete.append(Util.getDROPSQLite(Semem.class));
        this.scriptDelete = scriptBuilderDelete.toString();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for(String scriptCreate : scriptCreateList) {
            try {
                db.execSQL(scriptCreate);
            }catch (Exception e){
                Log.e("Erro: ", e.getMessage());
            }

        }
    }


    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
