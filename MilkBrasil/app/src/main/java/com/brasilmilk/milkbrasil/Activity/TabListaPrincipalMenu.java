package com.brasilmilk.milkbrasil.Activity;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.brasilmilk.milkbrasil.R;

public class TabListaPrincipalMenu extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_principal_menu);
    }
}
