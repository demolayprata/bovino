package com.brasilmilk.milkbrasil.Util;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.os.Bundle;

import android.app.DatePickerDialog;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;

/**
 * Created by ricar on 15/12/2017.
 */

public class DatePickerFragmentUtil extends DialogFragment implements DatePickerDialog.OnDateSetListener {
    private int Ano, Mes, Dia;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState)
    {
        final Calendar calendario = Calendar.getInstance();
        Ano = calendario.get(Calendar.YEAR);
        Mes = calendario.get(Calendar.MONTH);
        Dia = calendario.get(Calendar.DAY_OF_MONTH);
        return new DatePickerDialog(getActivity(), this, Ano, Mes, Dia);
    }
    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Ano = year;
        Mes = month;
        Dia = day;
        AtualizarData();
        MensagemData();
    }

    private void MensagemData() {
       // Toast.makeText(this, new StringBuilder().append("Data: ").append(txtData.getText()),  Toast.LENGTH_SHORT).show();
    }

    private void AtualizarData() {
       // txtData.setText(new StringBuilder().append(Dia).append("/").append(Mes + 1).append("/").append(Ano).append(" "));
    }
   @Override
    public int show(FragmentTransaction transaction, String tag)
    {
        return super.show(transaction, tag);
    }
}

