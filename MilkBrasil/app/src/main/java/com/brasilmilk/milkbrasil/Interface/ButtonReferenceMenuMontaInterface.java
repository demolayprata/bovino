package com.brasilmilk.milkbrasil.Interface;

import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.TabHost;

/**
 * Created by ricar on 19/01/2018.
 */

public interface ButtonReferenceMenuMontaInterface {
    public void passarReferencia(final Button btnLancarMonta, final Button btnLancarResultado);
}
