package com.brasilmilk.milkbrasil.Fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.brasilmilk.milkbrasil.Helper.FragmentHelper;
import com.brasilmilk.milkbrasil.R;

public class MovimentoFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private Context ctx;
    public MovimentoFragment() {
        // Required empty public constructor
    }


    public static MovimentoFragment newInstance(String param1, String param2) {
        MovimentoFragment fragment = new MovimentoFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movimento, container, false);
        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.ctx = ctx;
    }




    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }
}
