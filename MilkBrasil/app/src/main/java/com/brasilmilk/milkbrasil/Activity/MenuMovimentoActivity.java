package com.brasilmilk.milkbrasil.Activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.brasilmilk.milkbrasil.Fragments.MenuMontaFragment;
import com.brasilmilk.milkbrasil.R;

public class MenuMovimentoActivity extends AppCompatActivity implements View.OnClickListener {
    private Button btnMonta;
    private Button btnNascimento;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_movimento);
        this.btnMonta = findViewById(R.id.btnMonta);
        this.btnMonta.setOnClickListener(this);
        this.btnNascimento = findViewById(R.id.btnNascimento);
        btnNascimento.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if(view.equals(btnMonta)){
            Intent intent = new Intent(this, MenuMontaActivity.class);
            startActivity(intent);
        }
        if(view.equals(btnNascimento)){
            Intent intent = new Intent(this, NascimentoActivity.class);
            startActivity(intent);
        }
    }
}
