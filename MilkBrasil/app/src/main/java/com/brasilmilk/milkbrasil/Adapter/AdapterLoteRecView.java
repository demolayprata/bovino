package com.brasilmilk.milkbrasil.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Desmama;
import com.brasilmilk.milkbrasil.Model.Lote;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.List;

/**
 * Created by ricar on 11/12/2017.
 */

public class AdapterLoteRecView extends  RecyclerView.Adapter<AdapterLoteRecView.RecyclerTesteViewHolder> {

public static ClickRecyclerView_Interface clickRecyclerViewInterface;
        Context mctx;
    private List<Lote> mList;




public AdapterLoteRecView(Context ctx, List<Lote> list, ClickRecyclerView_Interface clickRecyclerViewInterface) {
        this.mctx = ctx;
        this.mList = list;
        this.clickRecyclerViewInterface = clickRecyclerViewInterface;

        }

@Override
public RecyclerTesteViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(mctx).inflate(R.layout.adapter_lote,null);


        return new RecyclerTesteViewHolder(itemView);
        }

    @SuppressLint("WrongConstant")
    @Override
public void onBindViewHolder(RecyclerTesteViewHolder viewHolder,final int position) {
        Lote lote = mList.get(position);

            viewHolder.txtCodLote.setText("COD Lote: " + lote.getIdLote());
            viewHolder.txtNmLote.setText("Nome Lote: " + lote.getNmLote());
            viewHolder.txtDtCriacaoLote.setText("Data Criação: " + DateUtil.formatFieldToTxt(lote.getDtCriacao()));
            viewHolder.txtAtivoLote.setText("OBS: " + lote.getObs());
        }

@Override
public int getItemCount() {
        return mList.size();
        }


protected class RecyclerTesteViewHolder extends RecyclerView.ViewHolder {

    protected  TextView txtCodLote, txtNmLote, txtDtCriacaoLote, txtAtivoLote, txtObsLote;

    public RecyclerTesteViewHolder(final View itemView) {
        super(itemView);
        txtCodLote = itemView.findViewById(R.id.txtDtDesmama);
        txtNmLote = itemView.findViewById(R.id.txtIdDesmama);
        txtDtCriacaoLote = itemView.findViewById(R.id.txtIdade);
        txtAtivoLote = itemView.findViewById(R.id.txtObs);




        //Setup the click listener
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clickRecyclerViewInterface.onCustomClick(mList.get(getLayoutPosition()));

            }
        });
    }
}
}