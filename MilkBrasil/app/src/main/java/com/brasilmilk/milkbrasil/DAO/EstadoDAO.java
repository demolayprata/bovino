package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Estado;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ricar on 25/11/2017.
 */

public class EstadoDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Estado> listaEstado;
    public EstadoDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Estado.class);
            scriptSQLDelete = Util.getDROPSQLite(Estado.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaEstado = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objEstado) {
        return super.insert(objEstado, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_ESTADO");
        return super.delete(this.db, Estado.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_ESTADO");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getCodEstado"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaEstado.clear();
        listaEstado.addAll((Collection<? extends Estado>) super.getAll(db, Estado.class));
        return listaEstado;
    }

    @Override
    public Object getById(Object param) {
        String paramStr = String.valueOf(param);
        List<String> compoList = new ArrayList<String>();
        try {
            int paramCod = Integer.valueOf(paramStr);
            compoList.add("ID_ESTADO");
        }catch (Exception e){
            compoList.add("NM_ESTADO");
        }
        String[] parametros = new String[]{paramStr};
        return super.getByParam(db, Estado.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Estado.class);
    }

}

