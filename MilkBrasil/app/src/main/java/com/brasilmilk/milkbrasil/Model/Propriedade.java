package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;


/**
 * Created by ricar on 20/11/2017.
 */
@ClassAnotation(nomeTabela = "FAZENDA")
public class Propriedade {
    @ApresentacaoAN(lisView = true)
    @AtributoAnotation(nomeAtributo = "ID_FAZENDA", tipoAtributo = "INTEGER", nomeGet = "getIdFazenda", nomeSet = "setIdFazenda", primaryKeyAtributo = true, autoIncrementAtributo = true)
    private Integer idFazenda;

    @AtributoAnotation(nomeAtributo = "NM_FAZENDA", nomeGet ="getNomeFazenda", nomeSet ="setNomeFazenda" )
    @ApresentacaoAN(lisView = true)
    private String  nomeFazenda;

    @AtributoAnotation(nomeAtributo = "QTD_HECTARE", tipoAtributo = "INTEGER", nomeGet = "getQtdHectares", nomeSet ="setQtdHectares" )
    @ApresentacaoAN
    private Integer qtdHectares;

    @AtributoAnotation(nomeAtributo = "NIRF_FAZENDA", nomeGet = "getNirfFazenda" , nomeSet ="setNirfFazenda" )
    @ApresentacaoAN
    private String nirfFazenda;

    @AtributoAnotation(nomeAtributo = "LATITUDE_SEDE", nomeGet = "getLatitudeSede", nomeSet ="setLatitudeSede" )
    @ApresentacaoAN
    private String latitudeSede;

    @AtributoAnotation(nomeAtributo = "LONGITUDE_FAZENDA", nomeGet = "getLongitudeFazenda", nomeSet ="setLongitudeFazenda" )
    @ApresentacaoAN
    private String longitudeFazenda;

    @AtributoAnotation(nomeAtributo = "ISC_ESTADUAL_FAZENDA", nomeGet = "getInscEstadual", nomeSet ="setInscEstadual" )
    @ApresentacaoAN
    private String inscEstadual;

    @AtributoAnotation(nomeAtributo = "DISTANCIA_SEDE_MUNICIPIO", nomeGet = "getDistanciaSedeMunincipio" , nomeSet ="setDistanciaSedeMunincipio" )
    @ApresentacaoAN
    private Integer distanciaSedeMunincipio;

    @AtributoAnotation(nomeAtributo = "FLG_CRIA", tipoAtributo = "INTEGER", tamanhoAtributo = 1, nomeGet = "isCria", nomeSet ="setCria" )
    @ApresentacaoAN
    private boolean flgCria;

    @AtributoAnotation(nomeAtributo = "FLG_RECRIA", tipoAtributo = "INTEGER", tamanhoAtributo = 1, nomeGet = "isRecria", nomeSet ="setRecria" )
    @ApresentacaoAN
    private boolean flgRecria;

    @AtributoAnotation(nomeAtributo = "FLG_ENGORDA", tipoAtributo = "INTEGER", tamanhoAtributo = 1, nomeGet = "isEngorda", nomeSet ="setEngorda" )
    @ApresentacaoAN
    private boolean flgEngorda;

    @AtributoAnotation(nomeAtributo = "FLG_LEITE", tipoAtributo = "INTEGER", tamanhoAtributo = 1, nomeGet = "isLeite", nomeSet ="setLeite" )
    @ApresentacaoAN
    private boolean flgLeite;

    @AtributoAnotation(nomeAtributo = "KM_ESTRADA", tipoAtributo = "INTEGER", nomeGet = "getKmEstrada", nomeSet ="setKmEstrada" )
    @ApresentacaoAN
    private Integer kmEstrada;

    @AtributoAnotation(nomeAtributo = "KM_RODOVIA", tipoAtributo = "INTEGER", nomeGet = "getKmRodovia", nomeSet ="setKmRodovia" )
    @ApresentacaoAN
    private Integer kmRodovia;

    @AtributoAnotation(nomeAtributo = "OBS", tamanhoAtributo = 500, nomeGet = "getObs", nomeSet ="setObs" )
    @ApresentacaoAN
    private String obs;

    @ApresentacaoAN
    @AtributoAnotation(nomeAtributo = "ID_CIDADE", tipoAtributo = "INTEGER", forengKey = true, referencesTable = "CIDADE", fieldReferences = "ID_CIDADE", nomeSet = "setCodCidade", nomeGet = "getCodCidade")
    private Integer codCidade;

    @ApresentacaoAN
    @AtributoAnotation(nomeAtributo = "ID_ESTADO", tipoAtributo = "INTEGER", forengKey = true, referencesTable = "ESTADO", fieldReferences = "ID_ESTADO",nomeGet = "getCodEstado", nomeSet = "setCodEstado")
    private Integer codEstado;

    public Propriedade() {

    }
    public Propriedade(Integer idFazenda, String nomeFazenda, Integer qtdHectares, String nirfFazenda, String latitudeSede, String longitudeFazenda, String inscEstadual, Integer distanciaSedeMunincipio, boolean cria, boolean recria, boolean engorda, boolean leite, Integer kmEstrada, Integer kmRodovia, String obs) {
        this.idFazenda = idFazenda;
        this.nomeFazenda = nomeFazenda;
        this.qtdHectares = qtdHectares;
        this.nirfFazenda = nirfFazenda;
        this.latitudeSede = latitudeSede;
        this.longitudeFazenda = longitudeFazenda;
        this.inscEstadual = inscEstadual;
        this.distanciaSedeMunincipio = distanciaSedeMunincipio;
        this.flgCria = cria;
        this.flgRecria = recria;
        this.flgEngorda = engorda;
        this.flgLeite = leite;
        this.kmEstrada = kmEstrada;
        this.kmRodovia = kmRodovia;
        this.obs = obs;
    }

    @MetodoAnotation(tipoRetorno = "Integer")
    public Integer getIdFazenda() {
        return idFazenda;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setIdFazenda(Integer idFazenda) {
        this.idFazenda = idFazenda;
    }

    @MetodoAnotation()
    public String getNomeFazenda() {
        return nomeFazenda;
    }

    @MetodoAnotation()
    public void setNomeFazenda(String nomeFazenda) {
        this.nomeFazenda = nomeFazenda;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getQtdHectares() {
        return qtdHectares;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setQtdHectares(Integer qtdHectares) {
        this.qtdHectares = qtdHectares;
    }

    @MetodoAnotation()
    public String getNirfFazenda() {
        return nirfFazenda;
    }

    @MetodoAnotation()
    public void setNirfFazenda(String nirfFazenda) {
        this.nirfFazenda = nirfFazenda;
    }

    @MetodoAnotation()
    public String getLatitudeSede() {
        return latitudeSede;
    }

    @MetodoAnotation()
    public void setLatitudeSede(String latitudeSede) {
        this.latitudeSede = latitudeSede;
    }

    @MetodoAnotation()
    public String getLongitudeFazenda() {
        return longitudeFazenda;
    }

    @MetodoAnotation()
    public void setLongitudeFazenda(String longitudeFazenda) {
        this.longitudeFazenda = longitudeFazenda;
    }

    @MetodoAnotation()
    public String getInscEstadual() {
        return inscEstadual;
    }

    @MetodoAnotation()
    public void setInscEstadual(String inscEstadual) {
        this.inscEstadual = inscEstadual;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getDistanciaSedeMunincipio() {
        return distanciaSedeMunincipio;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setDistanciaSedeMunincipio(Integer distanciaSedeMunincipio) {
        this.distanciaSedeMunincipio = distanciaSedeMunincipio;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isCria() {
        return flgCria;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setCria(boolean cria) {
        this.flgCria = cria;
    }

    @MetodoAnotation(tipoRetorno = "boolean")
    public boolean isRecria() {
        return flgRecria;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setRecria(boolean recria) {
        this.flgRecria = recria;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isEngorda() {
        return flgEngorda;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setEngorda(boolean engorda) {
        this.flgEngorda = engorda;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public boolean isLeite() {
        return flgLeite;
    }

    @MetodoAnotation(tipoRetorno = "boolean", tipoSet = "boolean")
    public void setLeite(boolean leite) {
        this.flgLeite = leite;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getKmEstrada() {
        return kmEstrada;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setKmEstrada(Integer kmEstrada) {
        this.kmEstrada = kmEstrada;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getKmRodovia() {
        return kmRodovia;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setKmRodovia(Integer kmRodovia) {
        this.kmRodovia = kmRodovia;
    }

    @MetodoAnotation()
    public String getObs() {
        return obs;
    }

    @MetodoAnotation()
    public void setObs(String obs) {
        obs = obs;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getCodCidade() {
        return codCidade;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setCodCidade(Integer codCidade) {
        this.codCidade = codCidade;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public Integer getCodEstado() {
        return codEstado;
    }

    @MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
    public void setCodEstado(Integer codEstado) {
        this.codEstado = codEstado;
    }
}
