package com.brasilmilk.milkbrasil.Helper;

import android.content.Context;
import android.widget.ListView;

import com.brasilmilk.milkbrasil.Adapter.AdapterPeso;
import com.brasilmilk.milkbrasil.DAO.PesoDAO;
import com.brasilmilk.milkbrasil.Model.Peso;

import java.util.List;

/**
 * Created by ricar on 24/12/2017.
 */

public class PesoHelper {
    private Context ctx;
    private PesoDAO pesoDAO;
    private AdapterPeso adapterPeso;
    private List<Peso> pesoList;
    public PesoHelper(Context ctx){
        this.ctx = ctx;
    }
    public void preencherListView( final ListView listViewPeso, String idBovino) {
        if(idBovino.length()>0){
            pesoDAO = new PesoDAO(ctx);
            pesoList = (List<Peso>) pesoDAO.getPesosByBovino(Integer.parseInt(idBovino));

            adapterPeso = new AdapterPeso(ctx,pesoList);
            listViewPeso.removeAllViewsInLayout();
            listViewPeso.setAdapter(adapterPeso);
        }

    }

}
