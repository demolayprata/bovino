package com.brasilmilk.milkbrasil.Model;

import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;


import java.math.BigDecimal;
import java.sql.Date;


/**
 * The persistent class for the notafiscal database table.
 * 
 */
@ClassAnotation(nomeTabela = "NOTAFISCAL")
public class Notafiscal {

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_NOTAFISCAL", tipoAtributo = "INTEGER", nomeSet = "setIdNotaFiscal", nomeGet = "getIdNotaFiscal", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idNotafiscal;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTEMISSAO", nomeGet = "getDtEmissao", nomeSet = "setDtEmissao")
	private Date dtEmissao;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "TIPOOPERACAO", nomeGet = "getTipoOperacao", nomeSet = "setTipoOperacao")
	private String tipoOperacao;
//
//	private boolean flgCompra;
//
//
//	private boolean flgServico;
//
//
//	private boolean flgVenda;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "NRFISCAL", nomeGet = "getNrFiscal", nomeSet = "setNrFiscal", unique = true)
	private String nrFiscal;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "SERIENOTA", nomeGet = "getSerieNota", nomeSet = "setSerieNota")
	private String serieNota;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "VLRNOTA", nomeGet = "getVlrNota", nomeSet = "setVlrNota", tipoAtributo = "DOUBLE")
	private BigDecimal vlrNota;


	public Notafiscal(Integer idNotafiscal, Date dtEmissao, String tipoOperacao, String nrFiscal, String serieNota) {
		this.idNotafiscal = idNotafiscal;
		this.dtEmissao = dtEmissao;
		this.tipoOperacao = tipoOperacao;
		this.nrFiscal = nrFiscal;
		this.serieNota = serieNota;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdNotafiscal() {
		return idNotafiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdNotafiscal(Integer idNotafiscal) {
		this.idNotafiscal = idNotafiscal;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtEmissao() {
		return dtEmissao;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtEmissao(Date dtEmissao) {
		this.dtEmissao = dtEmissao;
	}

	@MetodoAnotation()
	public String getTipoOperacao() {
		return tipoOperacao;
	}

	@MetodoAnotation()
	public void setTipoOperacao(String tipoOperacao) {
		this.tipoOperacao = tipoOperacao;
	}

	@MetodoAnotation()
	public String getNrFiscal() {
		return nrFiscal;
	}

	@MetodoAnotation()
	public void setNrFiscal(String nrFiscal) {
		this.nrFiscal = nrFiscal;
	}

	@MetodoAnotation()
	public String getSerieNota() {
		return serieNota;
	}

	@MetodoAnotation()
	public void setSerieNota(String serieNota) {
		this.serieNota = serieNota;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrNota() {
		return vlrNota;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrNota(BigDecimal vlrNota) {
		this.vlrNota = vlrNota;
	}
}