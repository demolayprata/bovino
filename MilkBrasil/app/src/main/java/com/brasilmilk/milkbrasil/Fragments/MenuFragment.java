package com.brasilmilk.milkbrasil.Fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Interface.ButtonReferenceInterface;
import com.brasilmilk.milkbrasil.R;

public class MenuFragment extends Fragment {

    private Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar;
    private TabHost tabHost;
    private TextView txtViewCabecalho;
    private RecyclerView recyclerView;

    private ButtonReferenceInterface buttonReferenceListener;

    public MenuFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @SuppressLint("WrongViewCast")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        btnCadastrar = view.findViewById(R.id.btnSalvar);
        btnEditar = view.findViewById(R.id.btnEditar);
        btnExcluir = view.findViewById(R.id.btnExcluir);
        btnLimpar = view.findViewById(R.id.btnLimpar);
        btnPesquisar = view.findViewById(R.id.btnPesquisar);
        btnListar = view.findViewById(R.id.btnListar);

        tabHost = view.findViewById(R.id.tab_host);
        txtViewCabecalho = view.findViewById(R.id.txtViewCabecalho);
        recyclerView = view.findViewById(R.id.recycler_listVMonta);

        if(savedInstanceState != null){
            txtViewCabecalho.setText(savedInstanceState.getString("cabecalho"));
        }

        buttonReferenceListener.passarReferencia(btnCadastrar,btnEditar,btnExcluir,btnLimpar,btnPesquisar,btnListar,tabHost, recyclerView);

        return view;
    }



    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ButtonReferenceInterface){
            buttonReferenceListener = (ButtonReferenceInterface) context;
        }
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }

    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
