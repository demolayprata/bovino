package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class PropriedadeDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Propriedade> listaPropriedade;
    public PropriedadeDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Propriedade.class);
            scriptSQLDelete = Util.getDROPSQLite(Propriedade.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaPropriedade = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objPropriedade) {
        return super.insert(objPropriedade, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_FAZENDA");
        return super.delete(this.db, Propriedade.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_FAZENDA");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdFazenda"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaPropriedade.clear();
        listaPropriedade.addAll((Collection<? extends Propriedade>) super.getAll(db, Propriedade.class));
        return  listaPropriedade;
    }

    public List<?> getLike(String nome) {
        listaPropriedade.clear();
        Cursor cursor = null;
        try {
            StringBuilder whereBuilder = new StringBuilder();
            whereBuilder.append("NM_FAZENDA");
            whereBuilder.append(" LIKE %?%");

            String where = whereBuilder.toString();
            String[]args = new String[]{nome};

            cursor = db.query(Util.getNomeTabela(Propriedade.class), Util.getColumView(Propriedade.class), where, args, null, null, null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Propriedade.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Propriedade.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Propriedade.class, objLinha );
                    }
                    listaPropriedade.add((Propriedade) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return  listaPropriedade;
    }

    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        try {
            Integer paramCod = Integer.valueOf((String) param);
            compoList.add("ID_FAZENDA");
        }catch (Exception e){
            compoList.add("NM_FAZENDA");
        }
        String[] parametros = new String[]{(String) param};
        return super.getByParam(db, Propriedade.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Propriedade.class);
    }

}
