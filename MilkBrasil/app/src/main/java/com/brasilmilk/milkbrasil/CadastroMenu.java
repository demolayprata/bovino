package com.brasilmilk.milkbrasil;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;

import com.brasilmilk.milkbrasil.Activity.BovinoCadastro;
import com.brasilmilk.milkbrasil.Activity.MontaActivy;
import com.brasilmilk.milkbrasil.Activity.PropriedadeCadastro;

/**
 * Created by ricar on 21/11/2017.
 */

public class CadastroMenu extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cadastro_menu);
        setTitle("CADASTROS");
        ImageButton btnCadPropriedade = (ImageButton) findViewById(R.id.imgBttPropriedadeCad);
        btnCadPropriedade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(CadastroMenu.this, PropriedadeCadastro.class);
                startActivity(intent);
            }
        });
        ImageButton btnBovCad = (ImageButton) findViewById(R.id.imgBttBovinoCad);
        btnBovCad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(CadastroMenu.this, BovinoCadastro.class);
                startActivity(intent);
            }
        });

        ImageButton btnMontaCad = (ImageButton) findViewById(R.id.imgBtnMonta);
        btnMontaCad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent;
                intent = new Intent(CadastroMenu.this, MontaActivy.class);
                startActivity(intent);
            }
        });

    }
}
