package com.brasilmilk.milkbrasil.Fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Helper.MatrizHelper;
import com.brasilmilk.milkbrasil.Helper.MontaHelper;
import com.brasilmilk.milkbrasil.Helper.MontaResultadoHelper;
import com.brasilmilk.milkbrasil.Helper.ReprodutorHelper;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.R;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.Iterator;
import java.util.List;


public class MontaResultadoFragment extends Fragment implements Validator.ValidationListener, View.OnClickListener, TabHost.OnTabChangeListener, ClickRecyclerView_Interface {

    private OnItemSelectedListener listener;
    private MontaResultadoHelper montaResultadoHelper;
    private Monta monta;

    private Context ctx;
    private AlertDialog alerta;
    private EditText edtCodMonta;
    private EditText edtDtMonta;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtDtResultado;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtPrevParto;
    private EditText edtDtPrevResultado;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodMatriz;
    private EditText edtNmMatriz;
    @NotEmpty(message = "Campo Obrigatório")
    private EditText edtCodReprodutor;
    private EditText edtNmReprodutor;

   private CheckBox chkCheia, chkVaiza;

    private MatrizHelper matrizHelper;
    private ReprodutorHelper reprodutorHelper;
    private Button  btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar;
    private TabHost tabHost;
    private RecyclerView recyclerView;


    private  Validator validator;
    private boolean validacaoOk = false;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_monta_resultado, container, false);
        edtCodMonta = (EditText) view.findViewById(R.id.edtIdMonta);
        edtDtMonta = (EditText) view.findViewById(R.id.edtDtMonta);
        edtDtResultado = (EditText) view.findViewById(R.id.edtDtResultado);
        edtDtPrevResultado = (EditText) view.findViewById(R.id.edtPrevResultado);
        edtPrevParto = (EditText) view.findViewById(R.id.edtDtPrevParto);
        edtCodMatriz = (EditText) view.findViewById(R.id.edtIdMatriz);
        edtNmMatriz = (EditText) view.findViewById(R.id.edtDtNomeMatriz);
        edtCodReprodutor = (EditText) view.findViewById(R.id.edtIdReprodutor);
        edtNmReprodutor = (EditText) view.findViewById(R.id.edtDtNomeReprodutor);

        chkCheia = (CheckBox) view.findViewById(R.id.chkCheia);
        chkVaiza = (CheckBox) view.findViewById(R.id.chkVazia);

        ctx = inflater.getContext();

        montaResultadoHelper = new MontaResultadoHelper(ctx, edtCodMonta, edtDtMonta, edtPrevParto,edtDtPrevResultado,
                edtDtResultado, edtCodReprodutor,   edtNmReprodutor,  edtCodMatriz, edtNmMatriz,
                chkCheia, chkVaiza,  monta,  recyclerView);

        matrizHelper = new MatrizHelper(ctx,  edtNmMatriz, edtCodMatriz);
        reprodutorHelper = new ReprodutorHelper(ctx, edtNmReprodutor, edtCodReprodutor);
        validator = new Validator(this);
        validator.setValidationListener(this);

        return view;



    }

    @Override
    public void onValidationSucceeded() {
        validacaoOk = true;

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(ctx));
            }
            iterator.remove();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View view) {

        if(view.equals(btnCadastrar)){
            try{
                if((chkCheia.isChecked() && !chkVaiza.isChecked())||(chkCheia.isChecked() && !chkVaiza.isChecked())){
                    validator.validate();
                    if(validacaoOk) {
                           montaResultadoHelper.alterar();
                        validacaoOk = false;
                    }
                }else {
                    Toast.makeText(ctx,"SELECIONAR O RESULTADO DA MONTA SE ESTA CHEIA OU VAZIA",Toast.LENGTH_SHORT);
                }

            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }

        }
        if(view.equals(btnExcluir)){

        }
        if(view.equals(btnLimpar)){
            montaResultadoHelper.limpaCampos();
        }
        if(view.equals(btnPesquisar)){

        }
        if(view.equals(btnListar)){
            montaResultadoHelper.setaRecyclerView();
            montaResultadoHelper.preencherListView(this);
            tabHost.setCurrentTab(1);
        }
    }


    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "ABA LISTA":
                montaResultadoHelper.setaRecyclerView();
                montaResultadoHelper.preencherListView(this);
                break;
            default:
                break;
        }

    }

    @Override
    public void onCustomClick(Object object) {
        monta = (Monta)object;
        montaResultadoHelper.setarCampos();
        tabHost.setCurrentTab(0);

    }

    public interface  OnItemSelectedListener{
        public void onItemSelected(Monta monta);
    }

    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
        ctx = context;

    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }


    public void passarReferenciaButton(final Button btnCadastrar, final Button btnEditar, final Button btnExcluir
            , final Button btnLimpar, final Button btnPesquisar, final Button btnListar, final TabHost tabHost, final RecyclerView recyclerView) {
        this.btnCadastrar = btnCadastrar;
        this.btnCadastrar.setOnClickListener(this);
        this.btnExcluir = btnExcluir;
        this.btnExcluir.setOnClickListener(this);
        this.btnLimpar = btnLimpar;
        this.btnLimpar.setOnClickListener(this);
        this.btnPesquisar = btnPesquisar;
        this.btnPesquisar.setOnClickListener(this);
        this.btnListar = btnListar;
        this.btnListar.setOnClickListener(this);
        this.btnEditar = btnEditar;
        this.btnEditar.setOnClickListener(this);
        this.tabHost = tabHost;
        this.tabHost.setOnTabChangedListener(this);
        this.recyclerView = recyclerView;


    }
}
