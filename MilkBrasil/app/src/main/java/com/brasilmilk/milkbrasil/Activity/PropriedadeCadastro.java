package com.brasilmilk.milkbrasil.Activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TabHost;
import android.widget.TextView;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.DAO.CidadeDAO;
import com.brasilmilk.milkbrasil.DAO.EstadoDAO;
import com.brasilmilk.milkbrasil.DAO.PropriedadeDAO;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Fragments.MapFazendaFragment;
import com.brasilmilk.milkbrasil.Model.Cidade;
import com.brasilmilk.milkbrasil.Model.Estado;
import com.brasilmilk.milkbrasil.Model.Propriedade;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.Util;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ricar on 22/11/2017.
 */
@SuppressWarnings("deprecation")
public class PropriedadeCadastro extends FragmentActivity  implements View.OnClickListener, AdapterView.OnItemClickListener, Validator.ValidationListener, MapFazendaFragment.InterfaceComunicao,  NavigationView.OnNavigationItemSelectedListener, OnMapReadyCallback, GoogleMap.OnMapClickListener, TabHost.OnTabChangeListener {



    Button btnCadastrar, btnEditar, btnExcluir, btnLimpar, btnPesquisar, btnListar, btnAddCidade, btnAddUf;
    private AlertDialog alerta;

    EditText edtCodFazenda, edtPesquisa;

    @NotEmpty(message = "Nome da Fazenda Obrigatorio")
    EditText edtNomeFazenda;

    @NotEmpty(message = "Quantidade de Hectares Obrigatório")
    EditText edtQtdHectare;

    @NotEmpty(message = "NIRF Obrigatório")
            @Pattern(regex = "\\d.\\d\\d\\d.\\d\\d\\d-\\d", message = "NIRF Incorreto")
    EditText edtNirf;

    @NotEmpty(message = "Latitude Obrigatório")
    @Pattern(regex = "\\d\\dº\\d\\d'\\d\\d''\\w", message = "Latitude Incorreta")
    EditText edtLatitude;

    @NotEmpty(message = "Longitude Obrigatório")
            @Pattern(regex = "\\d\\dº\\d\\d'\\d\\d''\\w")
    EditText edtLongitude;

    @NotEmpty
            @Pattern(regex = "\\d\\d.\\d\\d\\d.\\d\\d\\d\\d-\\d",message = "Inscrição Estadual Obrigatória")
    EditText edtIscEstadual;


    @NotEmpty
    EditText edtDistSedeMunicipio;


    Spinner spnCidades;
    Spinner spnUfs;

    CheckBox chkCria, chkRecria, chkEngorda, chkLeite;
    ListView listViewPropriedades;
    PropriedadeDAO propriedadeDao;
    CidadeDAO cidadeDAO;
    EstadoDAO estadoDAO;
    Propriedade propriedadeObj;

    Cidade cidadeSelected;
    Estado estadoSelected;

    FragmentManager fragmentManager;
    FragmentActivity mFragmentActivity;

    TabHost tabHost;

    private String provider;
    private GoogleMap mMap;
    private LocationManager locationManager;
    private  Validator validator;


    private List<String> propriedadeList;
    private boolean validacaoOk = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cidadeSelected = new Cidade();
        estadoSelected = new Estado();
        setContentView(R.layout.propriedade_cad);

        btnCadastrar = (Button) findViewById(R.id.btnSalvar);
        btnCadastrar.setOnClickListener(this);

        btnEditar = (Button) findViewById((R.id.btnEditar));
        btnEditar.setOnClickListener(this);

        btnExcluir = (Button) findViewById(R.id.btnExcluir);
        btnExcluir.setOnClickListener(this);

        btnLimpar = (Button) findViewById(R.id.btnLimpar);
        btnLimpar.setOnClickListener(this);

        btnPesquisar = (Button) findViewById(R.id.btnPesquisar);
        btnPesquisar.setOnClickListener(this);

        btnListar = (Button) findViewById(R.id.btnListar);
        btnListar.setOnClickListener(this);

        btnAddCidade = (Button) findViewById(R.id.btnAddCidade);
        btnAddCidade.setOnClickListener(this);

        btnAddUf = (Button) findViewById(R.id.btnAddUf);
        btnAddUf.setOnClickListener(this);

        listViewPropriedades = (ListView) findViewById(R.id.listViewPropriedades);
        listViewPropriedades.setOnItemClickListener(this);

        tabHost = (TabHost) findViewById(android.R.id.tabhost);
        tabHost.setup();

        TabHost.TabSpec tabCadastro = tabHost.newTabSpec("ABA CADASTRO");
        TabHost.TabSpec tabLista = tabHost.newTabSpec("ABA LISTA");
        TabHost.TabSpec tabMapa = tabHost.newTabSpec("MAPA");

        tabCadastro.setIndicator("Cadastro");
        tabCadastro.setContent(R.id.cadastroPropriedade);

        tabLista.setIndicator("Lista");
        tabLista.setContent(R.id.listaPropriedades);

        tabMapa.setIndicator("Mapa");
        tabMapa.setContent(R.id.containerMap);


        tabHost.addTab(tabCadastro);
        tabHost.addTab(tabMapa);
        tabHost.addTab(tabLista);

        tabHost.setOnTabChangedListener(this);


        edtCodFazenda = (EditText) findViewById(R.id.edtIdFazenda);
        edtNomeFazenda = (EditText) findViewById(R.id.edtNmFazenda);
        edtQtdHectare = (EditText) findViewById(R.id.edtQtdHectare);
        edtNirf = (EditText) findViewById(R.id.edtNirfFazenda);
        edtLatitude = (EditText) findViewById(R.id.edtLatSede);
        edtLongitude = (EditText) findViewById(R.id.edtLongitude);
        edtIscEstadual = (EditText) findViewById(R.id.edtIscEstadual);
        edtDistSedeMunicipio = (EditText) findViewById(R.id.edtDistSedeMun);
        edtPesquisa = (EditText) findViewById(R.id.edtPesquisa);


        chkCria = (CheckBox) findViewById(R.id.chkCria);
        chkRecria = (CheckBox) findViewById(R.id.chkRecria);
        chkEngorda = (CheckBox) findViewById(R.id.chkEngroda);
        chkLeite = (CheckBox) findViewById(R.id.chkLeite);

        setarMascaras();
        validator = new Validator(this);
        validator.setValidationListener(this);

        spnCidades = (Spinner) findViewById(R.id.spnCidades);
        spnUfs = (Spinner) findViewById(R.id.spnUfs);

        preencherNmCidadesArraySpn();
        preencherNmEstadosArraySpn();
        spnCidades.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posicao, long id) {
                cidadeSelected = (Cidade) cidadeDAO.getById(parent.getItemAtPosition(posicao).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spnUfs.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int posicao, long id) {
                estadoSelected = (Estado) estadoDAO.getById(parent.getItemAtPosition(posicao).toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

//        fragmentManager = getSupportFragmentManager();
//        FragmentTransaction transaction = fragmentManager.beginTransaction();
//
//        transaction.add(R.id.containerMap, new MapFazendaFragment());
//        transaction.commitAllowingStateLoss();


        MapFragment mMapFragment = MapFragment.newInstance();
        android.app.FragmentTransaction fragmentTransaction;
        fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.add(R.id.containerMap, mMapFragment);
        fragmentTransaction.commit();
        mMapFragment.getMapAsync(this);


        /////////campo pesquisa list
        edtPesquisa.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(edtPesquisa.length()==0){
                    propriedadeDao = new PropriedadeDAO(PropriedadeCadastro.this);
                    preencherListView((List<Propriedade>) propriedadeDao.getAll());
                    propriedadeDao.close();
                }else{
                    pesquisar();
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });



    }

    private ArrayAdapter<String> setAdapterList(List<String> nomeList) {
        ArrayAdapter<String> adapterCidades = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, nomeList);
        ArrayAdapter<String> spnArrayAdapter = adapterCidades;
        spnArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_item);
        return spnArrayAdapter;
    }

    private void preencherNmCidadesArraySpn() {
        List<String> nmCidadeList = new ArrayList<>();
        cidadeDAO = new CidadeDAO(this);
        List<Cidade> cidadeList = (List<Cidade>) cidadeDAO.getAll();
        for(Cidade cidade : cidadeList){
            nmCidadeList.add(cidade.getNmCidade());
        }
        spnCidades.setAdapter(setAdapterList(nmCidadeList));
        cidadeDAO.close();
    }
    private void preencherNmEstadosArraySpn(){
        List<String> nmEstadoList = new ArrayList<>();

        estadoDAO = new EstadoDAO(this);
        List<Estado> estadoList = (List<Estado>) estadoDAO.getAll();
        for(Estado estado : estadoList){
            nmEstadoList.add(estado.getNomeEstado());
        }
        spnUfs.setAdapter(setAdapterList(nmEstadoList));
        estadoDAO.close();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String vendedorNome = parent.getItemAtPosition(position).toString();
        propriedadeDao = new PropriedadeDAO(this);
        propriedadeObj = (Propriedade) propriedadeDao.getById(vendedorNome);
        if(propriedadeObj != null){
            setarCampos(propriedadeObj);
            tabHost.setCurrentTab(0);
        }

    }

    @Override
    public void onClick(View view) {
        propriedadeDao = new PropriedadeDAO(this);
        if(view == btnCadastrar){
            try{
                validator.validate();
                if(validacaoOk) {
                    if(edtCodFazenda.length()>0){
                        Propriedade propriedade = recuperaDadosCampos();
                        boolean update = propriedadeDao.alterar(propriedade);
                        limparCampos();
                        if (update == true) {
                            Toast.makeText(this, "Propriedade Alterada", Toast.LENGTH_LONG).show();
                        } else {
                            Toast.makeText(this, "Selecione uma Propriedade para Alterar", Toast.LENGTH_LONG).show();
                        }
                    }else {
                        Integer id = propriedadeDao.salvar(recuperaDadosCampos());
                        limparCampos();
                        Toast.makeText(this, "Propriedade Cadastrada", Toast.LENGTH_LONG).show();
                    }
                    validacaoOk = false;
                }
            }catch (Exception e){
                Log.e("Erro:", e.getMessage());
            }


        }
        if(view == btnEditar){
            if(edtCodFazenda.length()> 0){
                validator.validate();
                if(validacaoOk) {
                    Propriedade propriedade = recuperaDadosCampos();
                    boolean update = propriedadeDao.alterar(propriedade);
                    limparCampos();
                    if (update == true) {
                        Toast.makeText(this, "Propriedade Alterada", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(this, "Selecione uma Propriedade para Alterar", Toast.LENGTH_LONG).show();
                    }
                    validacaoOk = false;
                }
            }
        }
        if(view == btnExcluir){
            if(edtCodFazenda.length()>0){
                String[] parametros = new String[]{edtCodFazenda.getText().toString()};
                boolean delete = propriedadeDao.excluir(parametros);
                limparCampos();
                if(delete){
                    Toast.makeText(this, "Propriedade Removida", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(this, "Selecione uma Propriedade para excluir", Toast.LENGTH_LONG).show();
                }
            }
        }
        if(view == btnPesquisar){
            String propriedadeNome = edtNomeFazenda.getText().toString().length() >0 ? edtNomeFazenda.getText().toString(): edtCodFazenda.getText().toString() ;
            if(propriedadeNome.length() >0){
                propriedadeDao = new PropriedadeDAO(this);
                propriedadeObj = (Propriedade) propriedadeDao.getById(propriedadeNome);
                if(propriedadeObj != null){
                    setarCampos(propriedadeObj);
                }

            }
        }
        if(view == btnLimpar){
            validator.validate();
            limparCampos();

        }
        if(view == btnListar){
            preencherListView((List<Propriedade>) propriedadeDao.getAll());
            limparCampos();
            tabHost.setCurrentTab(2);
        }
        if(view == btnAddCidade){
            LayoutInflater li = getLayoutInflater();

            //inflamos o layout alerta.xml na view
            final View viewModalCidade = li.inflate(R.layout.modal_cidade_crud, null);
            //definimos para o botão do layout um clickListener
            viewModalCidade.findViewById(R.id.btnSalvarNovaCidade).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    EditText edtCidadeNova = (EditText) viewModalCidade.findViewById(R.id.edtNovaCidade);
                    cidadeDAO = new CidadeDAO(PropriedadeCadastro.this);
                    Cidade cidadeNova = new Cidade();
                    cidadeNova.setNmCidade(edtCidadeNova.getText().toString());
                     cidadeDAO.salvar(cidadeNova);
                     cidadeDAO.close();
                     preencherNmCidadesArraySpn();

                    //desfaz o alerta.
                    alerta.dismiss();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Cidade Nova");
            builder.setView(viewModalCidade);
            alerta = builder.create();
            alerta.show();

        }

        if(view == btnAddUf){
            LayoutInflater li = getLayoutInflater();

            //inflamos o layout alerta.xml na view
            final View viewModalEstado = li.inflate(R.layout.modal_estado_crud, null);
            //definimos para o botão do layout um clickListener
            viewModalEstado.findViewById(R.id.btnSalvarNovoEstado).setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    EditText edtEstadoNovo = (EditText) viewModalEstado.findViewById(R.id.edtNovoEstado);
                    estadoDAO = new EstadoDAO(PropriedadeCadastro.this);
                    Estado estadoNovo = new Estado();
                    estadoNovo.setNomeEstado(edtEstadoNovo.getText().toString());
                    estadoDAO.salvar(estadoNovo);
                    estadoDAO.close();
                    preencherNmEstadosArraySpn();

                    //desfaz o alerta.
                    alerta.dismiss();
                }
            });

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Estado Novo");
            builder.setView(viewModalEstado);
            alerta = builder.create();
            alerta.show();

        }

    }



    public Propriedade recuperaDadosCampos(){
        propriedadeObj = new Propriedade();
        try {
            if(edtCodFazenda.length() > 0){
                propriedadeObj.setIdFazenda(Integer.parseInt(edtCodFazenda.getText().toString()));
            }
            propriedadeObj.setCria(chkCria.isChecked());
            propriedadeObj.setDistanciaSedeMunincipio(Integer.parseInt(edtDistSedeMunicipio.getText().toString()));
            propriedadeObj.setEngorda(chkEngorda.isChecked());
            propriedadeObj.setInscEstadual(edtIscEstadual.getText().toString());
            propriedadeObj.setLatitudeSede(edtLatitude.getText().toString());
            propriedadeObj.setLeite(chkLeite.isChecked());
            propriedadeObj.setLongitudeFazenda(edtLongitude.getText().toString());
            propriedadeObj.setNirfFazenda(edtNirf.getText().toString());
            propriedadeObj.setNomeFazenda(edtNomeFazenda.getText().toString());
            propriedadeObj.setQtdHectares(Integer.parseInt(edtQtdHectare.getText().toString()));
            propriedadeObj.setRecria(chkRecria.isChecked());

            propriedadeObj.setCodCidade(cidadeSelected.getCodCidade());
            propriedadeObj.setCodEstado(estadoSelected.getCodEstado());

        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
        return  propriedadeObj;
    }
    public void limparCampos() {
        edtNirf.setText("");
        edtCodFazenda.setText("");
        edtDistSedeMunicipio.setText("");
        edtIscEstadual.setText("");
        edtLatitude.setText("");
        edtLongitude.setText("");
        edtNomeFazenda.setText("");
        edtQtdHectare.setText("");
        chkCria.setChecked(false);
        chkEngorda.setChecked(false);
        chkLeite.setChecked(false);
        chkRecria.setChecked(false);
        preencherListView((List<Propriedade>) propriedadeDao.getAll());

        List<String> nmCidadeList = new ArrayList<>();
        List<String> nmEstadoList = new ArrayList<>();
        spnCidades.setAdapter(setAdapterList(nmCidadeList));
        spnCidades.setSelection(-1);
        spnUfs.setAdapter(setAdapterList(nmEstadoList));
        spnUfs.setSelection(-1);
    }
    public void setarCampos(Propriedade propriedade){
        edtNirf.setText(propriedade.getNirfFazenda());
        edtCodFazenda.setText(String.valueOf(propriedade.getIdFazenda()));
        edtDistSedeMunicipio.setText(String.valueOf(propriedade.getDistanciaSedeMunincipio()));
        edtIscEstadual.setText(String.valueOf(propriedade.getInscEstadual()));
        edtLatitude.setText(propriedade.getLatitudeSede());
        edtLongitude.setText(propriedade.getLongitudeFazenda());
        edtNomeFazenda.setText(propriedade.getNomeFazenda());
        edtQtdHectare.setText(String.valueOf(propriedade.getQtdHectares()));
        chkCria.setChecked(propriedade.isCria());
        chkEngorda.setChecked(propriedade.isEngorda());
        chkLeite.setChecked(propriedade.isLeite());
        chkRecria.setChecked(propriedade.isRecria());

        Integer codCidade = propriedade.getCodCidade();
        Integer codEstado = propriedade.getCodEstado();
        cidadeDAO = new CidadeDAO(this);
        Cidade cidade = (Cidade) cidadeDAO.getById(codCidade);
        cidadeDAO.close();
        estadoDAO = new EstadoDAO(this);
        Estado estado = (Estado) estadoDAO.getById(codEstado);
        estadoDAO.close();

        preencherNmCidadesArraySpn();
        spnCidades.setSelection(Util.getIndexSpinner(spnCidades, cidade.getNmCidade()));
        preencherNmEstadosArraySpn();
        spnUfs.setSelection(Util.getIndexSpinner(spnUfs, estado.getNomeEstado()));

    }
    private void preencherListView(List<Propriedade> listaPropriedade){
        propriedadeList = new ArrayList<>();
        String[] listNomeVendedores = new String[listaPropriedade.size()];
        for(int i = 0; i< listaPropriedade.size(); i++){
            listNomeVendedores[i] = listaPropriedade.get(i).getNomeFazenda();
            propriedadeList.add(listaPropriedade.get(i).getNomeFazenda());
        }
        ArrayAdapter<String> adapterListaVendedores =
                new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, listNomeVendedores);
        listViewPropriedades.setAdapter(adapterListaVendedores);
    }
    private  void setarMascaras(){

        Util.setMaskEditText(edtNirf, Mask.NIRF.getMascara());
        Util.setMaskEditText(edtLatitude, Mask.LATITUDE.getMascara());
        Util.setMaskEditText(edtLongitude, Mask.LONGITUDE.getMascara());
        Util.setMaskEditText(edtIscEstadual,Mask.INSC_ESTADUAL.getMascara());


    }

    @Override
    public void onValidationSucceeded() {
        if(spnCidades.getSelectedItemPosition() != -1 && spnUfs.getSelectedItemPosition()!= -1
                && (chkCria.isChecked()||chkEngorda.isChecked()||chkLeite.isChecked()||chkRecria.isChecked())){
            validacaoOk = true;
        }else{

            validacaoOk = false;
            Toast.makeText(this, "Falta campos para preenhcer", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        Iterator<ValidationError> iterator = errors.iterator();
        while(iterator.hasNext()){
            ValidationError error = iterator.next();
            View view = error.getView();
            if(view instanceof TextView){
                ((TextView) view).setError(error.getCollatedErrorMessage(this));
            }
            iterator.remove();
        }
    }

    @Override
    public void getLocation(LatLng provider) {
        edtLatitude.setText(String.valueOf( provider.latitude));
        edtLongitude.setText(String.valueOf(provider.longitude));
    }

    @Override
    public void setLocation(Integer latitude, Integer longitude) {

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @SuppressLint("MissingPermission")
    @Override
    public void onMapReady(GoogleMap googleMap) {
        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);

        try {

            mMap = googleMap;
            mMap.setOnMapClickListener(this);
            mMap.getUiSettings().setZoomControlsEnabled(true);

            mMap.setMyLocationEnabled(true);
        }catch (SecurityException e){
            Log.e("Erro:", e.getMessage());
        }

        // Add a marker in Sydney and move the camera

        //@SuppressLint("MissingPermission") Location location =  locationManager.getLastKnownLocation(provider);
        Location location = mMap.getMyLocation();
        if(location == null){
            location = locationManager.getLastKnownLocation(provider);
        }
        if(location!= null) {
            edtLongitude.setText(String.valueOf(location.getLongitude()));
            edtLatitude.setText(String.valueOf(location.getLatitude()));

            setarMarcadorLocalidade(location.getLatitude(),location.getLongitude());

        }
        mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
               LatLng latLng = marker.getPosition();
               String[] latitude = Util.separararDMS(String.valueOf(latLng.latitude),1);
               String[] longitude = Util.separararDMS(String.valueOf(latLng.longitude),2);
               StringBuilder stringBuilder = new StringBuilder();
               for(String str : latitude){
                   stringBuilder.append(str);
               }
               edtLatitude.setText(stringBuilder.toString());
                stringBuilder.setLength(0);
                for(String str : longitude){
                    stringBuilder.append(str);
                }
               edtLongitude.setText(stringBuilder.toString());
            }
        });
    }

    private void setarMarcadorLocalidade(double latitude, double longitude) {
        LatLng localidade = new LatLng(latitude, longitude);
        MarkerOptions marker = new MarkerOptions();
        marker.draggable(true);
        marker.position(localidade);
        marker.title("Local Propriedade");

        mMap.addMarker(marker);

        mMap.moveCamera(CameraUpdateFactory.newLatLng(localidade));
    }

    @Override
    public void onMapClick(LatLng latLng) {
        String[] latidute = Util.separararDMS(String.valueOf(latLng.latitude), 1);
        String[] longitude = Util.separararDMS(String.valueOf(latLng.longitude),2);
        StringBuilder latBuilder = new StringBuilder();
        latBuilder.append("Lat: ");
        for(int i=0;i<latidute.length; i++){
            latBuilder.append(latidute[i]);
            switch (i){
                case 0:
                    latBuilder.append("º");
                    break;
                case 1:
                    latBuilder.append("'");
                    break;
                case 2:
                    latBuilder.append("''");
                break;
            }

        }
        latBuilder.append(" Long: ");
        for(int i=0;i<longitude.length; i++){
            latBuilder.append(longitude[i]);
            switch (i){
                case 0:
                    latBuilder.append("º");
                    break;
                case 1:
                    latBuilder.append("'");
                    break;
                case 2:
                    latBuilder.append("''");
                    break;
            }

        }

        Toast.makeText(this,"coordenadas: "+ latBuilder.toString(),Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onTabChanged(String s) {
        switch (s){
            case "MAPA":
                if((!edtLatitude.getText().equals("") && !edtLongitude.getText().equals(""))&&
                        ((edtLatitude.getText().toString().contains("S")|| edtLatitude.getText().toString().contains("N"))&& (edtLongitude.getText().toString().contains("W") || edtLongitude.getText().toString().contains("E"))   )){
                    mMap.clear();
                    setarMarcadorLocalidade(Util.DMSToDecimal(edtLatitude.getText().toString()),Util.DMSToDecimal(edtLongitude.getText().toString()));

                 }
                break;
            case "ABA CADASTRO":
                preencherNmCidadesArraySpn();
                preencherNmEstadosArraySpn();
                break;
            case "ABA LISTA":
                propriedadeDao = new PropriedadeDAO(this);
                preencherListView((List<Propriedade>) propriedadeDao.getAll());
                propriedadeDao.close();
                break;
            default:
                break;
        }
    }

    public void pesquisar() {
        List<String> pesquisa = new ArrayList<>();
        int textlength = edtPesquisa.getText().length();
        pesquisa.clear();

        for(String pr : propriedadeList){
            if(pr !=null) {
                if (pr.contains(edtPesquisa.getText().toString())) {
                    pesquisa.add(pr);
                }
            }
        }
        ArrayAdapter<String> adapterListaPesquisa = new ArrayAdapter<String>(this, android.R.layout.simple_expandable_list_item_1, pesquisa);
        listViewPropriedades.setAdapter(adapterListaPesquisa);
    }


}
