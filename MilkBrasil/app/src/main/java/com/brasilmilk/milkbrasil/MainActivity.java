package com.brasilmilk.milkbrasil;

import android.app.TabActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TabHost;

import com.brasilmilk.milkbrasil.Activity.TabListaPrincipalMenu;
import com.brasilmilk.milkbrasil.Activity.TabMenuPrincipal;

@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TabHost tabHost = (TabHost) findViewById(android.R.id.tabhost);

        TabHost.TabSpec tabFeed = tabHost.newTabSpec("TAB FEED");
        TabHost.TabSpec tabMenu = tabHost.newTabSpec("TAB MENU");

        tabFeed.setIndicator("Vendas");
        //tabFeed.setIndicator("", getResources().getDrawable(R.mipmap.ic_launcher));
        tabFeed.setContent(new Intent(this, TabListaPrincipalMenu.class));

        tabMenu.setIndicator("Menu");
        //tabMenu.setIndicator("", getResources().getDrawable(R.mipmap.ic_launcher));
        tabMenu.setContent(new Intent(this, TabMenuPrincipal.class));

        tabHost.addTab(tabFeed);
        tabHost.addTab(tabMenu);

    }
}
