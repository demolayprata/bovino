package com.brasilmilk.milkbrasil.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Peso;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;

import java.util.List;

/**
 * Created by ricar on 20/12/2017.
 */

public class AdapterPeso extends BaseAdapter {
    private Context ctx;
    private List<Peso> listaPeso;

    public AdapterPeso(Context act, List<Peso> listaBovino) {
        this.ctx = act;
        this.listaPeso = listaBovino;
    }

    @Override
    public int getCount() {
        return listaPeso.size();
    }

    @Override
    public Object getItem(int i) {
        return listaPeso.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        View view = LayoutInflater.from(ctx).inflate(R.layout.bovino_peso_adapter,null);

        TextView txtDtPeso = (TextView) view.findViewById(R.id.txtDataPesagemAd);
        TextView txtQtdPeso = (TextView) view.findViewById(R.id.txtPesoAd);

        Peso peso = listaPeso.get(position);
        if(peso != null){
            txtDtPeso.setText("Data Pesagem: "+ DateUtil.formatFieldToTxt(peso.getDtPesagem()));
            txtQtdPeso.setText("Peso: "+peso.getQtdQuilos().toString());
        }


        return view;
    }
}
