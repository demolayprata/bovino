package com.brasilmilk.milkbrasil.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Adapter.AdapterMontaRecView;
import com.brasilmilk.milkbrasil.DAO.MontaDAO;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.List;

/**
 * Created by ricar on 11/01/2018.
 */

public class MontaResultadoHelper {
        private Context ctx;

        private AlertDialog.Builder builder;
        private AlertDialog alertaHelper;
        private RecyclerView mRecyclerView;

        private AdapterMontaRecView montaRecView;
        private List<Monta> montaList;
        private List<Bovino> matrizVaziaList;
        private RecyclerView.LayoutManager mLayoutManager;

        private EditText edtCodMonta, edtDtMonta, edtDtPrevResultado, edtDtResultado, edtDtPrevParto, edtCodSemem, edtNmReprodutor, edtCodMatriz, edtNmMatriz ;
        private TabHost tabHost;

        private CheckBox chkCheia, chkVazia;

        private Monta monta;

        private MontaDAO montaDAO;

       // private TabHost tabHost;

//        private MatrizHelper matrizHelper;
//        private ReprodutorHelper reprodutorHelper;
        AlertDialog alertaRacaHelper;
    public MontaResultadoHelper(Context ctx, final EditText edtCodMonta, final EditText edtDtMonta, final EditText edtDtPrevParto,
                                final EditText edtDtPrevResultado, final EditText edtDtResultado, final EditText edtCodSemem,
                                final EditText edtNmReprodutor, final EditText edtCodMatriz, final EditText edtNmMatriz,
                                final CheckBox chkCheia, final CheckBox chkVazia,
                                final Monta monta, final RecyclerView recyclerView ){
            this.ctx = ctx;

            this.edtCodMonta = edtCodMonta;
            this.edtDtMonta = edtDtMonta;
            this.edtDtResultado = edtDtResultado;
            this.edtDtPrevParto = edtDtPrevParto;
            this.edtDtPrevResultado = edtDtPrevResultado;
            this.edtCodSemem = edtCodSemem;
            this.edtNmReprodutor = edtNmReprodutor;
            this.edtCodMatriz = edtCodMatriz;
            this.edtNmMatriz = edtNmMatriz;

            this.chkCheia = chkCheia;
            this.chkVazia = chkVazia;

            this.monta = monta;


            this.mRecyclerView = recyclerView;

            setarMascara();
            limpaCampos();

        }
    public void preencherListView(ClickRecyclerView_Interface clickRecyclerView_interface) {
        montaDAO = new MontaDAO(ctx);
        montaList = (List<Monta>) montaDAO.getMontaByResultadosByFaltantes();
        montaRecView = new AdapterMontaRecView(ctx, montaList, clickRecyclerView_interface);
        mRecyclerView.setAdapter(montaRecView);
    }



    @RequiresApi(api = Build.VERSION_CODES.N)
    public void recuperarCampos(){
        if(edtCodMonta.getText().toString().length() > 0) {
            monta.setIdMonta(Integer.parseInt(edtCodMonta.getText().toString()));
        }
        monta.setDtMonta(DateUtil.covertToDateSql(edtDtMonta.getText().toString()));
        monta.setDtPrevParto(edtDtPrevParto.getText().toString().length()>0?DateUtil.covertToDateSql(edtDtPrevParto.getText().toString()):null);
        monta.setDtResultado(edtDtResultado.getText().toString().length()>0?DateUtil.covertToDateSql(edtDtResultado.getText().toString()):null);
        monta.setFlgCheia(chkCheia.isChecked());
        monta.setFlgVazia(chkVazia.isChecked());
        monta.setIdSemem(Integer.parseInt(edtCodSemem.getText().toString()));
        monta.setIdMatriz(Integer.parseInt(edtCodMatriz.getText().toString()));
        monta.setIdMatriz(Integer.parseInt(edtCodMatriz.getText().toString()));
        monta.setDtPrevResultado(DateUtil.covertToDateSql(edtDtPrevResultado.getText().toString()));
    }
    public void limpaCampos(){
        edtNmMatriz.setText("");
        edtNmReprodutor.setText("");
        edtCodMatriz.setText("");
        edtCodSemem.setText("");
        edtDtPrevParto.setText(DateUtil.calculaDataPrevisaoParto(DateUtil.gerarDateNowSql()));
        edtDtPrevResultado.setText("");
        edtDtResultado.setText(DateUtil.gerarDateNow());
        edtDtMonta.setText("");
        edtCodMonta.setText("");
        chkVazia.setChecked(false);
        chkCheia.setChecked(false);
        monta = new Monta();
    }
    public void setarCampos(){

        BovinoHelper bovinoHelper = new BovinoHelper(ctx);
        Bovino bovino =  bovinoHelper.getBovinoById(monta.getIdMatriz());

        edtNmMatriz.setText(bovino.getApelido());
        edtCodMatriz.setText(bovino.getIdBovino().toString());

        bovino = bovinoHelper.getBovinoById(monta.getIdSemem());
        edtNmReprodutor.setText(bovino.getApelido());
        edtCodSemem.setText(bovino.getIdBovino().toString());
        edtDtPrevParto.setText(DateUtil.calculaDataPrevisaoParto(DateUtil.gerarDateNowSql()));
        edtDtResultado.setText(DateUtil.gerarDateNow());
        edtDtPrevResultado.setText(DateUtil.formatFieldToTxt(monta.getDtPrevResultado()));
        edtDtMonta.setText(DateUtil.formatFieldToTxt(monta.getDtMonta()));
        edtCodMonta.setText(monta.getIdMonta().toString());
        chkVazia.setChecked(monta.isFlgVazia());
        chkCheia.setChecked(monta.isFlgCheia());
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public int salvar(){
        montaDAO = new MontaDAO(ctx);
        recuperarCampos();
        int idMonta = montaDAO.salvar(monta);
        montaDAO.close();
        limpaCampos();
        if( idMonta > 0){
            Toast.makeText(ctx, "Monta Cadastrada", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(ctx, "Monta Não Cadastrada", Toast.LENGTH_LONG).show();
        }
        return  idMonta;
    }
    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean alterar(){
        montaDAO = new MontaDAO(ctx);
        recuperarCampos();
        boolean isAlterado = montaDAO.alterar(monta);
        montaDAO.close();

        if( isAlterado){
            BovinoHelper bovinoHelper = new BovinoHelper(ctx);
            Bovino bovino = bovinoHelper.getBovinoById(monta.getIdMatriz());
            bovino.setFlgCheia(chkCheia.isChecked());
            bovino.setFlgVazia(chkVazia.isChecked());
            boolean isAlteradoBovino = bovinoHelper.alterar(bovino);
            Toast.makeText(ctx, "Resultado Monta Cadastrado", Toast.LENGTH_LONG).show();
        }else {
            Toast.makeText(ctx, "Falha ao Cadastrar Resultado Monta", Toast.LENGTH_LONG).show();
        }
        limpaCampos();
        return  isAlterado;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public boolean excluir(){
        montaDAO = new MontaDAO(ctx);
        String[] parametros = new String[]{edtCodMonta.getText().toString()};
        boolean isDelete = montaDAO.excluir(parametros);
        limpaCampos();
        if(isDelete){
            Toast.makeText(ctx, "Monta Removida", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(ctx, "Erro ao Excluir a Monta", Toast.LENGTH_LONG).show();
        }
        return  isDelete;
    }

    private void setarMascara(){
        Util.setMaskEditText(edtDtMonta, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtPrevParto, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtResultado, Mask.DATA.getMascara());
    }

    public void setaRecyclerView(){
        //Aqui é instanciado o Recyclerview

        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

    }




}
