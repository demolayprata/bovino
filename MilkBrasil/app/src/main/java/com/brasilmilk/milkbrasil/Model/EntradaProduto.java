package com.brasilmilk.milkbrasil.Model;


import com.brasilmilk.milkbrasil.Anotacoes.ApresentacaoAN;
import com.brasilmilk.milkbrasil.Anotacoes.AtributoAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.ClassAnotation;
import com.brasilmilk.milkbrasil.Anotacoes.MetodoAnotation;

import java.math.BigDecimal;
import java.sql.Date;


@ClassAnotation(nomeTabela = "ENTRADAPRODUTO")
public class EntradaProduto {
	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_ENTRADAPRODUTO", tipoAtributo = "INTEGER", nomeSet = "setIdEntrada", nomeGet = "getIdEntrada", primaryKeyAtributo = true, autoIncrementAtributo = true, notNull = true)
	private Integer idEntrada;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "DTENTRADA", nomeGet = "getDtEntrada", nomeSet = "setDtEntrada")
	private Date dtEntrada;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "QTDENTRADA", tipoAtributo = "DOUBLE", nomeGet = "getQtdEntrada", nomeSet = "setQtdEntrada")
	private BigDecimal qtdEntrada;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "VLRUNITARIO",tipoAtributo = "DOUBLE", nomeGet = "getVlrUnitario", nomeSet = "setVlrUnitario")
	private BigDecimal vlrUnitario;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_PRODUTO",tipoAtributo = "INTEGER", nomeGet = "getIdProduto", nomeSet = "setIdProduto",  forengKey = true, referencesTable = "PRODUTO", fieldReferences = "ID_PRODUTO", notNull = true)
	private Integer idProduto;

	@ApresentacaoAN(lisView = true)
	@AtributoAnotation(nomeAtributo = "ID_NOTAFISCAL",tipoAtributo = "INTEGER", nomeGet = "getIdNotaFiscal", nomeSet = "setIdNotaFiscal",  forengKey = true, referencesTable = "NOTAFISCAL", fieldReferences = "ID_NOTAFISCAL", notNull = true)
	private Integer idNotaFiscal;


	public EntradaProduto(Integer idEntrada, Date dtEntrada, BigDecimal qtdEntrada, BigDecimal vlrUnitario, Integer idProduto, Integer idNotaFiscal) {
		this.idEntrada = idEntrada;
		this.dtEntrada = dtEntrada;
		this.qtdEntrada = qtdEntrada;
		this.vlrUnitario = vlrUnitario;
		this.idProduto = idProduto;
		this.idNotaFiscal = idNotaFiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdEntrada() {
		return idEntrada;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdEntrada(Integer idEntrada) {
		this.idEntrada = idEntrada;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public Date getDtEntrada() {
		return dtEntrada;
	}

	@MetodoAnotation(tipoRetorno = "Date", tipoSet = "Date")
	public void setDtEntrada(Date dtEntrada) {
		this.dtEntrada = dtEntrada;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdNotaFiscal() {
		return idNotaFiscal;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdNotaFiscal(Integer idNotaFiscal) {
		this.idNotaFiscal = idNotaFiscal;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getQtdEntrada() {
		return qtdEntrada;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setQtdEntrada(BigDecimal qtdEntrada) {
		this.qtdEntrada = qtdEntrada;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public BigDecimal getVlrUnitario() {
		return vlrUnitario;
	}

	@MetodoAnotation(tipoRetorno = "BigDecimal", tipoSet = "BigDecimal")
	public void setVlrUnitario(BigDecimal vlrUnitario) {
		this.vlrUnitario = vlrUnitario;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public Integer getIdProduto() {
		return idProduto;
	}

	@MetodoAnotation(tipoRetorno = "Integer", tipoSet = "Integer")
	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}
}