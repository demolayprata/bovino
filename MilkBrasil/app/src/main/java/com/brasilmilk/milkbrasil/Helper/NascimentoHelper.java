package com.brasilmilk.milkbrasil.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.brasilmilk.milkbrasil.Adapter.AdapterMontaRecView;
import com.brasilmilk.milkbrasil.Adapter.AdapterNascimentoRecView;
import com.brasilmilk.milkbrasil.DAO.NascimentoDAO;
import com.brasilmilk.milkbrasil.Enum.Mask;
import com.brasilmilk.milkbrasil.Fragments.MenuFragment;
import com.brasilmilk.milkbrasil.Interface.ClickRecyclerView_Interface;
import com.brasilmilk.milkbrasil.Interface.InterfaceHelper;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Model.Nascimento;
import com.brasilmilk.milkbrasil.R;
import com.brasilmilk.milkbrasil.Util.DateUtil;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.List;

/**
 * Created by ricar on 24/12/2017.
 */

public class NascimentoHelper implements InterfaceHelper{
    private Context ctx;
    private NascimentoDAO nascimentoDAO;
    private AdapterNascimentoRecView nascimentoAdapter;
    private List<Nascimento> nascimentoList;
    private RecyclerView mRecyclerView;

    private RecyclerView.LayoutManager mLayoutManager;
    private Nascimento nascimento;

    private EditText edtCodNascimento, edtDtParto, edtCodMonta, edtDtMonta, edtCodBovinoNovo, edtNmBovinoNovo;
    private RadioButton rbMacho, rbFemea, rbVivo, rbMorto;
    private RadioGroup rgSexo, rgStatusNascimento;
    private AlertDialog alerta;
    private AlertDialog.Builder  builder;


    public NascimentoHelper(Context ctx, final Nascimento nascimento, final RecyclerView recyclerView,
                            final EditText edtCodNascimento, final EditText edtDtParto, final EditText  edtCodMonta,
                            final EditText  edtDtMonta, final EditText edtCodBovinoNovo, final EditText edtNmBovinoNovo, final RadioButton rbMacho, final RadioButton rbFemea,
                            final RadioButton rbVivo, final RadioButton rbMorto,
                            final RadioGroup rgSexo, final RadioGroup rgStatusNascimento){
        this.ctx = ctx;
        this.nascimento = nascimento;
        this.mRecyclerView = recyclerView;
        this.edtCodNascimento = edtCodNascimento;
        this.edtDtParto =edtDtParto;
        this.edtCodMonta = edtCodMonta;
        this.edtDtMonta = edtDtMonta;
        this.edtCodBovinoNovo = edtCodBovinoNovo;
        this.edtNmBovinoNovo = edtNmBovinoNovo;
        this.rbMacho = rbMacho;
       ;
        this.rbFemea = rbFemea;
        this.rbVivo = rbVivo;

        this.rbMorto = rbMorto;
        this.rgSexo = rgSexo;
        this.rgSexo = rgSexo;
        this.rgStatusNascimento = rgStatusNascimento;
        this.rgStatusNascimento = rgStatusNascimento;
    }

    public NascimentoHelper(Context ctx, final Nascimento nascimento){
        this.ctx = ctx;
        this.nascimento = nascimento;
    }

    @Override
    public void limparCampos() {
        edtCodNascimento.setText("");
        edtCodMonta.setText("");
        edtDtMonta.setText("");
        edtCodBovinoNovo.setText("");
        edtNmBovinoNovo.setText("");
        edtDtParto.setText(DateUtil.gerarDateNow());
        rbVivo.isChecked();
        rbMacho.isChecked();
    }

    @Override
    public void setarCampos() {
        Monta monta = new Monta();
        MontaHelper montaHelper = new MontaHelper(ctx, monta);
        montaHelper.getMontaByCodMonta(nascimento.getIdMonta());
        edtCodMonta.setText(nascimento.getIdMonta().toString());
        edtDtMonta.setText(DateUtil.formatFieldToTxt(monta.getDtMonta()));

        Bovino bovino = new Bovino();
        BovinoHelper bovinoHelper = new BovinoHelper(ctx, bovino);
        bovinoHelper.getBovinoById(nascimento.getIdBovino());
        edtCodBovinoNovo.setText(bovino.getIdBovino().toString());
        edtNmBovinoNovo.setText(bovino.getApelido());

        edtDtParto.setText(DateUtil.formatFieldToTxt(nascimento.getDtParto()));
        edtCodNascimento.setText(nascimento.getIdNascimento().toString());
        rbMacho.setChecked(nascimento.getSexo().equals("M"));
        rbFemea.setChecked(nascimento.getSexo().equals("F"));
        rbVivo.setChecked(nascimento.isFlgVivo());
        rbMorto.setChecked(nascimento.isFlgVivo()?false:true);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void recuperarCampos() {
        if(edtCodNascimento.getText().toString().length()>0){
            nascimento.setIdNascimento(Integer.parseInt(edtCodNascimento.getText().toString()));
        }
        nascimento.setDtParto(DateUtil.covertToDateSql(edtDtParto.getText().toString()));
        nascimento.setFlgVivo(rbVivo.isChecked());
        nascimento.setIdMonta(Integer.parseInt(edtCodMonta.getText().toString()));
        nascimento.setIdBovino(Integer.parseInt(edtCodBovinoNovo.getText().toString()));
        nascimento.setSexo(rbMacho.isChecked()?"M":"F");
    }

    @Override
    public boolean excluir() {
        return false;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public int salvar() {
        nascimentoDAO = new NascimentoDAO(ctx);
        recuperarCampos();
        int idNascimento = nascimentoDAO.salvar(nascimento);
        nascimentoDAO.close();
        if(idNascimento >0){
            Monta monta = new Monta();
            MontaHelper montaHelper = new MontaHelper(ctx, monta);
            montaHelper.getMontaByCodMonta(nascimento.getIdMonta());

            BovinoHelper bovinoHelper = new BovinoHelper(ctx);
            Bovino bovino = bovinoHelper.getBovinoById(monta.getIdMatriz());
            bovino.setFlgCheia(false);
            bovino.setFlgVazia(true);
            boolean isAlteradoBovino = bovinoHelper.alterar(bovino);

            Toast.makeText(ctx, "Nascimento Cadastrado", Toast.LENGTH_SHORT);
        }else {
            Toast.makeText(ctx, "Nascimento não Cadastrado",Toast.LENGTH_SHORT);
        }
        return idNascimento;
    }

    @Override
    public void preencherListView(RecyclerView mRecyclerView, ClickRecyclerView_Interface clickRecyclerView_Interface) {

            nascimentoDAO = new NascimentoDAO(ctx);
            nascimentoList = (List<Nascimento>) nascimentoDAO.getAll();

            nascimentoAdapter = new AdapterNascimentoRecView(ctx, nascimentoList, clickRecyclerView_Interface);
            mRecyclerView.setAdapter(nascimentoAdapter);

    }

    @Override
    public void setaRecyclerView(RecyclerView mRecyclerView) {
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
    }

    @Override
    public void setarMascara() {
        Util.setMaskEditText(edtDtMonta, Mask.DATA.getMascara());
        Util.setMaskEditText(edtDtParto, Mask.DATA.getMascara());
    }

    @Override
    public void inflateModalList(View view, AlertDialog alerta) {

    }

    public void inflaterModalBovinoList(View viewModalBovinoList, AlertDialog alerta, ClickRecyclerView_Interface clickRecyclerView_interface) {

        BovinoHelper bovinoHelper = new BovinoHelper(ctx);
        this.alerta = alerta;
        mRecyclerView = (RecyclerView) viewModalBovinoList.findViewById(R.id.recycler_listBovino);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        bovinoHelper.preencherListView(mRecyclerView,  clickRecyclerView_interface);

        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR BOVINO");
        builder.setView(viewModalBovinoList);
        this.alerta = builder.create();
        this.alerta.show();
       
    }

    public void inflaterModalMontaList(View viewModalMontaList, AlertDialog alerta, ClickRecyclerView_Interface clickRecyclerView_interface) {


        this.alerta = alerta;
        mRecyclerView = (RecyclerView) viewModalMontaList.findViewById(R.id.recycler_listViewMonta);
        MontaHelper montaHelper = new MontaHelper(ctx, mRecyclerView);
        mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);
        montaHelper.preencherListView( clickRecyclerView_interface);

        builder = new AlertDialog.Builder(ctx);
        builder.setTitle("SELECIONAR MONTA");
        builder.setView(viewModalMontaList);
        this.alerta = builder.create();
        this.alerta.show();

    }
public void fecharModalList(){
        if(alerta != null){
            this.alerta.dismiss();
        }

}

}
