package com.brasilmilk.milkbrasil.DAO;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.brasilmilk.milkbrasil.Abstract.AbstratroDAO;
import com.brasilmilk.milkbrasil.Helper.SQLiteHelper;
import com.brasilmilk.milkbrasil.Interface.InterfaceDAO;
import com.brasilmilk.milkbrasil.Model.Bovino;
import com.brasilmilk.milkbrasil.Model.Monta;
import com.brasilmilk.milkbrasil.Util.Util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ricar on 20/11/2017.
 */

public class MontaDAO extends AbstratroDAO implements InterfaceDAO {
    private  String scriptSQLCreate;
    private  String scriptSQLDelete;
    private SQLiteDatabase db;
    private SQLiteHelper dbHelper;
    private List<Monta> listaMonta;
    public MontaDAO(Context ctx) {
        try {
            scriptSQLCreate = Util.getSQLCreate(Monta.class);
            scriptSQLDelete = Util.getDROPSQLite(Monta.class);
            dbHelper = new SQLiteHelper(ctx, SQLiteHelper.NOME_BD, SQLiteHelper.VERSAO_BD, this.scriptSQLCreate, this.scriptSQLDelete);
            this.listaMonta = new ArrayList<>();
            ///metodo para criacao dos banco e tabelas
            db = dbHelper.getWritableDatabase();
            dbHelper.onCreate(db);
        }catch (Exception e){
            Log.e("Erro: ", e.getMessage());
        }
    }

    @Override
    public Integer salvar(Object objBovino) {
        return super.insert(objBovino, this.db);
    }

    @Override
    public boolean excluir(String[] parametros) {
        List<String> campoList = new ArrayList<String>();
        campoList.add("ID_MONTA");
        return super.delete(this.db, Monta.class, campoList, parametros);
    }

    @Override
    public boolean alterar(final Object obj) {
        List<String> campoList = new ArrayList<String>(){{add("ID_MONTA");}};
        String[] parametros = new String[]{String.valueOf(Util.getValueMethod(obj,"getIdMonta"))};
        return super.edit(this.db, obj, campoList, parametros);
    }

    @Override
    public List<?> getAll() {
        listaMonta.clear();
        listaMonta.addAll((Collection<? extends Monta>) super.getAll(db, Monta.class));
        return listaMonta;
    }

    public List<?> getNascimentoByMonta(String idMonta) {
        listaMonta.clear();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT B.* FROM MONTA M, NASCIMENTO N, BOVINO B");
            selectBuilder.append("WHERE M.ID_MONTA = N.ID_MONTA AND B.ID_NASCIMENTO = N.ID_NASCIMENTO");
            selectBuilder.append(" M.ID_MONTA =  ");
            selectBuilder.append(idMonta);



           // "SELECT B.* FROM MONTA M, NASCIMENTO N, BOVINO B WHERE M.ID_MONTA = N.ID_MONTA AND B.ID_NASCIMENTO = N.ID_NASCIMENTO"

            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Bovino.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, objLinha );
                    }
              //      listaMonta.add((Bovino) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return listaMonta;
    }

    public List<?> getMontaByResultadoByNull() {
        listaMonta.clear();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT M.ID_MATRIZ FROM MONTA M");
            selectBuilder.append("WHERE  M.DTRESULTADO = 'null'");


            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Monta.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Monta.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Monta.class, objLinha );
                    }
                    listaMonta.add((Monta) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return listaMonta;
    }

    @Override
    public Object getById(Object param) {
        List<String> compoList = new ArrayList<String>();
        try {
            Integer paramCod = Integer.valueOf((String) param);
            compoList.add("ID_BOVINO");
        }catch (Exception e){
            compoList.add("APELIDO");
        }
        String[] parametros = new String[]{(String) param};
        return super.getByParam(db, Bovino.class, compoList, parametros);
    }

    public void close() {
        super.close(db);
    }

    @Override
    public ContentValues contentValues() {
        return super.contentCreate(Bovino.class);
    }

    public Bovino getMatrizByNascimento(String idMonta) {
        Bovino bovino = new Bovino();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT B.* FROM MONTA M, BOVINO B");
            selectBuilder.append("WHERE B.ID_BOVINO = M.ID_MATRIZ ");
            selectBuilder.append("AND M.ID_MONTA =  " );
            selectBuilder.append(idMonta);

            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Bovino.class);
                for(Map.Entry<String, String> entry : mapKey.entrySet()){
                    Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Bovino.class, bovino );
                }

            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return bovino;
    }


    public List<?> getMontaByResultadosByFaltantes() {
        listaMonta.clear();
        Cursor cursor = null;
        try {
            StringBuilder selectBuilder = new StringBuilder();
            selectBuilder.append("SELECT M.* FROM MONTA M ");
            selectBuilder.append(" WHERE ");
            selectBuilder.append(" M.DTRESULTADO = 'null' OR M.DTRESULTADO IS NULL ");




            // "SELECT B.* FROM MONTA M, NASCIMENTO N, BOVINO B WHERE M.ID_MONTA = N.ID_MONTA AND B.ID_NASCIMENTO = N.ID_NASCIMENTO"

            cursor = db.rawQuery(selectBuilder.toString(), null);
            if(cursor.getCount()>0){
                HashMap<String, String> mapKey = Util.getColumSet(Monta.class);
                while (cursor.moveToNext()){
                    Object objLinha = Util.createNewInstance(Monta.class);
                    for(Map.Entry<String, String> entry : mapKey.entrySet()){
                        Util.setValueAtributoByCursor(entry.getKey(), entry.getValue(), cursor, Monta.class, objLinha );
                    }
                        listaMonta.add((Monta) objLinha);
                }
            }
        }catch (Exception e){
            Log.e("Erro: ",e.getMessage());
        }
        finally {
            if(cursor != null){
                if(!cursor.isClosed()){
                    cursor.close();
                }
            }
        }



        return listaMonta;
    }


}
